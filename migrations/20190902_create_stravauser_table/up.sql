CREATE TABLE stravauser (
  id INTEGER PRIMARY KEY,
  user INTEGER NOT NULL,
  access_token VARCHAR NOT NULL,
  expires_at INTEGER NOT NULL,
  refresh_token VARCHAR NOT NULL,
  token_type VARCHAR NOT NULL,
  FOREIGN KEY (user) REFERENCES user(id)
);
