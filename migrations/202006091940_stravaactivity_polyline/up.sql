CREATE TABLE stravamap (
  id VARCHAR PRIMARY KEY NOT NULL,
  polyline VARCHAR,
  summary_polyline VARCHAR
);
CREATE TABLE stravaactivity_new (
  id BIGINT PRIMARY KEY NOT NULL,
  activity INTEGER,
  external_id VARCHAR NOT NULL,
  upload_id BIGINT NOT NULL,
  name VARCHAR NOT NULL,
  distance NUMERIC NOT NULL,
  moving_time INTEGER NOT NULL,
  elapsed_time INTEGER NOT NULL,
  total_elevation_gain NUMERIC NOT NULL,
  elev_high NUMERIC NOT NULL,
  elev_low NUMERIC NOT NULL,
  activitytype VARCHAR CHECK (activitytype in ('alpine_ski', 'backcountry_ski', 'canoeing', 'crossfit', 'e_bike_ride', 'elliptical', 'golf', 'handcycle', 'hike', 'ice_skate', 'inline_skate', 'kayaking', 'kitesurf', 'nordic_ski', 'ride', 'rock_climbing', 'roller_ski', 'rowing', 'run', 'sail', 'skateboard', 'snowboard', 'snowshoe', 'soccer', 'stair_stepper', 'stand_up_paddling', 'surfing', 'swim', 'velomobile', 'virtual_ride', 'virtual_run', 'walk', 'weight_training', 'wheelchair', 'windsurf', 'workout', 'yoga')) NOT NULL,
  start_date VARCHAR NOT NULL,
  start_date_local VARCHAR NOT NULL,
  timezone VARCHAR NOT NULL,
  -- start_latlng LatLng        An instance of LatLng.
  -- end_latlng LatLng  An instance of LatLng.
  achievement_count INTEGER NOT NULL,
  kudos_count INTEGER NOT NULL,
  comment_count INTEGER NOT NULL,
  athlete_count INTEGER,
  photo_count INTEGER NOT NULL,
  total_photo_count INTEGER NOT NULL,
  stravamap VARCHAR,
  trainer BOOLEAN NOT NULL,
  average_speed NUMERIC NOT NULL,
  max_speed NUMERIC NOT NULL,
  average_cadence NUMERIC,
  average_heartrate NUMERIC,
  max_heartrate NUMERIC,
  average_temp NUMERIC,
  gear_id VARCHAR NOT NULL,
  kilojoules NUMERIC,
  average_watts NUMERIC,
  device_watts BOOLEAN,
  max_watts INTEGER,
  weighted_average_watts INTEGER,
  description VARCHAR,
  -- gear SummaryGear   An instance of SummaryGear.
  calories NUMERIC NOT NULL,
  device_name VARCHAR,
  FOREIGN KEY (activity) REFERENCES activity(id),
  FOREIGN KEY (stravamap) REFERENCES stravamap(id)
);
INSERT INTO stravaactivity_new SELECT id, activity, external_id, upload_id, name, distance, moving_time, elapsed_time, total_elevation_gain, elev_high, elev_low, activitytype, start_date, start_date_local, timezone, achievement_count, kudos_count, comment_count, athlete_count, photo_count, total_photo_count, NULL, trainer, average_speed, max_speed, average_cadence, average_heartrate, max_heartrate, average_temp, gear_id, kilojoules, average_watts, device_watts, max_watts, weighted_average_watts, description, calories, device_name FROM stravaactivity;
DROP TABLE stravaactivity;
ALTER TABLE stravaactivity_new RENAME TO stravaactivity;
