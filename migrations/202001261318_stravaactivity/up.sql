CREATE TABLE stravaactivity2 (
  id BIGINT PRIMARY KEY NOT NULL,
  activity INTEGER,
  external_id VARCHAR NOT NULL,
  upload_id BIGINT NOT NULL,
  name VARCHAR NOT NULL,
  distance NUMERIC NOT NULL,
  moving_time INTEGER NOT NULL,
  elapsed_time INTEGER NOT NULL,
  total_elevation_gain NUMERIC NOT NULL,
  elev_high NUMERIC NOT NULL,
  elev_low NUMERIC NOT NULL,
  activitytype VARCHAR CHECK (activitytype in ('alpine_ski', 'backcountry_ski', 'canoeing', 'crossfit', 'e_bike_ride', 'elliptical', 'golf', 'handcycle', 'hike', 'ice_skate', 'inline_skate', 'kayaking', 'kitesurf', 'nordic_ski', 'ride', 'rock_climbing', 'roller_ski', 'rowing', 'run', 'sail', 'skateboard', 'snowboard', 'snowshoe', 'soccer', 'stair_stepper', 'stand_up_paddling', 'surfing', 'swim', 'velomobile', 'virtual_ride', 'virtual_run', 'walk', 'weight_training', 'wheelchair', 'windsurf', 'workout', 'yoga')) NOT NULL,
  start_date VARCHAR NOT NULL,
  start_date_local VARCHAR NOT NULL,
  timezone VARCHAR NOT NULL,
  -- start_latlng LatLng 	An instance of LatLng.
  -- end_latlng LatLng 	An instance of LatLng.
  achievement_count INTEGER NOT NULL,
  kudos_count INTEGER NOT NULL,
  comment_count INTEGER NOT NULL,
  athlete_count INTEGER,
  photo_count INTEGER NOT NULL,
  total_photo_count INTEGER NOT NULL,
  trainer BOOLEAN NOT NULL,
  average_speed NUMERIC NOT NULL,
  max_speed NUMERIC NOT NULL,
  average_cadence NUMERIC,
  average_heartrate NUMERIC,
  max_heartrate NUMERIC,
  average_temp NUMERIC,
  gear_id VARCHAR NOT NULL,
  kilojoules NUMERIC,
  average_watts NUMERIC,
  device_watts BOOLEAN,
  max_watts INTEGER,
  weighted_average_watts INTEGER,
  description VARCHAR,
  -- gear SummaryGear 	An instance of SummaryGear.
  calories NUMERIC NOT NULL,
  device_name VARCHAR,
  FOREIGN KEY (activity) REFERENCES activity(id)
);
INSERT INTO stravaactivity2 SELECT * FROM stravaactivity;
DROP TABLE stravaactivity;
ALTER TABLE stravaactivity2 RENAME TO stravaactivity;
