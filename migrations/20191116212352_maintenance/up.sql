DROP TABLE maintenance;
CREATE TABLE maintenance (
	id INTEGER PRIMARY KEY,
	bike INTEGER NOT NULL,
	date VARCHAR NOT NULL,
	description VARCHAR NOT NULL,
	FOREIGN KEY (bike) REFERENCES bike(id)
);
