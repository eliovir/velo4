CREATE TABLE oauthclient (
  id INTEGER PRIMARY KEY,
  user INTEGER NOT NULL,
  client_id VARCHAR NOT NULL,
  name VARCHAR NOT NULL,
  FOREIGN KEY (user) REFERENCES user(id)
);
CREATE TABLE oauthdevicecode (
  id INTEGER PRIMARY KEY,
  oauthclient INTEGER NOT NULL,
  device_code VARCHAR NOT NULL,	
  user_code VARCHAR NOT NULL,	
  user INTEGER, -- user who validated
  FOREIGN KEY (oauthclient) REFERENCES oauthclient(id),	
  FOREIGN KEY (user) REFERENCES user(id),
  UNIQUE(device_code),
  UNIQUE(user_code)
);
CREATE TABLE oauthtoken (
  id INTEGER PRIMARY KEY,
  oauthclient INTEGER NOT NULL,
  user INTEGER NOT NULL,
  access_token VARCHAR NOT NULL,
  expires_at BIGINT NOT NULL,
  refresh_token VARCHAR NOT NULL,
  FOREIGN KEY (oauthclient) REFERENCES oauthclient(id),
  FOREIGN KEY (user) REFERENCES user(id),
  UNIQUE(access_token),
  UNIQUE(refresh_token)
);

