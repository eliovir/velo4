CREATE TABLE bike2 (
	id INTEGER PRIMARY KEY,
	user INTEGER NOT NULL,
	name VARCHAR NOT NULL,
	enabled BOOLEAN NOT NULL,
	FOREIGN KEY (user) REFERENCES user(id)
);
INSERT INTO bike2 SELECT id, user, name, TRUE FROM bike;
DROP TABLE bike;
DROP VIEW v_bikesummary;
ALTER TABLE bike2 RENAME TO bike;
CREATE VIEW v_bikesummary AS
SELECT
        COUNT(*) AS activities,
        SUM(distance) AS distance,
        (SELECT MIN(date) FROM activity WHERE bike=b.id) AS first_activity,
        (SELECT COUNT(*) FROM activity WHERE bike=b.id AND strftime('%Y', date)=strftime('%Y', CURRENT_TIMESTAMP)) AS year_activities,
        (SELECT COALESCE(SUM(distance), 0) FROM activity WHERE bike=b.id AND strftime('%Y', date)=strftime('%Y', CURRENT_TIMESTAMP)) AS year_distance,
        b.id AS bike_id,
        b.name AS bike_name
FROM activity AS a
        JOIN bike AS b ON a.bike=b.id
GROUP BY b.id, b.name;
