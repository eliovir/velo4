/**
 * Check if browser has built-in datetime-local widget.
 * @return datetime-local support
 */
function supportDatetimeType() {
    var type = 'datetime-local';
    var i = document.createElement('input');
    i.setAttribute("type", type);
    return i.type !== "text";
}
/**
 * Load an URL then call callback function on success.
 *
 * @param {type} url URL to load
 * @param {type} callback function to run
 */
function load(url, callback) {
    console.log("load " + url);
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState < 4) {
            return;
        }
        if (xhr.status === 0) {
            console.log(url + " not loaded!");
            return;
        } else if (xhr.status !== 200) {
            return;
        }
        // all is well
        if (xhr.readyState === 4) {
            callback(xhr);
        }
    };
    xhr.responseType = 'arraybuffer';
    xhr.open('GET', url, true);
    xhr.setRequestHeader('Content-Type', 'application/msgpack');
    xhr.send('');
}
/**
 * Load a MsgPack object from URL then call callback function on success.
 *
 * @param {type} url URL to load
 * @param {type} callback function to run with MsgPack object
 */
function loadMsgpack(url, callback) {
    console.log("loadMsgpack " + url);
    load(url, function (xhr) {
        callback(msgpack.decode(new Uint8Array(xhr.response)));
    });
}
var units = {
    distance: "km",
    duration: "h",
    average: "km/h",
    max_speed: "km/h",
    temperature: "°C",
    ascent: "m",
    weight: "kg",
};
(function($){
  $(function(){
    $('.sidenav').sidenav();
    $('.collapsible').collapsible();
    $(".dropdown-trigger").dropdown();
    if (!supportDatetimeType()) {
      // https://materializecss.com/pickers.html
      $('.datepicker').datepicker({
        firstDay: 1,
        format: 'yyyy-mm-ddTHH:MM',
        i18n: {
            cancel: 'Annuler',
            clear: 'Effacer',
            decimalPoint: ',',
            done: 'Ok',
            months: [
                  'Janvier',
                  'Février',
                  'Mars',
                  'Avril',
                  'Mai',
                  'Juin',
                  'Juillet',
                  'Août',
                  'Septembre',
                  'Octobre',
                  'Novembre',
                  'Décembre'
            ],
            monthsShort: [
                  'Jan',
                  'Fév',
                  'Mar',
                  'Avr',
                  'Mai',
                  'Juin',
                  'Juil',
                  'Aoû',
                  'Sep',
                  'Oct',
                  'Nov',
                  'Déc'
            ],
            weekdays: [
                  'Dimanche',
                  'Lundi',
                  'Mardi',
                  'Mercredi',
                  'Jeudi',
                  'Vendredi',
                  'Samedi'
            ],
            weekdaysShort: [
                  'Dim',
                  'Lun',
                  'Mar',
                  'Mer',
                  'Jeu',
                  'Ven',
                  'Sam'
            ],
            weekdaysAbbrev: ['D', 'L','Ma','Me','J','V','S']
        }
      });
    }
    $('.fixed-action-btn').floatingActionButton();
    $('.select').formSelect();
    if (typeof Highcharts === 'undefined') {
        return;
    }
    var lang = {
            months: [
                'Janvier', 'Février', 'Mars', 'Avril',
                'Mai', 'Juin', 'Juillet', 'Août',
                'Septembre', 'Octobre', 'Novembre', 'Décembre'
            ],
            shortMonths: ["Jan", "Févb", "Mar", "Avr", "Mai", "Jun", "Jul", "Aoû", "Sep", "Oct", "Nov", "Déc"],
            weekdays: [
                'Dimanche', 'Lundi', 'Mardi', 'Mercredi',
                'Jeudi', 'Vendredi', 'Samedi'
            ]
    };
    const prefersDarkScheme = window.matchMedia("(prefers-color-scheme: dark)");
    if (prefersDarkScheme.matches) {
        console.log('dark');
        var options = {
            lang: lang,
            colors: ['#DDDF0D', '#7798BF', '#55BF3B', '#DF5353', '#aaeeee', '#ff0066', '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
            chart: {
                //backgroundColor: "rgb(96, 96, 96)",
                backgroundColor: "red",
                styledMode: true,
            },
            title: {
                style: {
                    color: '#fff',
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
                }
            },
            subtitle: {
                style: {
                    color: '#ddd',
                    font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
                }
            },
            legend: {
                itemStyle: {
                    font: '9pt Trebuchet MS, Verdana, sans-serif',
                    color: '#ccc'
                },
                itemHoverStyle:{
                    color: '#fff'
                }
            }
        };
        console.log('dark done');
    } else {
      var options = { lang: lang };
    }
	  setTimeout(function(){
    Highcharts.setOptions(options);
	  }, 3000);
  }); // end of document ready
})(jQuery); // end of jQuery name space
