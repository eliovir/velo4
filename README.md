Vélo4 [![Open HUB statistics](https://www.openhub.net/p/velo4/widgets/project_thin_badge.gif)](https://www.openhub.net/p/velo4/)
=====

Personal project to experiment [Rocket](https://rocket.rs/) and [Diesel](http://diesel.rs/) to build a web application using [Rust](http://www.rust-lang.org/).

## Continuous integration

The project uses Continuous integration from [Framagit](https://framagit.org/eliovir/velo4/graphs/master/ci).

[![build status](https://framagit.org/eliovir/velo4/badges/master/build.svg)](https://framagit.org/eliovir/velo4/commits/master)
[![coverage report](https://framagit.org/eliovir/velo4/badges/master/coverage.svg)](https://framagit.org/eliovir/velo4/commits/master)

You can see the `.gitlab-ci.yml` file with Rust tips.

## Install tools

### Rust nightly

with rustup

`curl https://sh.rustup.rs -sSf | sh`

### Rustfmt

`rustup component add rustfmt`

### Diesel

`sudo apt install libsqlite3-dev`

`cargo install diesel_cli --no-default-features --features sqlite`

To run migration, use:

```
export DATABASE_URL=~/docs/velo/velo4/db/db.sqlite
diesel migration run
```

NB: be careful on struct properties order vs db column order.

## Development

Make sure that you have formatted the codebase using [rustfmt](https://github.com/rust-lang/rustfmt).
`rustfmt` is a tool for formatting Rust code, which helps keep style consistent across the project.
If you have not used `rustfmt` before, it is not too difficult:

- to check coding style: `cargo fmt --all -- --check`
- to fix coding style: `cargo fmt --all --`

## Build and run

For dependencies, install `npm`, `pkg-config` and `libssl-dev`.

`sudo apt install npm pkg-config libsqlite3-dev libssl-dev`

To use Strava, create a file `.env` with the following keys, from the [Strava API page](https://www.strava.com/settings/api):


```
ROCKET_STRAVA_CLIENT_ID=
ROCKET_STRAVA_CLIENT_SECRET=
ROCKET_STRAVA_REDIRECT_URL=http://localhost:8000/auth/strava_redirect
```

* To run during development:
  ```sh
  ROOT=$(pwd)
  export DATABASE_URL=$ROOT/db/db.sqlite
  export ROCKET_DATABASES="{db={url=\""$DATABASE_URL"\"}}"
  cargo watch -x run
  ```
* To run in staging: `bin/run.sh`.
* To run in production: `bin/run.sh production`.
* To run in production server: `ssh velo4@server bash -c '/home/velo4/bin/run.sh\ production'`.

## Cross compilation

To compile for ARM, on Ubuntu:

    sudo apt-get install -y curl
    sudo apt-get install --fix-missing
    sudo apt-get install -y build-essential gcc-arm-linux-gnueabihf
    rustup target add armv7-unknown-linux-gnueabihf
    rustup override set nightly-x86_64-unknown-linux-gnu
    cargo build --target=armv7-unknown-linux-gnueabihf --release

## License

Rust is primarily distributed under the terms of both the MIT license
and the Apache License (Version 2.0), with portions covered by various
BSD-like licenses.

These codes are distributed under the MIT license.

See LICENSE for details.
