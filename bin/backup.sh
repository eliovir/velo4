#!/bin/bash
DIR=/home/backup

# echo in red
function echo_error {
	echo -e "\e[31m$@\e[0m"
}

if [ ! -d $DIR ]; then
	echo_error "$DIR is not a directory!"
	exit 1;
fi
if [ "$DATABASE_URL" == "" ]; then
	echo_error '$DATABASE_URL not set!';
	exit 1;
fi
if [ ! -f $DATABASE_URL ]; then
	echo_error "DATABASE_URL does not exist!";
	exit 1;
fi

sqlite3 $DATABASE_URL '.dump' | bzip2 -c > $DIR/velo4_`date +%y%m%d%H%M`.sql.bz2
