#!/bin/bash
if [ "$(which cargo)" = "" ]; then
	. ~/.profile;
fi
if [ ! -f secret.key ]; then
	head -c16 /dev/urandom > secret.key
fi
ROOT=$(dirname $(dirname $0))
$ROOT/bin/dependencies.sh
export DATABASE_URL=$ROOT/db/db.sqlite
export ROCKET_DATABASES="{db={url=\""$DATABASE_URL"\"}}"
if [ ! -d $(dirname $DATABASE_URL) ]; then
	mkdir $(dirname $DATABASE_URL);
fi
if [ ! -f $DATABASE_URL ]; then
	(cd $ROOT; diesel migration run);
fi
if [ "$1" != "" ]; then
	export ROCKET_ENV=$1;
fi
(cd $ROOT; cargo run);
$ROOT/bin/backup.sh
