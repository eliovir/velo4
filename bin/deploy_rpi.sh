#!/bin/bash
if [ ! -f secret.key ]; then
	head -c16 /dev/urandom > secret.key
fi
ROOT=$(realpath $(dirname $(dirname $0)))
$ROOT/bin/dependencies.sh
export DATABASE_URL=$ROOT/db/db.sqlite
export PKG_CONFIG_ALLOW_CROSS=1
export OPENSSL_DIR=$ROOT/target/openssl
if [ ! -d $OPENSSL_DIR ]; then
	mkdir -p $OPENSSL_DIR/lib &&
	mkdir -p $OPENSSL_DIR/include/openssl &&
	scp velo4@rpi:/usr/lib/arm-linux-gnueabihf/{libcrypto,libssl}.a $OPENSSL_DIR/lib &&
	scp velo4@rpi:/usr/include/arm-linux-gnueabihf/openssl/opensslconf.h $OPENSSL_DIR/include/openssl &&
	scp velo4@rpi:/usr/include/openssl/{crypto,opensslv}.h $OPENSSL_DIR/include/openssl
fi
export CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER=/usr/bin/arm-linux-gnueabihf-gcc
cargo build --target=armv7-unknown-linux-gnueabihf --release &&
(
ssh -l velo4 rpi rm velo4/target/release/velo4 &&
scp .env velo4@rpi:velo4/ &&
scp bin/run.sh bin/backup.sh velo4@rpi:velo4/bin/ &&
rsync -avz --delete migrations/ velo4@rpi:velo4/migrations/ &&
rsync -avz --delete scripts/ velo4@rpi:velo4/scripts/ &&
rsync -avz --delete static/ velo4@rpi:velo4/static/ &&
rsync -avz --delete templates/ velo4@rpi:velo4/templates/ &&
scp target/armv7-unknown-linux-gnueabihf/release/velo4 velo4@rpi:velo4/target/release
)
