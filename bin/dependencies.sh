#!/bin/bash
ROOT=$(dirname $(dirname $0))
FLUENT=$ROOT/static/js/fluent-web.js
if [ ! -f $FLUENT ]; then
	echo "Building fluent-web"
	REPO=/tmp/fluent-web;
	if [ ! -d $REPO ]; then
		echo "git clone"
		git clone https://github.com/projectfluent/fluent-web $REPO
	fi
	NODE=$(which node)
	if [ "$NODE" == "" ]; then
		echo "Patching path of node"
		export PATH=/usr/lib/nodejs-mozilla/bin:$PATH
	fi
	echo "Building $FLUENT"
	(cd $REPO;
	echo "npm install"
	npm install;
	echo "make build"
	make build)
	cp $REPO/fluent-web.js $FLUENT
fi
HIGHCHARTS_CORE=$ROOT/static/js/highcharts.js
if [ ! -f $HIGHCHARTS_CORE ]; then
	# https://code.highcharts.com/
	URL=https://code.highcharts.com/7.0.3/highcharts.js;
	wget $URL -O $HIGHCHARTS_CORE;
	wget $URL.map -O $HIGHCHARTS_CORE.map;
fi
JQUERY=$ROOT/static/js/jquery.min.js
if [ ! -f $JQUERY ]; then
	URL=https://code.jquery.com/jquery-2.1.1.min.js;
	wget $URL -O $JQUERY;
fi
LEAFLET_CSS=$ROOT/static/css/leaflet.css
if [ ! -f $LEAFLET_CSS ]; then
	URL=https://unpkg.com/leaflet@1.6.0/dist/leaflet.css;
	wget $URL -O $LEAFLET_CSS;
fi
LEAFLET_JS=$ROOT/static/js/leaflet.js
if [ ! -f $LEAFLET_JS ]; then
	URL=https://unpkg.com/leaflet@1.6.0/dist/leaflet.js;
	wget $URL -O $LEAFLET_JS;
fi
LEAFLET_FULLSCREEN_CSS=$ROOT/static/css/Control.FullScreen.css
if [ ! -f $LEAFLET_FULLSCREEN_CSS ]; then
	URL=https://github.com/brunob/leaflet.fullscreen/raw/1.6.0/Control.FullScreen.css;
	wget $URL -O $LEAFLET_FULLSCREEN_CSS;
fi
LEAFLET_FULLSCREEN_JS=$ROOT/static/js/Control.FullScreen.js
if [ ! -f $LEAFLET_FULLSCREEN_JS ]; then
	URL=https://github.com/brunob/leaflet.fullscreen/raw/1.6.0/Control.FullScreen.js;
	wget $URL -O $LEAFLET_FULLSCREEN_JS;
fi
LEAFLET_FULLSCREEN_PNG=$ROOT/static/css/icon-fullscreen.png
if [ ! -f $LEAFLET_FULLSCREEN_PNG ]; then
	URL=https://github.com/brunob/leaflet.fullscreen/raw/1.6.0/icon-fullscreen.png;
	wget $URL -O $LEAFLET_FULLSCREEN_PNG;
fi
LEAFLET_FULLSCREEN_PNG2X=$ROOT/static/css/icon-fullscreen-2x.png
if [ ! -f $LEAFLET_FULLSCREEN_PNG2X ]; then
	URL=https://github.com/brunob/leaflet.fullscreen/raw/1.6.0/icon-fullscreen-2x.png;
	wget $URL -O $LEAFLET_FULLSCREEN_PNG2X;
fi
LEAFLET_POLYLINE_ENCODED_JS=$ROOT/static/js/Polyline.encoded.js
if [ ! -f $LEAFLET_POLYLINE_ENCODED_JS ]; then
	URL=https://github.com/jieter/Leaflet.encoded/raw/0.0.9/Polyline.encoded.js;
	wget $URL -O $LEAFLET_POLYLINE_ENCODED_JS;
fi
MATERIALIZE_CSS=$ROOT/static/css/materialize.min.css
if [ ! -f $MATERIALIZE_CSS ]; then
	URL=https://raw.githubusercontent.com/Dogfalo/materialize/1.0.0/dist/css/materialize.min.css;
	wget $URL -O $MATERIALIZE_CSS;
fi
MATERIALIZE_JS=$ROOT/static/js/materialize.min.js
if [ ! -f $MATERIALIZE_JS ]; then
	URL=https://raw.githubusercontent.com/Dogfalo/materialize/1.0.0/dist/js/materialize.min.js;
	wget $URL -O $MATERIALIZE_JS;
fi
MSGPACK=$ROOT/static/js/msgpack.min.js
if [ ! -f $MSGPACK ]; then
	URL=https://raw.githubusercontent.com/ygoe/msgpack.js/master/msgpack.min.js;
	wget $URL -O $MSGPACK;
	wget $URL.map -O $MSGPACK.map;
fi
RAINBOW_JS=$ROOT/static/js/rainbowvis.js
if [ ! -f $RAINBOW_JS ]; then
	URL=https://raw.githubusercontent.com/anomal/RainbowVis-JS/master/rainbowvis.js;
	wget $URL -O $RAINBOW_JS;
fi
ICONS=$ROOT/static/css/material_icons.css
if [ ! -f $ICONS ]; then
	URL=https://raw.githubusercontent.com/google/material-design-icons/master/iconfont/material-icons.css
	wget $URL -O - | sed -e "s:url(MaterialIcons-Regular:url(/fonts/MaterialIcons-Regular:g" > $ICONS;
fi
FONTS=$ROOT/static/fonts
mkdir -p $FONTS/roboto
ROBOTO=https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/fonts/
for EXT in ttf woff woff2; do
	for FONT in Bold Light Medium Regular Thin; do
		FILE=roboto/Roboto-${FONT}.$EXT ;
		if [ ! -f $FONTS/$FILE ]; then
			wget $ROBOTO/$FILE -O $FONTS/$FILE ;
		fi
	done
	if [ ! -f $FONTS/MaterialIcons-Regular.$EXT ]; then
		wget https://raw.githubusercontent.com/google/material-design-icons/master/iconfont/MaterialIcons-Regular.$EXT -O $FONTS/MaterialIcons-Regular.$EXT;
	fi
done
