#!/usr/bin/env python3

import json
import logging
import os
import sqlite3
from urllib.error import HTTPError
from urllib.request import Request, urlopen

class Client:

    def __init__(self, conn):
        self._base_url = "https://www.strava.com/api/v3/"
        self._con = conn
        self._logger = logging.getLogger(__name__)
        c = conn.cursor()
        c.execute("SELECT access_token FROM stravauser")
        self._access_token = c.fetchone()[0]
        self._urlopenargs = {}
        print(self._access_token)

    def _request(self, req):
        req.add_header('Authorization', 'Bearer ' + self._access_token)
        try:
            with urlopen(req, **self._urlopenargs) as response:
                encoding = response.info().get_content_charset('utf-8')
                self._logger.debug("url: %s" % response.url)
                self._logger.debug("headers: %s" % response.headers)
                self._logger.debug("reason: %s" % response.reason)
                self._logger.debug("status: %d" % response.status)
                self._logger.debug("encoding: %s" % encoding)
                data = response.read()
                self._logger.debug("data: %s" % data)
                return json.loads(data.decode(encoding))
        except HTTPError as e:
            self._logger.error("error: %s" % e)
            self._logger.error("url: %s" % e.url)
            self._logger.error("headers: %s" % e.headers)
            self._logger.error("reason: %s" % e.reason)
            self._logger.error("status: %d" % e.status)
            data = e.read()
            self._logger.error("data: %s" % data)
            return None

    def get(self, resource, parameters=None):
        if parameters:
            query = '?' + parse.urlencode(parameters)
        else:
            query = ""
        req = Request(self._base_url + resource + query)
        req.add_header('Content-Type', 'application/json; charset=utf-8')
        return self._request(req)

def main():
    """
    Launcher.
    """
    conn = sqlite3.connect(os.path.expanduser(os.environ['DATABASE_URL']))
    client = Client(conn)
    c = conn.cursor()
    insert_sql = "INSERT INTO stravamap (id, polyline, summary_polyline) VALUES (?, ?, ?)"
    update_sql = "UPDATE stravaactivity SET stravamap=? WHERE id=?"
    #for activity in activities:
    c.execute("SELECT id FROM stravaactivity WHERE stravamap IS NULL")
    for row in c.fetchall():
        activity = client.get("/activities/%d" % row[0])
        if not "map" in activity:
            print("activity #%s does not have map" % activity["id"])
            c.execute(update_sql, (' ', activity["id"]))
            continue
        print("activity #%s has map %s" % (activity["id"], activity["map"]["id"]))
        polyline = ''
        summary_polyline = ''
        if "polyline" in activity["map"]:
            print("- activity has polyline")
            polyline = activity["map"]["polyline"]
        if "summary_polyline" in activity["map"]:
            print("- activity has summary_polyline")
            summary_polyline = activity["map"]["summary_polyline"]
        c.execute(insert_sql, (activity["map"]["id"], polyline, summary_polyline))
        c.execute(update_sql, (activity["map"]['id'], activity["id"]))
    conn.commit()

if __name__ == "__main__":
    main()
