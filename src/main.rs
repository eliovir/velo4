#![feature(proc_macro_hygiene, decl_macro)]
#![recursion_limit = "256"]

extern crate chrono;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_derive_enum;
extern crate dotenv;
#[macro_use]
extern crate dotenv_codegen;
extern crate jsonwebtoken;
extern crate random_string;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
extern crate rocket_slog;
#[macro_use]
extern crate serde_derive;
extern crate serdeconv;
#[macro_use(debug)]
extern crate slog;
extern crate sloggers;

use std::collections::HashMap;
use std::error::Error;
use std::process;
use std::thread;
use std::time;

use chrono::Local;
use dotenv::dotenv;
use rocket::fairing::AdHoc;
use rocket::response::Redirect;
use rocket::{Request, State};
use rocket_contrib::templates::Template;
use rocket_slog::SlogFairing;
use sloggers::{Config, LoggerConfig};

use controllers::activities;
use controllers::auth;
use controllers::bikes;
use controllers::healths;
use controllers::maintenances;
use controllers::settings;
use controllers::static_files;
use controllers::statistics;
use models::database::Database;
use request::jwt::UserRolesToken;
use strava::config::StravaConfig;

mod controllers;
mod models;
mod request;
mod response;
mod strava;

include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[derive(Serialize)]
struct ApplicationInfo<'a> {
    build_date: &'a str,
    version: &'a str,
    commit_date: Option<&'a str>,
    commit_revision: Option<&'a str>,
    rust_version: Option<&'a str>,
    start: &'a str,
}

#[get("/halt")]
fn halt() -> &'static str {
    thread::spawn(move || {
        let ten_millis = time::Duration::from_millis(10);
        thread::sleep(ten_millis);
        process::exit(1);
    });
    "halting"
}

#[get("/")]
fn index(_urt: UserRolesToken) -> Redirect {
    Redirect::permanent("/home")
}

#[get("/", rank = 2)]
fn index_login() -> Redirect {
    Redirect::to("/auth/login")
}

#[get("/home")]
fn home(_urt: UserRolesToken) -> Template {
    let mut map = HashMap::new();
    map.insert("path", "TODO".to_string());
    Template::render("home", &map)
}

#[get("/home", rank = 2)]
fn home_login() -> Redirect {
    Redirect::to("/auth/login")
}

#[get("/info")]
fn info(application_info: State<ApplicationInfo>) -> Template {
    Template::render("info", application_info.inner())
}

#[catch(404)]
fn not_found(req: &Request) -> Template {
    let mut map = HashMap::new();
    map.insert("path", req.uri().to_string());
    Template::render("error_404", &map)
}

fn main() -> Result<(), Box<dyn Error>> {
    let start = Local::now().format("%Y-%m-%d %H:%M:%S %z").to_string();
    let application_info = ApplicationInfo {
        build_date: APP_BUILD_DATE,
        version: APP_VERSION,
        commit_date: GIT_BUILD_DATE,
        commit_revision: GIT_BUILD_VERSION,
        rust_version: RUSTC_VERSION,
        start: Box::leak(start.into_boxed_str()),
    };
    dotenv().ok();
    let config: LoggerConfig = serdeconv::from_toml_str(
        r#"
type = "file" # terminal or file
format = "full" # full or compact
source_location = "module_and_line" # none or module_and_line
timezone = "local" # utc or local
level = "debug" # one of trace, debug, info, warning, error, critical
path = "/var/log/velo4/velo4.log"
rotate_size = 25600
rotate_keep = 10
# rotate_compress = true
"#,
    )
    .unwrap();
    let logger = config.build_logger().unwrap();
    let fairing = SlogFairing::new(logger.clone());
    rocket::ignite()
        .mount(
            "/",
            routes![
                home,
                home_login,
                halt,
                index,
                index_login,
                info,
                static_files::css,
                static_files::favicon,
                static_files::fonts,
                static_files::head_js,
                static_files::images,
                static_files::js,
                static_files::locales
            ],
        )
        .mount(
            "/activities",
            routes![
                activities::all,
                activities::create,
                activities::create_get,
                activities::delete,
                activities::filter,
                activities::read,
                activities::read_map,
                activities::stravaactivities,
                activities::strava_add,
                activities::strava_associate_get,
                activities::strava_associate_post,
                activities::update
            ],
        )
        .mount(
            "/auth",
            routes![
                auth::activate_get,
                auth::activate_post,
                auth::device_code,
                auth::login_get,
                auth::login_post,
                auth::logout,
                auth::password_get,
                auth::password_post,
                auth::strava_connect,
                auth::strava_disconnect,
                auth::strava_login,
                auth::strava_redirect,
                auth::strava_redirect_connect,
                auth::token
            ],
        )
        .mount(
            "/bikes",
            routes![
                bikes::all,
                bikes::create,
                bikes::delete,
                bikes::read,
                bikes::update
            ],
        )
        .mount(
            "/healths",
            routes![
                healths::all,
                healths::create,
                healths::create_get,
                healths::create_weight,
                healths::delete,
                healths::read,
                healths::update,
                healths::weights
            ],
        )
        .mount(
            "/maintenances",
            routes![
                maintenances::all,
                maintenances::create,
                maintenances::create_get,
                maintenances::delete,
                maintenances::filter,
                maintenances::read,
                maintenances::update
            ],
        )
        .mount("/settings", routes![settings::profile])
        .mount(
            "/statistics",
            routes![
                statistics::index,
                statistics::annual,
                statistics::annual_json,
                statistics::general_year,
                statistics::general,
                statistics::histogram,
                statistics::histogram_json,
                statistics::monthly,
                statistics::monthly_msgpack,
                statistics::scatter,
                statistics::scatter_json,
                statistics::timeline
            ],
        )
        .register(catchers![not_found])
        .attach(Database::fairing())
        .attach(Template::fairing())
        .attach(AdHoc::on_attach("Token Config", |rocket| {
            let conf = rocket.config().clone();
            let strava_config = StravaConfig::from_env(&conf);
            println!("Strava={}", strava_config);
            Ok(rocket.manage(strava_config))
        }))
        .attach(fairing)
        .manage(application_info)
        .launch();
    Ok(())
}
