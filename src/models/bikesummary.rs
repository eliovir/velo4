use diesel::prelude::*;

use models::schema::v_bikesummary;
use models::schema::v_bikesummary::dsl::v_bikesummary as items;

#[derive(Debug, Queryable, Serialize)]
pub struct BikeSummary {
    pub bike_id: i32,
    pub bike_name: String,
    pub distance: f64,
    pub activities: i32,
    pub first_activity: String,
    pub year_distance: f64,
    pub year_activities: i32,
}

impl BikeSummary {
    pub fn all(conn: &SqliteConnection) -> Vec<BikeSummary> {
        items
            .order(v_bikesummary::bike_name.asc())
            .load::<BikeSummary>(conn)
            .unwrap()
    }
}
