use diesel;
use diesel::prelude::*;
use models::schema::oauthclient;
use models::schema::oauthclient::dsl::oauthclient as oauthclients;
use models::schema::oauthdevicecode;
use models::schema::oauthdevicecode::dsl::oauthdevicecode as oauthdevicecodes;
use models::schema::oauthtoken;
use models::schema::oauthtoken::dsl::oauthtoken as oauthtokens;

#[derive(AsChangeset, Debug, FromForm, Identifiable, Insertable, Queryable, Serialize)]
#[table_name = "oauthclient"]
pub struct OAuthClient {
    pub id: Option<i32>,
    pub user: i32,
    pub client_id: String,
    pub name: String,
}
impl OAuthClient {
    pub fn find(conn: &SqliteConnection, id: i32) -> QueryResult<OAuthClient> {
        oauthclients.find(id).first(conn)
    }
    pub fn find_by_client_id(
        conn: &SqliteConnection,
        client_id: String,
    ) -> QueryResult<OAuthClient> {
        oauthclients
            .filter(oauthclient::dsl::client_id.eq(client_id))
            .first(conn)
    }
}

#[derive(AsChangeset, Debug, FromForm, Identifiable, Insertable, Queryable, Serialize)]
#[table_name = "oauthdevicecode"]
pub struct OAuthDeviceCode {
    pub id: Option<i32>,
    pub oauthclient: i32,
    pub user_code: String,
    pub device_code: String,
    pub user: Option<i32>,
}
impl OAuthDeviceCode {
    pub fn delete_with_id(conn: &SqliteConnection, id: i32) -> bool {
        diesel::delete(oauthdevicecodes.find(id))
            .execute(conn)
            .is_ok()
    }
    pub fn find(conn: &SqliteConnection, id: i32) -> QueryResult<OAuthDeviceCode> {
        oauthdevicecodes.find(id).first(conn)
    }
    pub fn find_by_oauthclient_and_device_code(
        conn: &SqliteConnection,
        oauthclient: i32,
        device_code: String,
    ) -> QueryResult<OAuthDeviceCode> {
        oauthdevicecodes
            .filter(oauthdevicecode::dsl::oauthclient.eq(oauthclient))
            .filter(oauthdevicecode::dsl::device_code.eq(device_code))
            .first(conn)
    }
    pub fn find_by_user_code(
        conn: &SqliteConnection,
        user_code: String,
    ) -> QueryResult<OAuthDeviceCode> {
        oauthdevicecodes
            .filter(oauthdevicecode::dsl::user_code.eq(user_code))
            .first(conn)
    }
    pub fn insert(&self, conn: &SqliteConnection) -> QueryResult<OAuthDeviceCode> {
        match diesel::insert_into(oauthdevicecode::table)
            .values(self)
            .execute(conn)
        {
            Ok(_) => OAuthDeviceCode::find_by_oauthclient_and_device_code(
                conn,
                self.oauthclient,
                self.device_code.to_string(),
            ),
            Err(err) => Err(err),
        }
    }
    pub fn set_user(&self, conn: &SqliteConnection, user_id: i32) -> bool {
        diesel::update(oauthdevicecodes.filter(oauthdevicecode::dsl::id.eq(self.id)))
            .set((oauthdevicecode::dsl::user.eq(user_id),))
            .execute(conn)
            .unwrap_or(0)
            == 1
    }
}

#[derive(AsChangeset, Debug, FromForm, Identifiable, Insertable, Queryable, Serialize)]
#[table_name = "oauthtoken"]
pub struct OAuthToken {
    pub id: Option<i32>,
    pub oauthclient: i32,
    pub user: i32,
    pub access_token: String,
    pub expires_at: i64,
    pub refresh_token: String,
}
impl OAuthToken {
    pub fn find_by_access_token(
        conn: &SqliteConnection,
        access_token: String,
    ) -> QueryResult<OAuthToken> {
        oauthtokens
            .filter(oauthtoken::dsl::access_token.eq(access_token))
            .first(conn)
    }
    pub fn insert(&self, conn: &SqliteConnection) -> QueryResult<OAuthToken> {
        match diesel::insert_into(oauthtoken::table)
            .values(self)
            .execute(conn)
        {
            Ok(_) => OAuthToken::find_by_access_token(conn, self.access_token.to_owned()),
            Err(err) => Err(err),
        }
    }
    pub fn set_access_token(
        &self,
        conn: &SqliteConnection,
        access_token: String,
        expires_at: i64,
    ) -> bool {
        diesel::update(oauthtokens.filter(oauthtoken::dsl::id.eq(self.id)))
            .set((
                oauthtoken::dsl::access_token.eq(access_token),
                oauthtoken::dsl::expires_at.eq(expires_at),
            ))
            .execute(conn)
            .unwrap_or(0)
            == 1
    }
}
