table! {
    v_bikesummary (bike_id) {
        bike_id -> Integer,
        bike_name -> Text,
        distance -> Double,
        activities -> Integer,
        first_activity -> Timestamp,
        year_distance -> Double,
        year_activities -> Integer,
    }
}

table! {
    activity (id) {
        id -> Nullable<Integer>,
        date -> Text,
        distance -> Double,
        duration -> Text,
        average -> Nullable<Double>,
        max_speed -> Nullable<Double>,
        wind -> Integer,
        temperature -> Integer,
        description -> Nullable<Text>,
        ascent -> Integer,
        bike -> Integer,
        user -> Integer,
        intensity -> Text,
        total -> Nullable<Double>,
    }
}

table! {
    bike (id) {
        id -> Nullable<Integer>,
        user -> Integer,
        name -> Text,
        enabled -> Bool,
    }
}

table! {
    health (id) {
        id -> Nullable<Integer>,
        date -> Text,
        user -> Integer,
        weight -> Double,
        fat_content -> Nullable<Double>,
        water_content -> Nullable<Double>,
    }
}

table! {
    maintenance (id) {
        id -> Nullable<Integer>,
        bike -> Integer,
        date -> Text,
        description -> Text,
    }
}

table! {
    oauthclient (id) {
        id -> Nullable<Integer>,
        user -> Integer,
        client_id -> Text,
        name -> Text,
    }
}

table! {
    oauthdevicecode (id) {
        id -> Nullable<Integer>,
        oauthclient -> Integer,
        device_code -> Text,
        user_code -> Text,
        user -> Nullable<Integer>,
    }
}

table! {
    oauthtoken (id) {
        id -> Nullable<Integer>,
        oauthclient -> Integer,
        user -> Integer,
        access_token -> Text,
        expires_at -> BigInt,
        refresh_token -> Text,
    }
}

table! {
    polar (id) {
        id -> Nullable<Integer>,
        activity -> Integer,
        path -> Nullable<Text>,
        average_hr -> Nullable<Integer>,
        max_hr -> Nullable<Integer>,
        energy -> Nullable<Integer>,
        ascent -> Nullable<Integer>,
        min_temperature -> Nullable<Integer>,
        average_temperature -> Nullable<Integer>,
        max_temperature -> Nullable<Integer>,
        min_altitude -> Nullable<Integer>,
        average_altitude -> Nullable<Integer>,
        max_altitude -> Nullable<Integer>,
        average_cadence -> Nullable<Integer>,
        max_cadence -> Nullable<Integer>,
    }
}

table! {
    road (id) {
        id -> Nullable<Integer>,
        name -> Text,
        department -> Integer,
        altitude -> Nullable<Integer>,
    }
}

table! {
    route (id) {
        id -> Nullable<Integer>,
        activity -> Integer,
        route_order -> Integer,
        road -> Integer,
    }
}

table! {
    use diesel::sql_types::{BigInt,Bool,Double,Integer,Nullable,Text};
    use models::stravaactivity::StravaActivityTypeMapping;
    stravaactivity (id) {
        id -> BigInt,
        activity -> Nullable<Integer>,
        external_id -> Text,
        upload_id -> BigInt,
        name -> Text,
        distance -> Double,
        moving_time -> Integer,
        elapsed_time -> Integer,
        total_elevation_gain -> Double,
        elev_high -> Double,
        elev_low -> Nullable<Double>,
        activitytype -> StravaActivityTypeMapping,
        start_date -> Text,
        start_date_local -> Text,
        timezone -> Text,
        achievement_count -> Integer,
        kudos_count -> Integer,
        comment_count -> Integer,
        athlete_count -> Nullable<Integer>,
        photo_count -> Integer,
        total_photo_count -> Integer,
        stravamap -> Nullable<Text>,
        trainer -> Bool,
        average_speed -> Double,
        max_speed -> Double,
        average_cadence -> Nullable<Double>,
        average_heartrate -> Nullable<Double>,
        max_heartrate -> Nullable<Double>,
        average_temp -> Nullable<Double>,
        gear_id -> Text,
        kilojoules -> Nullable<Double>,
        average_watts -> Nullable<Double>,
        device_watts -> Nullable<Bool>,
        max_watts -> Nullable<Integer>,
        weighted_average_watts -> Nullable<Integer>,
        description -> Nullable<Text>,
        calories -> Double,
        device_name -> Nullable<Text>,
    }
}

table! {
    stravamap (id) {
        id -> Text,
        polyline -> Nullable<Text>,
        summary_polyline -> Nullable<Text>,
    }
}

table! {
    stravauser (id) {
        id -> Nullable<Integer>,
        user -> Integer,
        access_token -> Text,
        expires_at -> Integer,
        refresh_token -> Text,
        token_type -> Text,
    }
}

table! {
    user (id) {
        id -> Nullable<Integer>,
        login -> Text,
        name -> Nullable<Text>,
        password -> Text,
        rank -> Nullable<Integer>,
        active -> Nullable<Text>,
        inactive -> Nullable<Text>,
    }
}

joinable!(activity -> bike (bike));
joinable!(activity -> user (user));
joinable!(bike -> user (user));
joinable!(health -> user (user));
joinable!(maintenance -> bike (bike));
joinable!(oauthclient -> user (user));
joinable!(oauthdevicecode -> oauthclient (oauthclient));
joinable!(oauthdevicecode -> user (user));
joinable!(oauthtoken -> oauthclient (oauthclient));
joinable!(oauthtoken -> user (user));
joinable!(polar -> activity (activity));
joinable!(route -> road (road));
joinable!(stravaactivity -> activity (activity));
joinable!(stravaactivity -> stravamap (stravamap));
joinable!(stravauser -> user (user));

allow_tables_to_appear_in_same_query!(
    activity,
    bike,
    health,
    maintenance,
    oauthclient,
    oauthdevicecode,
    oauthtoken,
    polar,
    road,
    route,
    stravaactivity,
    stravamap,
    stravauser,
    user,
);
