use diesel;
use diesel::prelude::*;
use diesel::result::DatabaseErrorKind;
use diesel::result::Error::DatabaseError;

use models::schema::bike;
use models::schema::bike::dsl::bike as all_bikes;

#[derive(Debug, FromForm, Identifiable, Insertable, Queryable, Serialize)]
#[table_name = "bike"]
pub struct Bike {
    pub id: Option<i32>,
    pub user: i32,
    pub name: String,
    pub enabled: bool,
}

impl Bike {
    pub fn find_all_by_user(conn: &SqliteConnection, user: i32) -> Vec<Bike> {
        all_bikes
            .filter(bike::dsl::user.eq(user))
            .order(bike::id.desc())
            .load::<Bike>(conn)
            .unwrap()
    }
    pub fn find_all_enabled_by_user(conn: &SqliteConnection, user: i32) -> Vec<Bike> {
        all_bikes
            .filter(bike::dsl::user.eq(user))
            .filter(bike::dsl::enabled.eq(true))
            .order(bike::id.desc())
            .load::<Bike>(conn)
            .unwrap()
    }
    pub fn delete_with_id(conn: &SqliteConnection, id: i32) -> bool {
        diesel::delete(all_bikes.find(id)).execute(conn).is_ok()
    }
    pub fn find_with_name_user(
        conn: &SqliteConnection,
        name: &String,
        user: i32,
    ) -> QueryResult<Bike> {
        all_bikes
            .filter(bike::dsl::name.eq(name))
            .filter(bike::dsl::user.eq(user))
            .first::<Bike>(conn)
    }
    pub fn find(conn: &SqliteConnection, id: i32) -> QueryResult<Bike> {
        all_bikes.find(id).first(conn)
    }
    pub fn insert(&self, conn: &SqliteConnection) -> QueryResult<Bike> {
        match diesel::insert_into(bike::table).values(self).execute(conn) {
            Ok(_) => Bike::find_with_name_user(conn, &self.name, self.user),
            Err(err) => Err(err),
        }
    }
    pub fn update(&self, conn: &SqliteConnection, update: Bike) -> QueryResult<Bike> {
        if let Some(id) = self.id {
            if diesel::update(self)
                .set((
                    bike::dsl::name.eq(&update.name),
                    bike::dsl::enabled.eq(&update.enabled),
                ))
                .execute(conn)
                .is_ok()
            {
                Bike::find(conn, id)
            } else {
                let msg = String::from("Failed to update bike!");
                let error_kind = DatabaseErrorKind::__Unknown;
                Err(DatabaseError(error_kind, Box::new(msg)))
            }
        } else {
            let msg = String::from("No bike.id!");
            let error_kind = DatabaseErrorKind::__Unknown;
            Err(DatabaseError(error_kind, Box::new(msg)))
        }
    }
}
