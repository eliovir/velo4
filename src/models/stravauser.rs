use std::convert::TryInto;

use chrono::prelude::*;
use diesel;
use diesel::prelude::*;

use models::schema::stravauser;
use models::schema::stravauser::dsl::stravauser as items;

#[derive(AsChangeset, Debug, FromForm, Identifiable, Insertable, Queryable, Serialize)]
#[table_name = "stravauser"]
pub struct StravaUser {
    pub id: Option<i32>,
    pub user: i32,
    pub access_token: String,
    pub expires_at: i32,
    pub refresh_token: String,
    pub token_type: String,
}

impl StravaUser {
    pub fn delete_by_id(conn: &SqliteConnection, id: i32) -> usize {
        diesel::delete(items.filter(stravauser::dsl::id.eq(id)))
            .execute(conn)
            .expect("Error deleting StravaUser")
    }
    pub fn find_all_by_user(conn: &SqliteConnection, user: i32) -> QueryResult<Vec<StravaUser>> {
        items
            .filter(stravauser::dsl::user.eq(user))
            .load::<StravaUser>(conn)
    }
    pub fn find_by_id(conn: &SqliteConnection, id: i32) -> QueryResult<StravaUser> {
        items.filter(stravauser::dsl::id.eq(id)).first(conn)
    }
    pub fn find_by_user(conn: &SqliteConnection, user: i32) -> QueryResult<StravaUser> {
        items.filter(stravauser::dsl::user.eq(user)).first(conn)
    }
    pub fn insert(&self, conn: &SqliteConnection) -> QueryResult<StravaUser> {
        println!("Inserting StravaUser: {:?}", self);
        match diesel::insert_into(stravauser::table)
            .values(self)
            .execute(conn)
        {
            Ok(_) => StravaUser::find_by_id(conn, self.id.unwrap()),
            Err(err) => Err(err),
        }
    }
    pub fn is_expired(&self) -> bool {
        self.expires_at <= Utc::now().timestamp().try_into().unwrap()
    }
    pub fn update_token(&self, conn: &SqliteConnection) -> QueryResult<usize> {
        diesel::update(items.filter(stravauser::dsl::id.eq(self.id)))
            .set((
                stravauser::dsl::access_token.eq(self.access_token.to_string()),
                stravauser::dsl::expires_at.eq(self.expires_at),
                stravauser::dsl::refresh_token.eq(self.refresh_token.to_string()),
            ))
            .execute(conn)
    }
}
