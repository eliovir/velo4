use diesel;
use diesel::prelude::*;
use diesel::result::DatabaseErrorKind;
use diesel::result::Error::DatabaseError;

use models::schema::bike;
use models::schema::maintenance;
use models::schema::maintenance::dsl::maintenance as maintenances;

#[derive(Debug, FromForm, Identifiable, Insertable, Queryable, Serialize)]
#[table_name = "maintenance"]
pub struct Maintenance {
    pub id: Option<i32>,
    pub bike: i32,
    pub date: String,
    pub description: String,
}

impl Maintenance {
    pub fn find_all_by_bike(conn: &SqliteConnection, bike: i32) -> Vec<Maintenance> {
        maintenances
            .filter(maintenance::dsl::bike.eq(bike))
            .order(maintenance::date.desc())
            .load::<Maintenance>(conn)
            .unwrap()
    }
    pub fn find_all_by_user(conn: &SqliteConnection, user: i32) -> Vec<Maintenance> {
        maintenance::table
            .inner_join(bike::table)
            .select((
                maintenance::id,
                maintenance::bike,
                maintenance::date,
                maintenance::description,
            ))
            .filter(bike::dsl::user.eq(user))
            .order(maintenance::date.desc())
            .load::<Maintenance>(conn)
            .unwrap()
    }
    pub fn find_by_bike_and_date(
        conn: &SqliteConnection,
        bike: i32,
        date: &String,
    ) -> QueryResult<Maintenance> {
        maintenances
            .filter(maintenance::dsl::bike.eq(bike))
            .filter(maintenance::dsl::date.eq(date))
            .first(conn)
    }
    pub fn find_all_descriptions_by_user(conn: &SqliteConnection, user: i32) -> Vec<String> {
        maintenance::table
            .inner_join(bike::table)
            .select(maintenance::description)
            .distinct()
            .filter(bike::dsl::user.eq(user))
            .order(maintenance::description.asc())
            .load::<String>(conn)
            .unwrap()
    }
    pub fn find_last_with_user(conn: &SqliteConnection, user: i32) -> QueryResult<Maintenance> {
        maintenance::table
            .inner_join(bike::table)
            .select((
                maintenance::id,
                maintenance::bike,
                maintenance::date,
                maintenance::description,
            ))
            .filter(bike::dsl::user.eq(user))
            .order(maintenance::dsl::date.desc())
            .first(conn)
    }
    pub fn delete_with_id(conn: &SqliteConnection, id: i32) -> bool {
        diesel::delete(maintenances.find(id)).execute(conn).is_ok()
    }
    pub fn find_next(
        conn: &SqliteConnection,
        maintenance: &Maintenance,
    ) -> QueryResult<Maintenance> {
        maintenances
            .filter(maintenance::dsl::bike.eq(maintenance.bike))
            .filter(maintenance::dsl::date.gt(maintenance.date.to_string()))
            .order(maintenance::dsl::date.asc())
            .first(conn)
    }
    pub fn find_previous(
        conn: &SqliteConnection,
        maintenance: &Maintenance,
    ) -> QueryResult<Maintenance> {
        maintenances
            .filter(maintenance::dsl::bike.eq(maintenance.bike))
            .filter(maintenance::dsl::date.lt(maintenance.date.to_string()))
            .order(maintenance::dsl::date.desc())
            .first(conn)
    }
    pub fn find(conn: &SqliteConnection, id: i32) -> QueryResult<Maintenance> {
        maintenances.find(id).first(conn)
    }
    pub fn insert(&self, conn: &SqliteConnection) -> QueryResult<Maintenance> {
        match diesel::insert_into(maintenance::table)
            .values(self)
            .execute(conn)
        {
            Ok(_) => maintenances.order(maintenance::id.desc()).first(conn),
            Err(err) => Err(err),
        }
    }
    pub fn update(&self, conn: &SqliteConnection, update: Maintenance) -> QueryResult<Maintenance> {
        if let Some(id) = self.id {
            if diesel::update(self)
                .set((
                    maintenance::dsl::date.eq(&update.date),
                    maintenance::dsl::bike.eq(&update.bike),
                    maintenance::dsl::description.eq(&update.description),
                ))
                .execute(conn)
                .is_ok()
            {
                Maintenance::find(conn, id)
            } else {
                let msg = String::from("Failed to update Maintenance!");
                let error_kind = DatabaseErrorKind::__Unknown;
                Err(DatabaseError(error_kind, Box::new(msg)))
            }
        } else {
            let msg = String::from("No Maintenance.id!");
            let error_kind = DatabaseErrorKind::__Unknown;
            Err(DatabaseError(error_kind, Box::new(msg)))
        }
    }
}
