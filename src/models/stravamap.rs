#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(StravaActivity)]
#[table_name = "stravamap"]
pub struct StravaMap {
        id: i32,
        polyline: String,
        summary_polyline: String,
}
