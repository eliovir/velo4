use chrono::Duration;
use chrono::NaiveTime;
use diesel;
use diesel::prelude::*;
use diesel::result::DatabaseErrorKind;
use diesel::result::Error::DatabaseError;
use diesel::sql_query;

use models::schema::activity::dsl::activity as activities;
use models::schema::{activity, polar};
use models::stravaactivity::StravaActivity;

#[derive(
    AsChangeset,
    Clone,
    Debug,
    FromForm,
    Identifiable,
    Insertable,
    Queryable,
    QueryableByName,
    Serialize,
)]
#[table_name = "activity"]
pub struct Activity {
    pub id: Option<i32>,
    pub date: String,
    pub distance: f64,
    pub duration: String,
    pub average: Option<f64>,
    pub max_speed: Option<f64>,
    pub wind: i32,
    pub temperature: i32,
    pub description: Option<String>,
    pub ascent: i32,
    pub bike: i32,
    pub user: i32,
    pub intensity: String,
    pub total: Option<f64>,
}

#[derive(Debug, FromForm, Identifiable, Insertable, Queryable, Serialize)]
#[table_name = "polar"]
pub struct Polar {
    pub id: Option<i32>,
    pub activity: i32,
    pub path: Option<String>,
    pub average_hr: Option<i32>,
    pub max_hr: Option<i32>,
    pub energy: Option<i32>,
    pub ascent: Option<i32>,
    pub min_temperature: Option<i32>,
    pub average_temperature: Option<i32>,
    pub max_temperature: Option<i32>,
    pub min_altitude: Option<i32>,
    pub average_altitude: Option<i32>,
    pub max_altitude: Option<i32>,
    pub average_cadence: Option<i32>,
    pub max_cadence: Option<i32>,
}

impl Activity {
    pub fn find_all_by_user(conn: &SqliteConnection, user: i32) -> Vec<Activity> {
        activities
            .filter(activity::dsl::user.eq(user))
            .order(activity::date.desc())
            .load::<Activity>(conn)
            .unwrap()
    }
    pub fn find_all_by_user_and_bike_and_description_and_distance_and_year(
        conn: &SqliteConnection,
        user: i32,
        bike: Option<i32>,
        description: Option<String>,
        distance_min: Option<f64>,
        distance_max: Option<f64>,
        year: Option<f64>,
    ) -> Vec<Activity> {
        let mut expression = activity::table.into_boxed();
        expression = expression.filter(activity::dsl::user.eq(user));
        if let Some(b) = bike {
            expression = expression.filter(activity::dsl::bike.eq(b));
        }
        if let Some(descr) = description {
            expression =
                expression.filter(activity::dsl::description.like("%".to_owned() + &descr + "%"));
        }
        if let Some(min) = distance_min {
            expression = expression.filter(activity::dsl::distance.ge(min));
        }
        if let Some(max) = distance_max {
            expression = expression.filter(activity::dsl::distance.le(max));
        }
        if let Some(the_year) = year {
            expression = expression.filter(activity::dsl::date.like(the_year.to_string() + "-%"));
        }
        expression
            .order(activity::date.desc())
            .load::<Activity>(conn)
            .unwrap()
    }
    pub fn find_all_by_user_and_day(
        conn: &SqliteConnection,
        user: i32,
        day: String,
    ) -> Vec<Activity> {
        let sql_expression = format!(
        "SELECT * FROM activity WHERE STRFTIME('%Y-%m-%d', date)='{}' AND user={} ORDER BY date",
        day,
        user
    );
        let query = sql_query(sql_expression);
        query
            .get_results::<Activity>(&*conn)
            .expect("Can't query activity")
    }
    pub fn delete_with_id(conn: &SqliteConnection, id: i32) -> bool {
        diesel::delete(activities.find(id)).execute(conn).is_ok()
    }
    pub fn find_last_with_user(conn: &SqliteConnection, user: i32) -> QueryResult<Activity> {
        activities
            .filter(activity::dsl::user.eq(user))
            .order(activity::dsl::date.desc())
            .first(conn)
    }
    pub fn find_next(conn: &SqliteConnection, activity: &Activity) -> QueryResult<Activity> {
        activities
            .filter(activity::dsl::user.eq(activity.user))
            .filter(activity::dsl::date.gt(activity.date.to_string()))
            .order(activity::dsl::date.asc())
            .first(conn)
    }
    pub fn find_previous(conn: &SqliteConnection, activity: &Activity) -> QueryResult<Activity> {
        activities
            .filter(activity::dsl::user.eq(activity.user))
            .filter(activity::dsl::date.lt(activity.date.to_string()))
            .order(activity::dsl::date.desc())
            .first(conn)
    }
    pub fn find_with_date_user(
        conn: &SqliteConnection,
        date: &String,
        user: i32,
    ) -> QueryResult<Activity> {
        activities
            .filter(activity::dsl::date.eq(date))
            .filter(activity::dsl::user.eq(user))
            .first::<Activity>(conn)
    }
    pub fn find(conn: &SqliteConnection, id: i32) -> QueryResult<Activity> {
        activities.find(id).first(conn)
    }
    pub fn insert(&self, conn: &SqliteConnection) -> QueryResult<Activity> {
        match diesel::insert_into(activity::table)
            .values(self)
            .execute(conn)
        {
            Ok(_) => Activity::find_with_date_user(conn, &self.date, self.user),
            Err(err) => Err(err),
        }
    }
    pub fn update(&self, conn: &SqliteConnection) -> QueryResult<usize> {
        if self.id.is_none() {
            let msg = String::from("No Activity.id!");
            let error_kind = DatabaseErrorKind::__Unknown;
            return Err(DatabaseError(error_kind, Box::new(msg)));
        }
        diesel::update(activities.filter(activity::dsl::id.eq(self.id)))
            .set(self)
            .execute(conn)
    }
    pub fn update_with(&self, conn: &SqliteConnection, update: Activity) -> QueryResult<Activity> {
        if self.id.is_none() {
            let msg = String::from("No Activity.id!");
            let error_kind = DatabaseErrorKind::__Unknown;
            return Err(DatabaseError(error_kind, Box::new(msg)));
        }
        let id = self.id.unwrap();
        if diesel::update(activities.filter(activity::dsl::id.eq(id)))
            .set((
                activity::dsl::date.eq(&update.date),
                activity::dsl::distance.eq(&update.distance),
                activity::dsl::duration.eq(&update.duration),
                activity::dsl::average.eq(&update.average),
                activity::dsl::max_speed.eq(&update.max_speed),
                activity::dsl::wind.eq(&update.wind),
                activity::dsl::temperature.eq(&update.temperature),
                activity::dsl::description.eq(&update.description),
                activity::dsl::ascent.eq(&update.ascent),
                activity::dsl::bike.eq(&update.bike),
                activity::dsl::user.eq(&update.user),
                activity::dsl::intensity.eq(&update.intensity),
                activity::dsl::total.eq(&update.total),
            ))
            .execute(conn)
            .is_ok()
        {
            Activity::find(conn, id)
        } else {
            let msg = String::from("Failed to update Activity!");
            let error_kind = DatabaseErrorKind::__Unknown;
            Err(DatabaseError(error_kind, Box::new(msg)))
        }
    }
}

impl From<StravaActivity> for Activity {
    fn from(obj: StravaActivity) -> Self {
        let time =
            NaiveTime::from_hms_opt(0, 0, 0).unwrap() + Duration::seconds(obj.elapsed_time.into());
        Activity {
            id: None,
            date: obj.start_date_local.replace("Z", ""),
            distance: (obj.distance / 10.).round() / 100.,
            duration: time.format("%H:%M:%S").to_string(),
            average: Some((obj.average_speed * 3.6 * 10.).round() / 10.),
            max_speed: Some(obj.max_speed * 3.6),
            wind: 0,
            temperature: obj.average_temp.unwrap_or(0.0) as i32,
            description: Some(obj.name.to_string()),
            ascent: obj.total_elevation_gain as i32,
            bike: -1,
            user: -1,
            intensity: "Normal".to_string(),
            total: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use models::stravaactivity::StravaActivity;
    use models::stravaactivity::StravaActivityType;

    use super::Activity;

    #[test]
    fn from_stravaactivity() {
        let s = StravaActivity {
            // not used
            id: -1,
            activity: None,
            external_id: "-1".to_string(),
            upload_id: -1,
            moving_time: -1,
            elev_high: -1.,
            elev_low: Some(-1.),
            activitytype: StravaActivityType::Ride,
            start_date: "-1".to_string(),
            timezone: "-1".to_string(),
            achievement_count: -1,
            kudos_count: -1,
            comment_count: -1,
            athlete_count: Some(-1),
            photo_count: -1,
            total_photo_count: -1,
            trainer: false,
            average_cadence: None,
            average_heartrate: None,
            max_heartrate: None,
            gear_id: "-1".to_string(),
            kilojoules: None,
            average_watts: None,
            device_watts: None,
            max_watts: None,
            weighted_average_watts: None,
            description: None,
            calories: -1.,
            device_name: None,
            // used to convert
            average_speed: 5.695,
            average_temp: Some(9.),
            distance: 47758.4,
            elapsed_time: 9022,
            max_speed: 16.5,
            name: "Sortie vélo l'après-midi".to_string(),
            start_date_local: "2019-12-28T13:37:51Z".to_string(),
            total_elevation_gain: 840.,
            stravamap: None,
        };
        let a = Activity::from(s);
        assert_eq!(a.ascent, 840);
        assert!((a.average.unwrap() - 20.50).abs() < 0.01);
        assert_eq!(a.date, "2019-12-28T13:37:51".to_string());
        assert_eq!(a.distance, 47.76);
        assert_eq!(a.duration, "02:30:22".to_string());
        assert_eq!(a.temperature, 9);
    }
    #[test]
    fn update() {
        use diesel::connection::SimpleConnection;
        use diesel::prelude::*;
        use diesel::result::Error;
        use diesel::sqlite::SqliteConnection;
        use std::fs;
        let a = Activity {
            id: None,
            date: "2020-01-13".to_string(),
            distance: 123.4f64,
            duration: "01:23:45".to_string(),
            average: None,
            max_speed: None,
            wind: -1,
            temperature: 0,
            description: None,
            ascent: 0,
            bike: -1,
            user: -1,
            intensity: "Normal".to_string(),
            total: None,
        };
        let conn = SqliteConnection::establish(":memory:").unwrap();
        let sql =
            fs::read_to_string("sql/schema.sql").expect("Something went wrong reading the file");
        if let Err(err) = conn.batch_execute(&sql) {
            println!("Did not succeed to execute schema.sql: {}", err);
            return;
        }
        conn.test_transaction::<_, Error, _>(|| {
            let activities = Activity::find_all_by_user(&conn, a.user);
            assert_eq!(0, activities.len());

            if let Err(err) = a.insert(&conn) {
                println!("Did not succeed to insert an Activity: {}", err);
                return Err(err);
            }
            if let Ok(new_a) = Activity::find(&conn, 1) {
                assert_eq!(a.temperature, new_a.temperature, "a.temperature not set!");
            }

            let after = Activity::find_all_by_user(&conn, a.user);
            assert_eq!(1, after.len());

            let mut b = a.clone();
            b.temperature = 12;
            let inserted_b;
            match b.insert(&conn) {
                Err(err) => {
                    println!("Did not succeed to insert an Activity b: {}", err);
                    return Err(err);
                }
                Ok(entity) => inserted_b = entity,
            };

            assert_eq!(2, inserted_b.id.unwrap(), "For b, activity.id must be 2");
            let after_b = Activity::find_all_by_user(&conn, a.user);
            assert_eq!(2, after_b.len());

            let mut c = a.clone();
            c.temperature = 23;
            let mut inserted_c;
            match c.insert(&conn) {
                Err(err) => {
                    println!("Did not succeed to insert an Activity c: {}", err);
                    return Err(err);
                }
                Ok(entity) => inserted_c = entity,
            };
            assert_eq!(3, inserted_c.id.unwrap(), "For c, activity.id must be 3");

            let after_c = Activity::find_all_by_user(&conn, a.user);
            assert_eq!(3, after_c.len());

            inserted_c.temperature = 34;
            if let Err(err) = inserted_c.update(&conn) {
                println!("Did not succeed to update an Activity: {}", err);
                return Err(err);
            }

            if let Ok(new_a) = Activity::find(&conn, 1) {
                assert_eq!(a.temperature, new_a.temperature, "a.temperature changed!");
            }
            if let Ok(new_b) = Activity::find(&conn, 2) {
                assert_eq!(b.temperature, new_b.temperature, "b.temperature changed!");
            }
            if let Ok(new_c) = Activity::find(&conn, inserted_c.id.unwrap()) {
                assert_eq!(c.temperature, new_c.temperature, "c.temperature changed!");
            }

            Ok(())
        });
    }
}
