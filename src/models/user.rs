use diesel;
use diesel::debug_query;
use diesel::prelude::*;
use diesel::sqlite::Sqlite;

use models::schema::user;
use models::schema::user::dsl::user as items;

#[derive(AsChangeset, Debug, FromForm, Identifiable, Insertable, Queryable, Serialize)]
#[table_name = "user"]
pub struct User {
    pub id: Option<i32>,
    pub login: String,
    pub name: Option<String>,
    pub password: String,
    pub rank: Option<i32>,
    pub active: Option<String>,   //SystemTime,
    pub inactive: Option<String>, //SystemTime
}

impl User {
    pub fn find(conn: &SqliteConnection, id: i32) -> QueryResult<User> {
        items.find(id).first(conn)
    }
    pub fn find_by_login(conn: &SqliteConnection, login: String) -> QueryResult<User> {
        items.filter(user::dsl::login.eq(login)).first(conn)
    }
    pub fn find_by_login_and_password(
        conn: &SqliteConnection,
        login: String,
        password: String,
    ) -> QueryResult<User> {
        items
            .filter(user::dsl::login.eq(login))
            .filter(user::dsl::password.eq(password))
            .first(conn)
    }
    pub fn insert(&self, conn: &SqliteConnection) -> QueryResult<User> {
        match diesel::insert_into(user::table).values(self).execute(conn) {
            Ok(_) => User::find_by_login(conn, self.login.to_string()),
            Err(err) => Err(err),
        }
    }
    pub fn update_password(
        conn: &SqliteConnection,
        login: String,
        pwd: String,
    ) -> QueryResult<usize> {
        let target = items.filter(user::dsl::login.eq(login));
        let query = diesel::update(target).set(user::dsl::password.eq(pwd));
        println!("SQL: {}", debug_query::<Sqlite, _>(&query).to_string());
        query.execute(conn)
    }
}
