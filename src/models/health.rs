use diesel;
use diesel::prelude::*;
use diesel::result::DatabaseErrorKind;
use diesel::result::Error::DatabaseError;

use models::schema::health;
use models::schema::health::dsl::health as items;

#[derive(Debug, FromForm, Identifiable, Insertable, Queryable, Serialize)]
#[table_name = "health"]
pub struct Health {
    pub id: Option<i32>,
    pub date: String,
    pub user: i32,
    pub weight: f64,
    pub fat_content: Option<f64>,
    pub water_content: Option<f64>,
}

impl Health {
    pub fn delete_with_id(conn: &SqliteConnection, id: i32) -> bool {
        diesel::delete(items.find(id)).execute(conn).is_ok()
    }
    pub fn find_by_user(conn: &SqliteConnection, user: i32) -> Vec<Health> {
        items
            .filter(health::dsl::user.eq(user))
            .order(health::date.desc())
            .load::<Health>(conn)
            .unwrap()
    }
    pub fn find_last_with_user(conn: &SqliteConnection, user: i32) -> QueryResult<Health> {
        items
            .filter(health::dsl::user.eq(user))
            .order(health::dsl::date.desc())
            .first(conn)
    }
    pub fn find_with_date_user(
        conn: &SqliteConnection,
        date: &String,
        user: i32,
    ) -> QueryResult<Health> {
        items
            .filter(health::dsl::date.eq(date))
            .filter(health::dsl::user.eq(user))
            .first::<Health>(conn)
    }
    pub fn find_with_period_user(
        conn: &SqliteConnection,
        start: String,
        end: String,
        user: i32,
    ) -> QueryResult<Vec<Health>> {
        items
            .filter(health::dsl::date.gt(start))
            .filter(health::dsl::date.lt(end))
            .filter(health::dsl::user.eq(user))
            .load::<Health>(conn)
    }
    pub fn find(conn: &SqliteConnection, id: i32) -> QueryResult<Health> {
        items.find(id).first(conn)
    }
    pub fn insert(&self, conn: &SqliteConnection) -> QueryResult<Health> {
        match diesel::insert_into(health::table)
            .values(self)
            .execute(conn)
        {
            Ok(_) => Health::find_with_date_user(conn, &self.date, self.user),
            Err(err) => Err(err),
        }
    }
    pub fn update(&self, conn: &SqliteConnection, update: Health) -> QueryResult<Health> {
        if let Some(id) = self.id {
            if diesel::update(self)
                .set(health::dsl::date.eq(&update.date))
                .execute(conn)
                .is_ok()
            {
                Health::find(conn, id)
            } else {
                let msg = String::from("Failed to update health!");
                let error_kind = DatabaseErrorKind::__Unknown;
                Err(DatabaseError(error_kind, Box::new(msg)))
            }
        } else {
            let msg = String::from("No health.id!");
            let error_kind = DatabaseErrorKind::__Unknown;
            Err(DatabaseError(error_kind, Box::new(msg)))
        }
    }
}
