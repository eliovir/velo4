use diesel;
use diesel::prelude::*;

use models::schema::stravaactivity;
use models::schema::stravaactivity::dsl::stravaactivity as items;
use models::schema::stravamap;
use models::schema::stravamap::dsl::stravamap as mapitems;

#[derive(Clone, Debug, DbEnum, Deserialize, PartialEq, Serialize)]
#[DieselType = "StravaActivityTypeMapping"]
pub enum StravaActivityType {
    AlpineSki,
    BackcountrySki,
    Canoeing,
    Crossfit,
    EBikeRide,
    Elliptical,
    Golf,
    Handcycle,
    Hike,
    IceSkate,
    InlineSkate,
    Kayaking,
    Kitesurf,
    NordicSki,
    Ride,
    RockClimbing,
    RollerSki,
    Rowing,
    Run,
    Sail,
    Skateboard,
    Snowboard,
    Snowshoe,
    Soccer,
    StairStepper,
    StandUpPaddling,
    Surfing,
    Swim,
    Velomobile,
    VirtualRide,
    VirtualRun,
    Walk,
    WeightTraining,
    Wheelchair,
    Windsurf,
    Workout,
    Yoga,
}

#[derive(
    Clone, Debug, Deserialize, Eq, Hash, Identifiable, Insertable, PartialEq, Queryable, Serialize,
)]
#[table_name = "stravamap"]
pub struct StravaMap {
    pub id: String,
    pub polyline: Option<String>,
    pub summary_polyline: Option<String>,
}
impl StravaMap {
    pub fn find(conn: &SqliteConnection, id: String) -> QueryResult<Self> {
        mapitems.filter(stravamap::dsl::id.eq(id)).first(conn)
    }
    pub fn insert(&self, conn: &SqliteConnection) -> QueryResult<Self> {
        match diesel::insert_into(stravamap::table)
            .values(self)
            .execute(conn)
        {
            Ok(_) => StravaMap::find(conn, self.id.to_string()),
            Err(err) => Err(err),
        }
    }
}

/// http://developers.strava.com/docs/reference/#api-models-DetailedActivity
#[derive(
    Associations, Clone, Debug, Deserialize, Identifiable, Insertable, Queryable, Serialize,
)]
#[belongs_to(StravaMap, foreign_key = "stravamap")]
#[table_name = "stravaactivity"]
pub struct StravaActivity {
    // id long 	The unique identifier of the activity
    pub id: i64,
    pub activity: Option<i32>,
    // external_id string 	The identifier provided at upload time
    pub external_id: String,
    // upload_id long 	The identifier of the upload that resulted in this activity
    pub upload_id: i64,
    // athlete MetaAthlete 	An instance of MetaAthlete.
    // name string 	The name of the activity
    pub name: String,
    // distance float 	The activity's distance, in meters
    pub distance: f64,
    // moving_time integer 	The activity's moving time, in seconds
    pub moving_time: i32,
    // elapsed_time integer 	The activity's elapsed time, in seconds
    pub elapsed_time: i32,
    // total_elevation_gain float 	The activity's total elevation gain.
    pub total_elevation_gain: f64,
    // elev_high float 	The activity's highest elevation, in meters
    pub elev_high: f64,
    // elev_low float 	The activity's lowest elevation, in meters
    pub elev_low: Option<f64>,
    // type ActivityType 	An instance of ActivityType.
    #[serde(rename = "type")]
    pub activitytype: StravaActivityType,
    // start_date DateTime 	The time at which the activity was started.
    pub start_date: String,
    // start_date_local DateTime 	The time at which the activity was started in the local timezone.
    pub start_date_local: String,
    // timezone string 	The timezone of the activity
    pub timezone: String,
    // start_latlng LatLng 	An instance of LatLng.
    //    pub start_latlng: Option<(f64, f64)>,
    // end_latlng LatLng 	An instance of LatLng.
    //    pub end_latlng: Option<(f64, f64)>,
    // achievement_count integer 	The number of achievements gained during this activity
    pub achievement_count: i32,
    // kudos_count integer 	The number of kudos given for this activity
    pub kudos_count: i32,
    // comment_count integer 	The number of comments for this activity
    pub comment_count: i32,
    // athlete_count integer 	The number of athletes for taking part in a group activity
    pub athlete_count: Option<i32>,
    // photo_count integer 	The number of Instagram photos for this activity
    pub photo_count: i32,
    // total_photo_count integer 	The number of Instagram and Strava photos for this activity
    pub total_photo_count: i32,
    // map PolylineMap 	An instance of PolylineMap.
    pub stravamap: Option<String>,
    // trainer boolean 	Whether this activity was recorded on a training machine
    pub trainer: bool,
    // commute boolean 	Whether this activity is a commute
    // manual boolean 	Whether this activity was created manually
    // private boolean 	Whether this activity is private
    // flagged boolean 	Whether this activity is flagged
    // workout_type integer 	The activity's workout type
    // upload_id_str string 	The unique identifier of the upload in string format
    // average_speed float 	The activity's average speed, in meters per second
    pub average_speed: f64,
    // max_speed float 	The activity's max speed, in meters per second
    pub max_speed: f64,
    pub average_cadence: Option<f64>,
    pub average_heartrate: Option<f64>,
    pub max_heartrate: Option<f64>,
    pub average_temp: Option<f64>,
    // has_kudoed boolean 	Whether the logged-in athlete has kudoed this activity
    // gear_id string 	The id of the gear for the activity
    pub gear_id: String,
    // kilojoules float 	The total work done in kilojoules during this activity. Rides only
    pub kilojoules: Option<f64>,
    // average_watts float 	Average power output in watts during this activity. Rides only
    pub average_watts: Option<f64>,
    // device_watts boolean 	Whether the watts are from a power meter, false if estimated
    pub device_watts: Option<bool>,
    // max_watts integer 	Rides with power meter data only
    pub max_watts: Option<i32>,
    // weighted_average_watts integer 	Similar to Normalized Power. Rides with power meter data only
    pub weighted_average_watts: Option<i32>,
    // description string 	The description of the activity
    pub description: Option<String>,
    // photos PhotosSummary 	An instance of PhotosSummary.
    // gear SummaryGear 	An instance of SummaryGear.
    // calories float 	The number of kilocalories consumed during this activity
    pub calories: f64,
    // segment_efforts DetailedSegmentEffort 	A collection of DetailedSegmentEffort objects.
    // device_name string 	The name of the device used to record the activity
    pub device_name: Option<String>,
    // embed_token string 	The token used to embed a Strava activity
    // splits_metric Split 	The splits of this activity in metric units (for runs)
    // splits_standard Split 	The splits of this activity in imperial units (for runs)
    // laps Lap 	A collection of Lap objects.
    // best_efforts DetailedSegmentEffort 	A collection of DetailedSegmentEffort objects.
}
#[derive(Deserialize, Serialize)]
pub struct StravaActivityWithMap {
    #[serde(flatten)]
    pub stravaactivity: StravaActivity,
    pub map: Option<StravaMap>,
}

impl StravaActivity {
    pub fn find(conn: &SqliteConnection, id: i64) -> QueryResult<Self> {
        items.find(id).first(conn)
    }
    pub fn find_all_by_activity(conn: &SqliteConnection, activity: i32) -> Vec<Self> {
        items
            .filter(stravaactivity::dsl::activity.eq(activity))
            .order(stravaactivity::start_date_local.asc())
            .load::<Self>(conn)
            .unwrap()
    }
    pub fn insert(&self, conn: &SqliteConnection) -> QueryResult<Self> {
        match diesel::insert_into(stravaactivity::table)
            .values(self)
            .execute(conn)
        {
            Ok(_) => StravaActivity::find(conn, self.id),
            Err(err) => Err(err),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::StravaActivityWithMap;

    #[test]
    fn parse_json() {
        let data = r#"
{
    "achievement_count": 8,
    "athlete": {
        "id": 14923266,
        "resource_state": 1
    },
    "athlete_count": 1,
    "available_zones": [],
    "average_cadence": 77.6,
    "average_heartrate": 133.6,
    "average_speed": 5.695,
    "average_temp": 9,
    "average_watts": 115.0,
    "calories": 934.0,
    "comment_count": 0,
    "commute": false,
    "description": null,
    "device_name": "Garmin Edge 820",
    "device_watts": false,
    "display_hide_heartrate_option": true,
    "distance": 47758.4,
    "elapsed_time": 9022,
    "elev_high": 446.4,
    "elev_low": 62.6,
    "embed_token": "44444a4a44aa4aa4a4444aa4aaa4444444a4aa4a",
    "end_latlng": [
        44.444444,
        4.444444
    ],
    "external_id": "garmin_push_4444444444",
    "flagged": false,
    "from_accepted_tag": false,
    "gear": {
        "distance": 11994731,
        "id": "b4444444",
        "name": "Aaaaaa",
        "primary": false,
        "resource_state": 2
    },
    "gear_id": "b4444444",
    "has_heartrate": true,
    "has_kudoed": false,
    "heartrate_opt_out": false,
    "id": 4444444444,
    "kilojoules": 964.7,
    "kudos_count": 1,
    "laps": [
    ],
    "location_city": null,
    "location_country": "France",
    "location_state": null,
    "manual": false,
    "map": {
        "id": "a2897422139",
        "polyline": "4a4a4a4a4a4a4a",
        "resource_state": 3,
        "summary_polyline": "4a4a4a4a"
    },
    "max_heartrate": 185.0,
    "max_speed": 16.5,
    "moving_time": 8386,
    "name": "Sortie \u00e0 v\u00e9lo l'apr\u00e8s-midi",
    "perceived_exertion": null,
    "photo_count": 0,
    "photos": {
        "count": 0,
        "primary": null
    },
    "pr_count": 1,
    "prefer_perceived_exertion": null,
    "private": false,
    "resource_state": 3,
    "segment_efforts": [
    ],
    "splits_metric": [
    ],
    "splits_standard": [
    ],
    "start_date": "2019-12-28T12:37:51Z",
    "start_date_local": "2019-12-28T13:37:51Z",
    "start_latitude": 44.444444,
    "start_latlng": [
        44.444444,
        4.444444
    ],
    "start_longitude": 4.444444,
    "timezone": "(GMT+01:00) Europe/Paris",
    "total_elevation_gain": 840.0,
    "total_photo_count": 0,
    "trainer": false,
    "type": "Ride",
    "upload_id": 4444444444,
    "upload_id_str": "4444444444",
    "utc_offset": 3600.0,
    "visibility": "everyone",
    "workout_type": 10
}
"#;
        let a: StravaActivityWithMap = match serde_json::from_str(data) {
            Ok(s) => s,
            Err(e) => panic!("Should never occur: {}", e),
        };
        assert_eq!(a.stravaactivity.id, 4444444444);
        assert_eq!(a.stravaactivity.average_temp, Some(9.0));
        assert_eq!(a.stravaactivity.total_elevation_gain, 840.0);
        assert_eq!(a.map.unwrap().id, "a2897422139");
    }
}
