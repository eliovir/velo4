use models::stravaactivity::StravaActivityType;
use models::stravaactivity::StravaActivityWithMap;
use models::stravauser::StravaUser;
use strava::config::StravaConfig;

pub struct StravaApi<'a> {
    sc: &'a StravaConfig,
}

#[derive(Deserialize)]
pub struct StravaAthlete {
    pub id: i32,
    pub username: String,
}

#[derive(Deserialize)]
pub struct StravaAuth {
    pub access_token: String,
    pub athlete: StravaAthlete,
    pub expires_at: i32,
    pub refresh_token: String,
    pub token_type: String,
}

#[derive(Deserialize)]
pub struct StravaRefreshAuth {
    pub access_token: String,
    pub expires_at: i32,
    pub refresh_token: String,
    pub token_type: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct StravaSummaryActivity {
    pub id: i64,
    // The time at which the activity was started in the local timezone.
    pub start_date_local: String,
    // The activity's distance, in meters
    pub distance: f64,
    // The activity's moving time, in seconds
    pub moving_time: i32,
    // The activity's average speed, in meters per second
    pub average_speed: f64,
    // The activity's max speed, in meters per second
    pub max_speed: f64,
    // The name of the activity
    pub name: String,
    // type ActivityType 	An instance of ActivityType.
    #[serde(rename = "type")]
    pub activitytype: StravaActivityType,
    // The activity's total elevation gain, in meters
    pub total_elevation_gain: f64,
    // Velo4 activity id
    pub activity: Option<i32>,
}

impl<'a> StravaApi<'a> {
    pub fn from(sc: &'a StravaConfig) -> StravaApi<'a> {
        StravaApi { sc: sc }
    }

    /// Returns the activities of an athlete for a specific identifier. Requires activity:read.
    ///
    /// http://developers.strava.com/docs/reference/#api-Activities-getLoggedInAthleteActivities
    pub fn activities(
        &mut self,
        user: StravaUser,
        before: i64,
        after: i64,
        page: i32,
        per_page: i32,
    ) -> Option<Vec<StravaSummaryActivity>> {
        match attohttpc::get("https://www.strava.com/api/v3/athlete/activities")
            .header(
                "Authorization",
                format!("{} {}", user.token_type, user.access_token),
            )
            .param("before", before)
            .param("after", after)
            .param("page", page)
            .param("per_page", per_page)
            .send()
        {
            Ok(res) => {
                println!("response status: {}", res.status().as_str());
                if res.is_success() {
                    let json_res: attohttpc::Result<Vec<StravaSummaryActivity>> = res.json();
                    match json_res {
                        Ok(values) => return Some(values),
                        Err(err) => {
                            println!("error while JSON deserialization: {}", err);
                            return None;
                        }
                    };
                } else {
                    match res.text() {
                        Ok(text) => println!("response: {}", text),
                        Err(err) => println!("error to get reponse text: {}", err),
                    }
                    return None;
                }
            }
            Err(err) => {
                println!("error while calling web service: {}", err);
                return None;
            }
        }
    }

    /// Returns the given activity that is owned by the authenticated athlete.
    ///
    /// http://developers.strava.com/docs/reference/#api-Activities-getActivityById
    pub fn activity(&mut self, user: StravaUser, id: i64) -> Option<StravaActivityWithMap> {
        match attohttpc::get(format!(
            "https://www.strava.com/api/v3/activities/{}?include_all_efforts=false",
            id
        ))
        .header(
            "Authorization",
            format!("{} {}", user.token_type, user.access_token),
        )
        .send()
        {
            Ok(res) => {
                println!("response status: {}", res.status().as_str());
                if res.is_success() {
                    let json_res: attohttpc::Result<StravaActivityWithMap> = res.json();
                    return match json_res {
                        Ok(value) => Some(value),
                        Err(err) => {
                            println!("error while JSON deserialization: {}", err);
                            None
                        }
                    };
                } else {
                    match res.text() {
                        Ok(text) => println!("response: {}", text),
                        Err(err) => println!("error to get reponse text: {}", err),
                    }
                    return None;
                }
            }
            Err(err) => {
                println!("error while calling web service: {}", err);
                return None;
            }
        }
    }

    pub fn redirect_url(&mut self, state: String) -> String {
        format!(
            "https://www.strava.com/oauth/mobile/authorize?state={}&client_id={}&redirect_uri={}&response_type=code&approval_prompt=auto&scope=activity:read_all,profile:read_all",
            state,
            self.sc.client_id,
            self.sc.redirect_url
        )
    }

    /// Revoke access to an athlete\u2019s data.
    ///
    /// http://developers.strava.com/docs/authentication/#deauthorization
    pub fn revoke(&mut self, user: StravaUser) -> Result<attohttpc::Response, attohttpc::Error> {
        attohttpc::post("https://www.strava.com/oauth/deauthorize")
            .header(
                "Authorization",
                format!("{} {}", user.token_type, user.access_token),
            )
            .send()
    }

    /// exchange the authorisation code for a token using a POST request to Strava
    /// the user profile from the response and the various token data
    pub fn strava_auth_from_code(&mut self, code: String) -> Option<StravaAuth> {
        let params = [
            ("client_id", format!("{}", self.sc.client_id)),
            ("client_secret", self.sc.client_secret.to_string()),
            ("code", code),
        ];
        let mut strava_auth: Option<StravaAuth> = None;
        match attohttpc::post("https://www.strava.com/oauth/token")
            .params(params)
            .send()
        {
            Ok(res) => {
                let json_res: attohttpc::Result<StravaAuth> = res.json();
                match json_res {
                    Ok(json) => {
                        println!("json access_token: {}", json.access_token);
                        strava_auth = Some(json);
                    }
                    Err(err) => println!("json deserialize error: {}", err),
                }
            }
            Err(_err) => println!("err"),
        }
        return strava_auth;
    }

    /// exchange the refresh token for a token using a POST request to Strava
    /// the user profile from the response and the various token data
    pub fn strava_auth_from_refresh_token(&mut self, token: String) -> Option<StravaRefreshAuth> {
        let params = [
            ("client_id", format!("{}", self.sc.client_id)),
            ("client_secret", self.sc.client_secret.to_string()),
            ("grant_type", "refresh_token".to_string()),
            ("refresh_token", token),
        ];
        let mut strava_auth: Option<StravaRefreshAuth> = None;
        match attohttpc::post("https://www.strava.com/oauth/token")
            .params(params)
            .send()
        {
            Ok(res) => {
                let json_res: attohttpc::Result<StravaRefreshAuth> = res.json();
                match json_res {
                    Ok(json) => {
                        println!("json access_token: {}", json.access_token);
                        strava_auth = Some(json);
                    }
                    Err(err) => println!("json deserialize error: {}", err),
                }
            }
            Err(_err) => println!("err"),
        }
        return strava_auth;
    }
}
