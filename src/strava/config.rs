use std::fmt;

use strava::api::StravaApi;

pub struct StravaConfig {
    pub client_id: i64,
    pub client_secret: String,
    pub redirect_connect_url: String,
    pub redirect_url: String,
}
impl fmt::Display for StravaConfig {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "StravaConfig{{client_id: {}, client_secret: {}, redirect_connect_url: {}, redirect_url: {}}}",
            self.client_id, self.client_secret, self.redirect_connect_url, self.redirect_url
        )
    }
}
impl StravaConfig {
    /// Constructs a StravaConfig from Rocket.toml and environment variables.
    pub fn from_env(conf: &rocket::Config) -> StravaConfig {
        StravaConfig::new(
            conf.get_int("strava_client_id").unwrap_or(-1),
            conf.get_string("strava_client_secret")
                .unwrap_or("".to_string()),
            conf.get_string("application_url").unwrap_or("".to_string()),
        )
    }

    /// Constructor.
    pub fn new(client_id: i64, client_secret: String, application_url: String) -> StravaConfig {
        let mut redirect_connect_url = String::new();
        redirect_connect_url.push_str(application_url.as_str());
        redirect_connect_url.push_str("auth/strava_redirect_connect");
        let mut redirect_url = String::new();
        if !application_url.is_empty() {
            redirect_url.push_str(application_url.as_str());
            redirect_url.push_str("auth/strava_redirect");
        }
        StravaConfig {
            client_id,
            client_secret,
            redirect_connect_url,
            redirect_url,
        }
    }

    pub fn api(&self) -> StravaApi {
        StravaApi::from(&self)
    }

    pub fn is_valid(&self) -> bool {
        self.client_id != -1
            && !self.client_secret.is_empty()
            && !self.redirect_connect_url.is_empty()
            && !self.redirect_url.is_empty()
    }
}

#[cfg(test)]
mod tests {
    use super::StravaConfig;

    #[test]
    fn is_valid() {
        let valid = StravaConfig::new(123, "secret".to_string(), "http://localhost/".to_string());
        assert!(valid.is_valid());
        let notvalid1 =
            StravaConfig::new(-1, "secret".to_string(), "http://localhost/".to_string());
        assert!(!notvalid1.is_valid());
        let notvalid2 = StravaConfig::new(123, "".to_string(), "http://localhost/".to_string());
        assert!(!notvalid2.is_valid());
        let notvalid3 = StravaConfig::new(123, "secret".to_string(), "".to_string());
        assert!(!notvalid3.is_valid());
    }
}
