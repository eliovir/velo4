use std::convert::Infallible;
use std::time::SystemTime;

use jsonwebtoken::{decode, DecodingKey, encode, EncodingKey, Header, Validation};
use rocket::request::{self, FromRequest, Request};
use rocket::Outcome;

// head -c16 /dev/urandom > secret.key
static KEY: &'static [u8; 16] = include_bytes!("../../secret.key");
pub static ONE_WEEK: i64 = 60 * 60 * 24 * 7;
static ONE_YEAR: i64 = 60 * 60 * 24 * 365;

pub fn now_as_sec() -> i64 {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs() as i64
}

pub struct Token(String);
impl<'a, 'r> FromRequest<'a, 'r> for Token {
    type Error = Infallible;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let header = request.headers().get_one("Authorization");
        match header {
            Some(header) => {
                if header.starts_with("Bearer ") && header.len() > 7 {
                    let token = &header[7..];
                    println!("token from header: {}", token);
                    return Outcome::Success(Token(token.to_string()));
                }
                Outcome::Forward(())
            }
            // token does not exist
            None => Outcome::Forward(()),
        }
    }
}
impl Token {
    pub fn get(&self) -> String {
        self.0.to_string()
    }
}
#[derive(Debug, Serialize, Deserialize)]
pub struct UserRolesToken {
    // issued at
    iat: i64,
    // expiration
    pub exp: i64,
    pub id: i32,
    pub login: String,
    roles: Vec<String>,
}

impl UserRolesToken {
    pub fn expires_in() -> i64 {
        ONE_WEEK
    }
    pub fn new(id: i32, login: String, roles: Vec<String>) -> UserRolesToken {
        let now = now_as_sec();
        UserRolesToken {
            iat: now,
            exp: now + ONE_WEEK,
            id,
            login,
            roles,
        }
    }
    fn from_jwt(token: String) -> Result<UserRolesToken, String> {
        // Default validation: the only algo allowed is HS256
        let validation = Validation::default();
        // token is a struct with 2 params: header and claims
        let token_data = match decode::<UserRolesToken>(&token, &DecodingKey::from_secret(KEY.as_ref()), &validation) {
            Ok(c) => c,
            Err(err) => return Err(format!("Error: {}", err)),
        };
        println!("claims: {:?}", token_data.claims);
        println!("header: {:?}", token_data.header);
        if token_data.claims.is_expired() {
            return Err("Token is expired!".to_string());
        }
        Ok(token_data.claims)
    }
    #[allow(dead_code)]
    fn has_role(&self, role: &str) -> bool {
        self.roles.contains(&role.to_string())
    }
    fn is_expired(&self) -> bool {
        let now = now_as_sec();
        now >= self.exp
    }
    #[allow(dead_code)]
    fn is_claimed_login(&self, claimed_login: String) -> bool {
        self.login == claimed_login
    }
    pub fn to_refresh_token(&self) -> String {
        let urt = UserRolesToken {
            iat: self.iat,
            exp: self.iat + ONE_YEAR,
            id: self.id,
            login: self.login.to_owned(),
            roles: vec![],
        };
        urt.to_jwt()
    }
    pub fn to_jwt(&self) -> String {
        let token = match encode(&Header::default(), &self, &EncodingKey::from_secret(KEY.as_ref())) {
            Ok(t) => t,
            Err(_) => panic!(), // in practice you would return the error
        };
        token
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for UserRolesToken {
    type Error = ();
    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        match request.cookies().get("jwt") {
            Some(cookie) => {
                let token = cookie.value();
                println!("token from cookie: {}", token);
                let urt = match UserRolesToken::from_jwt(token.to_string()) {
                    Ok(c) => c,
                    Err(_) => return Outcome::Forward(()),
                };
                println!("token id: {}", urt.id);
                println!("token login: {}", urt.login);
                return Outcome::Success(urt);
            }
            None => match request.headers().get_one("Authorization") {
                Some(header) => {
                    if header.starts_with("Bearer ") && header.len() > 7 {
                        let token = &header[7..];
                        println!("token from header: {}", token);
                        let urt = match UserRolesToken::from_jwt(token.to_string()) {
                            Ok(c) => c,
                            Err(_) => return Outcome::Forward(()),
                        };
                        println!("token id: {}", urt.id);
                        println!("token login: {}", urt.login);
                        return Outcome::Success(urt);
                    }
                    println!("no token");
                    Outcome::Forward(())
                }
                None => Outcome::Forward(()),
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::UserRolesToken;

    #[test]
    fn test() {
        let urt = UserRolesToken::new(
            42,
            "John Doe".to_string(),
            vec!["user".to_string(), "admin".to_string()],
        );
        assert!(urt.has_role("user"));
        let jwt = urt.to_jwt();
        println!("JWT: {}", jwt);
        let urt2 = match UserRolesToken::from_jwt(jwt) {
            Ok(c) => c,
            Err(e) => panic!("Should never occur: {}", e),
        };
        assert_eq!(urt.id, urt2.id);
        assert_eq!(urt.login, urt2.login);
        assert!(!urt2.is_expired());
        assert!(urt2.is_claimed_login("John Doe".to_string()));
    }

    #[test]
    fn from_jwt_wrong_token() {
        let jwt = "abc.de.fg".to_string();
        let urt = UserRolesToken::from_jwt(jwt);
        assert!(urt.is_err());
    }
}
