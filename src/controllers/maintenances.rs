use chrono::prelude::*;
use diesel::SqliteConnection;
use rocket::request::{FlashMessage, Form};
use rocket::response::{Flash, Redirect};
use rocket_contrib::templates::Template;
use rocket_slog::SyncLogger;

use models::bike::Bike;
use models::database::Database;
use models::maintenance::Maintenance;
use request::jwt::UserRolesToken;

#[derive(Debug, Serialize)]
struct Filter {
    bike_id: Option<i32>,
}

#[derive(Debug, Serialize)]
struct Context<'a, 'b> {
    msg: Option<(&'a str, &'b str)>,
    maintenances: Vec<Maintenance>,
    bikes: Vec<Bike>,
    filter: Option<Filter>,
}

impl<'a, 'b> Context<'a, 'b> {
    pub fn err(
        conn: &SqliteConnection,
        msg: &'a str,
        user_id: i32,
        filter: Option<Filter>,
    ) -> Context<'static, 'a> {
        Context::raw(conn, Some(("error", msg)), user_id, filter)
    }

    pub fn raw(
        conn: &SqliteConnection,
        msg: Option<(&'a str, &'b str)>,
        user_id: i32,
        filter: Option<Filter>,
    ) -> Context<'a, 'b> {
        Context {
            msg,
            maintenances: Maintenance::find_all_by_user(conn, user_id),
            bikes: Bike::find_all_enabled_by_user(&*conn, user_id),
            filter,
        }
    }
}

#[derive(Debug, Serialize)]
struct EditContext<'a, 'b> {
    msg: Option<(&'a str, &'b str)>,
    maintenance: Maintenance,
    bikes: Vec<Bike>,
    descriptions: Vec<String>,
    previous_id: Option<i32>,
    previous_date: Option<String>,
    next_id: Option<i32>,
    next_date: Option<String>,
}

impl<'a, 'b> EditContext<'a, 'b> {
    pub fn raw(
        conn: Database,
        urt: UserRolesToken,
        msg: Option<(&'a str, &'b str)>,
        maintenance: Maintenance,
    ) -> EditContext<'a, 'b> {
        let mut previous_id: Option<i32> = None;
        let mut previous_date: Option<String> = None;
        let mut next_id: Option<i32> = None;
        let mut next_date: Option<String> = None;
        if let Ok(previous) = Maintenance::find_previous(&*conn, &maintenance) {
            previous_id = previous.id;
            previous_date = Some(previous.date);
        }
        if let Ok(next) = Maintenance::find_next(&*conn, &maintenance) {
            next_id = next.id;
            next_date = Some(next.date);
        }
        EditContext {
            msg,
            maintenance,
            bikes: Bike::find_all_enabled_by_user(&*conn, urt.id),
            descriptions: Maintenance::find_all_descriptions_by_user(&*conn, urt.id),
            previous_id,
            previous_date,
            next_id,
            next_date,
        }
    }
}

#[get("/")]
pub fn all(conn: Database, urt: UserRolesToken, msg: Option<FlashMessage>) -> Template {
    Template::render(
        "maintenances",
        &match msg {
            Some(ref msg) => Context::raw(&*conn, Some((msg.name(), msg.msg())), urt.id, None),
            None => Context::raw(&*conn, None, urt.id, None),
        },
    )
}

#[get("/create")]
pub fn create_get(conn: Database, urt: UserRolesToken) -> Template {
    let user_id = urt.id;
    let now = Local::now();
    let mut last_bike: i32 = 0;
    if let Ok(last) = Maintenance::find_last_with_user(&*conn, user_id) {
        last_bike = last.bike;
    }
    let maintenance = Maintenance {
        id: None,
        date: now.format("%Y-%m-%dT%H:%M:%S").to_string(),
        description: String::from(""),
        bike: last_bike,
    };
    Template::render(
        "maintenance",
        EditContext::raw(conn, urt, None, maintenance),
    )
}

#[post("/create", data = "<form>")]
pub fn create(conn: Database, _urt: UserRolesToken, form: Form<Maintenance>) -> Flash<Redirect> {
    let new_maintenance = form.into_inner();
    if new_maintenance.date.is_empty() {
        Flash::error(Redirect::to("/maintenances"), "Date cannot be empty.")
    } else {
        match new_maintenance.insert(&*conn) {
            Ok(saved) => {
                if let Some(id) = saved.id {
                    let url = format!("/maintenances/{}", id);
                    Flash::success(Redirect::found(url), "Maintenance successfully added.")
                } else {
                    Flash::error(
                        Redirect::to("/maintenances"),
                        "Whoops! The server failed to get maintenance.id!",
                    )
                }
            }
            Err(err) => {
                let msg = format!("Whoops! The server failed to save maintenance! {:?}", err);
                Flash::error(Redirect::to("/maintenances"), msg)
            }
        }
    }
}

#[delete("/<id>")]
pub fn delete(conn: Database, urt: UserRolesToken, id: i32) -> Result<Flash<Redirect>, Template> {
    if Maintenance::delete_with_id(&*conn, id) {
        Ok(Flash::success(
            Redirect::to("/maintenances"),
            "maintenance-was-deleted",
        ))
    } else {
        Err(Template::render(
            "maintenances",
            &Context::err(&*conn, "failed-to-delete-maintenance", urt.id, None),
        ))
    }
}

#[get("/?<bike>")]
pub fn filter(
    conn: Database,
    urt: UserRolesToken,
    logger: SyncLogger,
    msg: Option<FlashMessage>,
    bike: i32,
) -> Template {
    debug!(logger.get(), "Activities for bike #{}", bike);
    Template::render(
        "maintenances",
        Context {
            msg: match msg {
                Some(ref msg) => Some((msg.name(), msg.msg())),
                None => None,
            },
            maintenances: Maintenance::find_all_by_bike(&*conn, bike),
            bikes: Bike::find_all_enabled_by_user(&*conn, urt.id),
            filter: Some(Filter {
                bike_id: Some(bike),
            }),
        },
    )
}

#[get("/<id>")]
pub fn read(
    conn: Database,
    urt: UserRolesToken,
    id: i32,
    msg: Option<FlashMessage>,
) -> Result<Template, Flash<Redirect>> {
    if let Ok(maintenance) = Maintenance::find(&*conn, id) {
        Ok(Template::render(
            "maintenance",
            &match msg {
                Some(ref msg) => {
                    EditContext::raw(conn, urt, Some((msg.name(), msg.msg())), maintenance)
                }
                None => EditContext::raw(conn, urt, None, maintenance),
            },
        ))
    } else {
        Err(Flash::error(
            Redirect::to("/maintenances"),
            "failed-to-get-maintenance",
        ))
    }
}

#[post("/", data = "<form>")]
pub fn update(conn: Database, _urt: UserRolesToken, form: Form<Maintenance>) -> Flash<Redirect> {
    let update = form.into_inner();
    if let Some(id) = update.id {
        match Maintenance::find(&*conn, id) {
            Ok(maintenance) => {
                if maintenance.update(&*conn, update).is_ok() {
                    let url = format!("/maintenances/{}", id);
                    Flash::success(Redirect::found(url), "Maintenance successfully updated.")
                } else {
                    Flash::error(
                        Redirect::to("/maintenances"),
                        "failed-to-update-maintenance",
                    )
                }
            }
            Err(err) => {
                let msg = format!("Whoops! The server failed to get maintenance! {:?}", err);
                Flash::error(Redirect::to("/maintenances"), msg)
            }
        }
    } else {
        let msg = "Whoops! No maintenance.id given!";
        Flash::error(Redirect::to("/maintenances"), msg)
    }
}
