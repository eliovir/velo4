use std::collections::HashMap;

use chrono::prelude::*;
use diesel::SqliteConnection;
use rocket::http::{Cookie, Cookies, Status};
use rocket::request::{FlashMessage, Form};
use rocket::response::status;
use rocket::response::{Flash, Redirect};
use rocket_contrib::json::Json;
use rocket_contrib::json::JsonValue;
use rocket_contrib::templates::Template;

use models::database::Database;
use models::health::Health;
use models::oauth::OAuthToken;
use request::jwt::Token;
use request::jwt::UserRolesToken;

#[derive(Debug, Serialize)]
struct Context<'a, 'b> {
    msg: Option<(&'a str, &'b str)>,
    healths: Vec<Health>,
    user_id: i32,
}

impl<'a, 'b> Context<'a, 'b> {
    pub fn err(conn: &SqliteConnection, msg: &'a str, user_id: i32) -> Context<'static, 'a> {
        Context {
            msg: Some(("error", msg)),
            healths: Health::find_by_user(conn, user_id),
            user_id: user_id,
        }
    }

    pub fn raw(
        conn: &SqliteConnection,
        msg: Option<(&'a str, &'b str)>,
        user_id: i32,
    ) -> Context<'a, 'b> {
        Context {
            msg,
            healths: Health::find_by_user(conn, user_id),
            user_id,
        }
    }
}

#[derive(FromForm, Serialize)]
pub struct Weight {
    timestamp: i64,
    weight: f64,
}
#[derive(Serialize)]
pub struct Weights {
    value: Vec<Weight>,
}

#[get("/")]
pub fn all(conn: Database, urt: UserRolesToken, msg: Option<FlashMessage>) -> Template {
    Template::render(
        "healths",
        &match msg {
            Some(ref msg) => Context::raw(&*conn, Some((msg.name(), msg.msg())), urt.id),
            None => Context::raw(&*conn, None, urt.id),
        },
    )
}

#[get("/weights/<start>/<end>")]
pub fn weights(conn: Database, urt: UserRolesToken, start: i64, end: i64) -> Json<Weights> {
    let user_id = urt.id;
    let mut weights = Vec::new();
    let startf = DateTime::from_timestamp(start / 1000, 0)
        .unwrap()
        .format("%Y-%m-%dT%H:%M:%S")
        .to_string();
    let endf = DateTime::from_timestamp(end / 1000, 0)
        .unwrap()
        .format("%Y-%m-%dT%H:%M:%S")
        .to_string();
    if let Ok(healths) = Health::find_with_period_user(&*conn, startf, endf, user_id) {
        for health in healths {
            if let Ok(datetime) = NaiveDateTime::parse_from_str(&health.date, "%Y-%m-%dT%H:%M") {
                let weight = Weight {
                    timestamp: datetime.and_utc().timestamp(),
                    weight: health.weight,
                };
                weights.push(weight);
            } else if let Ok(datetime) =
                NaiveDateTime::parse_from_str(&health.date, "%Y-%m-%dT%H:%M:%S")
            {
                let weight = Weight {
                    timestamp: datetime.and_utc().timestamp(),
                    weight: health.weight,
                };
                weights.push(weight);
            } else {
                println!("Wrong format for weight date: {}", &health.date);
            }
        }
    }
    Json(Weights { value: weights })
}

#[get("/create")]
pub fn create_get(
    conn: Database,
    opt_urt: Option<UserRolesToken>,
    mut cookies: Cookies<'_>,
) -> Result<Template, Flash<Redirect>> {
    if opt_urt.is_none() {
        cookies.add_private(Cookie::new("previous_url", "/healths/create".to_string()));
        let msg = "Whoops! Not connected!";
        return Err(Flash::error(Redirect::to("/auth/login"), msg));
    }
    let urt = opt_urt.unwrap();
    let user_id = urt.id;
    let now = Local::now();
    let mut last_weight: f64 = -1.0;
    let mut last_fat_content = None;
    let mut last_water_content = None;
    if let Ok(last) = Health::find_last_with_user(&*conn, user_id) {
        last_weight = last.weight;
        last_fat_content = last.fat_content;
        last_water_content = last.water_content;
    }
    let health = Health {
        id: None,
        date: now.format("%Y-%m-%dT%H:%M:%S").to_string(),
        user: user_id,
        weight: last_weight,
        fat_content: last_fat_content,
        water_content: last_water_content,
    };
    let mut context = HashMap::new();
    context.insert("health", health);
    Ok(Template::render("health", &context))
}

// TODO use generic fn() -> {success, url, msg} for Flash and Json

fn create_health_entity(
    conn: Database,
    user: i32,
    date: String,
    weight: f64,
) -> Result<(Health, String, String), (Status, String, String)> {
    if Health::find_with_date_user(&*conn, &date, user).is_ok() {
        return Err((
            Status::BadRequest,
            "/healths".to_string(),
            format!("Whoops! The health at {:?} already exists!", date),
        ));
    }
    let new_health = Health {
        id: None,
        date: date,
        user: user,
        weight: weight,
        fat_content: None,
        water_content: None,
    };
    match new_health.insert(&*conn) {
        Ok(saved) => {
            if let Some(id) = saved.id {
                let url = format!("/healths/{}", id);
                return Ok((saved, url, "Health successfully added.".to_string()));
            } else {
                return Err((
                    Status::InternalServerError,
                    "/healths".to_string(),
                    "Whoops! The server failed to get health.id!".to_string(),
                ));
            }
        }
        Err(err) => {
            let msg = format!("Whoops! The server failed to save health! {:?}", err);
            return Err((Status::InternalServerError, "/healths".to_string(), msg));
        }
    }
}

#[post("/create", data = "<form>")]
pub fn create(conn: Database, urt: UserRolesToken, form: Form<Health>) -> Flash<Redirect> {
    let new_health = form.into_inner();
    let user_id = urt.id;
    if new_health.date.is_empty() {
        Flash::error(Redirect::to("/healths"), "Date cannot be empty.")
    } else {
        // already exist ?
        if Health::find_with_date_user(&*conn, &new_health.date, user_id).is_ok() {
            let msg = format!(
                "Whoops! The health at {:?} already exists!",
                new_health.date
            );
            return Flash::error(Redirect::to("/healths"), msg);
        }
        //
        match new_health.insert(&*conn) {
            Ok(saved) => {
                if let Some(id) = saved.id {
                    let url = format!("/healths/{}", id);
                    Flash::success(Redirect::found(url), "Health successfully added.")
                } else {
                    Flash::error(
                        Redirect::to("/healths"),
                        "Whoops! The server failed to get health.id!",
                    )
                }
            }
            Err(err) => {
                let msg = format!("Whoops! The server failed to save health! {:?}", err);
                Flash::error(Redirect::to("/healths"), msg)
            }
        }
    }
}

#[post("/create", data = "<data>", rank = 2)]
pub fn create_weight(
    conn: Database,
    token: Token,
    data: Form<Weight>,
) -> status::Custom<JsonValue> {
    let oauthtoken_result = OAuthToken::find_by_access_token(&*conn, token.get());
    if oauthtoken_result.is_err() {
        return status::Custom(
            Status::BadRequest,
            json!({"error": "400", "message": "unknown token"}),
        );
    }
    let oauthtoken = oauthtoken_result.unwrap();
    let date = if data.timestamp == -1 {
        Local::now().format("%Y-%m-%dT%H:%M:%S").to_string()
    } else {
        DateTime::from_timestamp(data.timestamp / 1000, 0)
            .unwrap()
            .format("%Y-%m-%dT%H:%M:%S")
            .to_string()
    };
    match create_health_entity(conn, oauthtoken.user, date, data.weight) {
        Ok((health, url, msg)) => {
            return status::Custom(
                Status::Ok,
                json!({"message": "done", "id": health.id.unwrap(), "url": url, "message": msg}),
            )
        }
        Err((status, url, msg)) => {
            return status::Custom(status, json!({"error": "400", "url": url, "message": msg}))
        }
    }
}

#[delete("/<id>")]
pub fn delete(conn: Database, urt: UserRolesToken, id: i32) -> Result<Flash<Redirect>, Template> {
    let user_id = urt.id;
    if Health::delete_with_id(&*conn, id) {
        Ok(Flash::success(
            Redirect::to("/healths"),
            "health-was-deleted",
        ))
    } else {
        Err(Template::render(
            "healths",
            &Context::err(&*conn, "failed-to-delete-health", user_id),
        ))
    }
}

#[get("/<id>")]
pub fn read(conn: Database, _urt: UserRolesToken, id: i32) -> Result<Template, Flash<Redirect>> {
    if let Ok(health) = Health::find(&*conn, id) {
        let mut context = HashMap::new();
        context.insert("health", health);
        Ok(Template::render("health", &context))
    } else {
        Err(Flash::error(
            Redirect::to("/healths"),
            "failed-to-get-health",
        ))
    }
}

#[post("/", data = "<form>")]
pub fn update(conn: Database, _urt: UserRolesToken, form: Form<Health>) -> Flash<Redirect> {
    let update = form.into_inner();
    if let Some(id) = update.id {
        match Health::find(&*conn, id) {
            Ok(health) => {
                if health.update(&*conn, update).is_ok() {
                    let url = format!("/healths/{}", id);
                    Flash::success(Redirect::found(url), "Health successfully updated.")
                } else {
                    Flash::error(Redirect::to("/healths"), "failed-to-update-health")
                }
            }
            Err(err) => {
                let msg = format!("Whoops! The server failed to get health! {:?}", err);
                Flash::error(Redirect::to("/healths"), msg)
            }
        }
    } else {
        let msg = "Whoops! No health.id given!";
        Flash::error(Redirect::to("/healths"), msg)
    }
}
