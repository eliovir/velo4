use std::fs;
use std::path::{Path, PathBuf};
use std::time::UNIX_EPOCH;

use chrono::Duration;
use chrono::{DateTime, Local, TimeZone};
use rocket::http::{ContentType, Header};
use rocket::response::Response;

use response::named_file::NamedFile;

fn files(directory: &'static str, path: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new(directory).join(path)).ok()
}

#[get("/css/<path..>", rank = 5)]
pub fn css(path: PathBuf) -> Option<NamedFile> {
    files("static/css", path)
}

#[get("/favicon.ico")]
pub fn favicon<'r>() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/images/favicon.ico")).ok()
}

#[get("/fonts/<path..>", rank = 5)]
pub fn fonts(path: PathBuf) -> Option<NamedFile> {
    files("static/fonts", path)
}

#[get("/images/<path..>", rank = 5)]
pub fn images(path: PathBuf) -> Option<NamedFile> {
    files("static/images", path)
}

#[get("/js/<path..>", rank = 5)]
pub fn js(path: PathBuf) -> Option<NamedFile> {
    files("static/js", path)
}

#[head("/js/<path..>", rank = 5)]
pub fn head_js<'a>(path: PathBuf) -> Response<'a> {
    // https://api.rocket.rs/rocket/response/struct.ResponseBuilder.html
    let mut builder = Response::build();

    let directory = "static/js";
    // let mut response = Response::new();
    let fpath = Path::new(directory).join(path);
    if let Some(ext) = fpath.extension() {
        // TODO: Use Cow for lowercase.
        let ext_string = ext.to_string_lossy().to_lowercase();
        if let Some(content_type) = ContentType::from_extension(&ext_string) {
            builder.header(content_type);
        }
    }

    /*
     * TODO: 404
    match fs::metadata(fpath.as_path()) {
        Ok(metadata) => println!("ok"),
        Err(err) => println!("err: {}", err),
    }
    */
    // https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.29
    if let Ok(metadata) = fs::metadata(fpath) {
        if let Ok(time) = metadata.modified() {
            let seconds = time.duration_since(UNIX_EPOCH).unwrap().as_secs() as i64;
            let date: DateTime<Local> = Local.timestamp_opt(seconds, 0).unwrap();
            let date_header = Header::new(
                "Last-Modified",
                date.format("%a, %d %b %Y %H:%M:%S %Z").to_string(),
            );
            //response.set_header(date_header);
            builder.header(date_header);
        }
        let size = metadata.len() / 8;
        println!("Content-Length: {}", size);
        let header = Header::new("Content-Length", format!("{}", size));
        //response.set_header(header);
        builder.header(header);
        builder.raw_header("Content-Length", format!("{}", size));
        builder.raw_header("X-Content-Length", format!("{}", size));
    }

    let now: DateTime<Local> = Local::now() + Duration::weeks(52);
    let expires_header = Header::new(
        "Expires",
        now.format("%a, %d %b %Y %H:%M:%S %Z").to_string(),
    );
    //response.set_header(expires_header);
    builder.header(expires_header);

    //response
    builder.finalize()
}

#[get("/locales/<path..>", rank = 5)]
pub fn locales(path: PathBuf) -> Option<NamedFile> {
    files("static/locales", path)
}
