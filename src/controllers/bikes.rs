use std::collections::HashMap;

use diesel::SqliteConnection;
use rocket::request::{FlashMessage, Form};
use rocket::response::{Flash, Redirect};
use rocket_contrib::templates::Template;

use models::bike::Bike;
use models::bikesummary::BikeSummary;
use models::database::Database;
use request::jwt::UserRolesToken;

#[derive(Debug, Serialize)]
struct Context<'a, 'b> {
    msg: Option<(&'a str, &'b str)>,
    bikes: Vec<Bike>,
    user_id: i32,
}

impl<'a, 'b> Context<'a, 'b> {
    pub fn err(conn: &SqliteConnection, msg: &'a str, user_id: i32) -> Context<'static, 'a> {
        Context::raw(conn, Some(("error", msg)), user_id)
    }

    pub fn raw(
        conn: &SqliteConnection,
        msg: Option<(&'a str, &'b str)>,
        user_id: i32,
    ) -> Context<'a, 'b> {
        Context {
            msg,
            bikes: Bike::find_all_by_user(conn, user_id),
            user_id,
        }
    }
}

#[derive(Serialize)]
struct SummaryContext<'a, 'b> {
    msg: Option<(&'a str, &'b str)>,
    bikesummaries: Vec<BikeSummary>,
    user_id: i32,
}

impl<'a, 'b> SummaryContext<'a, 'b> {
    pub fn raw(
        conn: &SqliteConnection,
        msg: Option<(&'a str, &'b str)>,
        user_id: i32,
    ) -> SummaryContext<'a, 'b> {
        SummaryContext {
            msg,
            bikesummaries: BikeSummary::all(conn),
            user_id,
        }
    }
}

#[get("/")]
pub fn all(conn: Database, urt: UserRolesToken, msg: Option<FlashMessage>) -> Template {
    Template::render(
        "bikes",
        &match msg {
            Some(ref msg) => SummaryContext::raw(&*conn, Some((msg.name(), msg.msg())), urt.id),
            None => SummaryContext::raw(&*conn, None, urt.id),
        },
    )
}

#[post("/create", data = "<form>")]
pub fn create(conn: Database, urt: UserRolesToken, form: Form<Bike>) -> Flash<Redirect> {
    let new_bike = form.into_inner();
    let user_id = urt.id;
    if new_bike.name.is_empty() {
        Flash::error(Redirect::to("/bikes"), "Name cannot be empty.")
    } else {
        // already exist ?
        if Bike::find_with_name_user(&*conn, &new_bike.name, user_id).is_ok() {
            let msg = format!("Whoops! The bike {:?} already exists!", new_bike.name);
            return Flash::error(Redirect::to("/bikes"), msg);
        }
        //
        match new_bike.insert(&*conn) {
            Ok(saved) => {
                if let Some(id) = saved.id {
                    let url = format!("/bikes/{}", id);
                    Flash::success(Redirect::found(url), "Bike successfully added.")
                } else {
                    Flash::error(
                        Redirect::to("/bikes"),
                        "Whoops! The server failed to get bike.id!",
                    )
                }
            }
            Err(err) => {
                let msg = format!("Whoops! The server failed to save bike! {:?}", err);
                Flash::error(Redirect::to("/bikes"), msg)
            }
        }
    }
}

#[delete("/<id>")]
pub fn delete(conn: Database, urt: UserRolesToken, id: i32) -> Result<Flash<Redirect>, Template> {
    if Bike::delete_with_id(&*conn, id) {
        Ok(Flash::success(Redirect::to("/bikes"), "bike-was-deleted"))
    } else {
        Err(Template::render(
            "bikes",
            &Context::err(&*conn, "failed-to-delete-bike", urt.id),
        ))
    }
}

#[get("/<id>")]
pub fn read(conn: Database, _urt: UserRolesToken, id: i32) -> Result<Template, Flash<Redirect>> {
    if let Ok(bike) = Bike::find(&*conn, id) {
        let mut context = HashMap::new();
        context.insert("bike", bike);
        Ok(Template::render("bike", &context))
    } else {
        Err(Flash::error(Redirect::to("/bikes"), "failed-to-get-bike"))
    }
}

#[post("/", data = "<form>")]
pub fn update(conn: Database, _urt: UserRolesToken, form: Form<Bike>) -> Flash<Redirect> {
    let update = form.into_inner();
    if let Some(id) = update.id {
        match Bike::find(&*conn, id) {
            Ok(bike) => {
                if bike.update(&*conn, update).is_ok() {
                    let url = format!("/bikes/{}", id);
                    Flash::success(Redirect::found(url), "Bike successfully updated.")
                } else {
                    Flash::error(Redirect::to("/bikes"), "failed-to-update-bike")
                }
            }
            Err(err) => {
                let msg = format!("Whoops! The server failed to get bike! {:?}", err);
                Flash::error(Redirect::to("/bikes"), msg)
            }
        }
    } else {
        let msg = "Whoops! No bike.id given!";
        Flash::error(Redirect::to("/bikes"), msg)
    }
}
