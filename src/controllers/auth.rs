use std::collections::HashMap;

use chrono::prelude::*;
use diesel::result::QueryResult;
use random_string::generate;
use rocket::http::RawStr;
use rocket::http::Status;
use rocket::http::{Cookie, Cookies};
use rocket::request::Form;
use rocket::response::status;
use rocket::response::{Flash, Redirect};
use rocket::State;
use rocket_contrib::json::Json;
use rocket_contrib::json::JsonValue;
use rocket_contrib::templates::Template;

use models::database::Database;
use models::oauth::OAuthClient;
use models::oauth::OAuthDeviceCode;
use models::oauth::OAuthToken;
use models::stravauser::StravaUser;
use models::user::User;
use request::jwt::now_as_sec;
use request::jwt::UserRolesToken;
use request::jwt::ONE_WEEK;
use strava::config::StravaConfig;

#[derive(FromForm)]
pub struct ActivateRequest {
    user_code: String,
}

#[derive(FromForm)]
pub struct Credentials {
    login: String,
    password: String,
    redirect_url: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct DeviceCodeRequest {
    client_id: String,
}

#[derive(Debug, Deserialize)]
pub struct DeviceCodeToken {
    client_id: String,
    device_code: String,
    grant_type: String,
}

#[derive(FromForm)]
pub struct PasswordChange {
    password: String,
    password_repeat: String,
}

fn generate_random_code() -> String {
    let charset = "23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ";
    generate(10, charset)
}

fn user_to_urt(user: User) -> UserRolesToken {
    let roles = match user.rank {
        Some(2) => vec!["user".to_string(), "admin".to_string()],
        _ => vec!["user".to_string()],
    };
    UserRolesToken::new(user.id.unwrap(), user.login.to_string(), roles)
}

fn user_to_jwt_cookie(user: User) -> Cookie<'static> {
    let urt = user_to_urt(user);
    let jwt = urt.to_jwt();
    println!("success: {} {}", urt.login, jwt.to_string());
    Cookie::build("jwt", jwt.to_string())
        .path("/")
        .permanent()
        .finish()
}

// Display web page for the user to activate device.
#[get("/activate?<user_code>")]
pub fn activate_get(
    conn: Database,
    option_urt: Option<UserRolesToken>,
    user_code: &RawStr,
) -> Result<Template, Flash<Redirect>> {
    if option_urt.is_none() {
        return Err(Flash::error(
            Redirect::to(format!(
                "/auth/login?redirect_url={}{}",
                urlencoding::encode("/auth/activate?user_code="),
                user_code
            )),
            "login",
        ));
    }
    let urt = option_urt.unwrap();
    // user must be authenticated : urt is injected
    let user_request = User::find(&*conn, urt.id);
    if user_request.is_err() {
        let mut context = HashMap::new();
        context.insert("error", "unknown user");
        return Ok(Template::render("auth-activate", &context));
    }
    let user = user_request.unwrap();
    // check user_code: get OAuthDeviceCode
    let oauthdevicecode_request = OAuthDeviceCode::find_by_user_code(&*conn, user_code.to_string());
    // if not bad request
    if oauthdevicecode_request.is_err() {
        let mut context = HashMap::new();
        context.insert("error", "unknown user_code");
        return Ok(Template::render("auth-activate", &context));
    }
    let oauthdevicecode = oauthdevicecode_request.unwrap();
    // find OAuthClient
    let oauthclient_request = OAuthClient::find(&*conn, oauthdevicecode.oauthclient.to_owned());
    if oauthclient_request.is_err() {
        println!(
            "OAuthClient not found for {:?}",
            oauthdevicecode.oauthclient
        );
        let mut context = HashMap::new();
        context.insert("error", "unknown OAuthClient");
        return Ok(Template::render("auth-activate", &context));
    }
    let oauthclient = oauthclient_request.unwrap();
    // draw page with url, user_code, user full name, OAuthClient details
    let mut context = HashMap::new();
    context.insert("user_code", user_code.to_string());
    context.insert(
        "user_full_name",
        user.name.unwrap_or("user name not set".to_string()),
    );
    context.insert("oauthclient_name", oauthclient.name);
    Ok(Template::render("auth-activate", &context))
}

// When the user press the button to activate the device.
#[post("/activate", data = "<request>")]
pub fn activate_post(
    conn: Database,
    request: Form<ActivateRequest>,
    urt: UserRolesToken,
) -> Template {
    // check user_code: get OAuthDeviceCode
    let user_code = request.user_code.to_owned();
    let oauthdevicecode_request = OAuthDeviceCode::find_by_user_code(&*conn, user_code.to_owned());
    // if not bad request
    if oauthdevicecode_request.is_err() {
        let mut context = HashMap::new();
        context.insert(
            "error",
            format!("unknown user_code: {}", user_code.to_string()),
        );
        return Template::render("auth-activate", &context);
    }
    // Validate OAuthDeviceCode
    let oauthdevicecode = oauthdevicecode_request.unwrap();
    if !oauthdevicecode.set_user(&*conn, urt.id) {
        let mut context = HashMap::new();
        context.insert("error", "validation failure");
        return Template::render("auth-activate", &context);
    }
    // user must be authenticated : urt is injected
    let user_request = User::find(&*conn, urt.id);
    if user_request.is_err() {
        let mut context = HashMap::new();
        context.insert("error", "unknown user");
        return Template::render("auth-activate", &context);
    }
    let user = user_request.unwrap();
    // find OAuthClient
    let oauthclient_request = OAuthClient::find(&*conn, oauthdevicecode.oauthclient.to_owned());
    if oauthclient_request.is_err() {
        println!(
            "OAuthClient not found for {:?}",
            oauthdevicecode.oauthclient
        );
        let mut context = HashMap::new();
        context.insert("error", "unknown OAuthClient");
        return Template::render("auth-activate", &context);
    }
    let oauthclient = oauthclient_request.unwrap();
    // Draw page or show API key page or redirect with flash
    let mut context = HashMap::new();
    context.insert("user_code", user_code.to_string());
    context.insert(
        "user_full_name",
        user.name.unwrap_or("user name not set".to_string()),
    );
    context.insert("oauthclient_name", oauthclient.name);
    Template::render("auth-activate", &context)
}

#[post("/device/code", data = "<request>")]
pub fn device_code(conn: Database, request: Json<DeviceCodeRequest>) -> status::Custom<JsonValue> {
    println!("{:?}", request);
    // find OAuthClient
    match OAuthClient::find_by_client_id(&*conn, request.client_id.to_owned()) {
        Ok(oauthclient) => {
            println!("OAuthClient found for client_id {}", request.client_id);
            // generate device_code with expiration
            let device_code = generate_random_code();
            let user_code = generate_random_code();
            // save OAuthDeviceCode
            let oauthdevicecode = OAuthDeviceCode {
                id: None,
                oauthclient: oauthclient.id.unwrap(),
                user_code: user_code.to_owned(),
                device_code: device_code.to_owned(),
                user: None,
            };
            match oauthdevicecode.insert(&*conn) {
                Ok(inserted) => println!("OAuthDeviceCode #{:?} inserted", inserted.id),
                Err(_) => {
                    return status::Custom(
                        Status::InternalServerError,
                        json!({
                        "error": "500",
                        "message": "unknown error"
                        }),
                    )
                }
            };
            // response
            let application_url = dotenv!("ROCKET_APPLICATION_URL");
            let verification_uri = format!("{}auth/activate", application_url);
            let verification_uri_complete = format!("{}?user_code={}", verification_uri, user_code);
            let expires_in = UserRolesToken::expires_in();
            status::Custom(
                Status::Ok,
                json!({
                    "device_code": device_code,
                    "user_code": user_code,
                    "verification_uri": verification_uri,
                    "verification_uri_complete": verification_uri_complete,
                    "expires_in": expires_in,
                    "interval": "5",
                }),
            )
        }
        Err(_) => {
            let msg = format!("OAuthClient not found for client_id {}", request.client_id);
            println!("error : {}", msg);
            status::Custom(
                Status::NotFound,
                json!({
                    "error": "404",
                    "message": msg
                }),
            )
        }
    }
}

#[get("/login?<redirect_url>")]
pub fn login_get(sc: State<StravaConfig>, redirect_url: Option<&RawStr>) -> Template {
    println!(">>>> redirect_url: {:?}", redirect_url);
    let mut context = HashMap::new();
    context.insert(
        "redirect_url",
        redirect_url
            .unwrap_or_else(|| RawStr::from_str("/"))
            .to_string(),
    );
    context.insert("strava_enabled", sc.is_valid().to_string());
    Template::render("login", &context)
}

//
#[post("/login", data = "<credentials>")]
pub fn login_post(
    conn: Database,
    mut cookies: Cookies,
    credentials: Form<Credentials>,
) -> Flash<Redirect> {
    println!("{}", credentials.login);
    println!("{}", credentials.password);
    match User::find_by_login_and_password(
        &*conn,
        credentials.login.to_string(),
        credentials.password.to_string(),
    ) {
        Ok(user) => {
            cookies.add(user_to_jwt_cookie(user));
            let cookie = cookies.get_private("previous_url");
            let mut url = "/home".to_string();
            if let Some(ref cookie) = cookie {
                url = cookie.value().to_string();
                cookies.remove_private(Cookie::named("previous_url"));
            };
            if let Some(encoded_url) = &credentials.redirect_url {
                if let Ok(decoded_url) = urlencoding::decode(&encoded_url) {
                    url = decoded_url.to_string();
                }
            }
            Flash::success(Redirect::to(url), "Successfully logged in.")
        }
        Err(err) => {
            let msg = format!("Whoops! The server failed to get identify. {:?}", err);
            Flash::error(Redirect::to("/auth/login"), msg)
        }
    }
}
#[get("/logout")]
pub fn logout(mut cookies: Cookies) -> Flash<Redirect> {
    cookies.remove_private(Cookie::named("jwt"));
    Flash::success(Redirect::to("/"), "Successfully logged out.")
}

#[get("/password")]
pub fn password_get(_urt: UserRolesToken) -> Template {
    let mut map = HashMap::new();
    map.insert("msg", "TODO".to_string());
    Template::render("password", &map)
}

#[post("/password", data = "<pwd>")]
pub fn password_post(conn: Database, urt: UserRolesToken, pwd: Form<PasswordChange>) -> Template {
    let mut map = HashMap::new();
    if pwd.password != pwd.password_repeat {
        map.insert("msg", "Passwords are not the same".to_string());
        return Template::render("password", &map);
    }
    match User::update_password(&*conn, urt.login.to_string(), pwd.password.to_string()) {
        Ok(usize) => {
            if usize == 1 {
                map.insert("msg", "OK".to_string());
            } else {
                map.insert("msg", "KO: no user found!".to_string());
            }
        }
        Err(_err) => {
            map.insert("msg", "KO: update failed!".to_string());
        }
    }
    return Template::render("password", &map);
}

#[get("/strava_connect")]
pub fn strava_connect(sc: State<StravaConfig>, _urt: UserRolesToken) -> Redirect {
    let state = "/".to_string();
    Redirect::to(sc.api().redirect_url(state))
}

#[get("/strava_disconnect/<id>")]
pub fn strava_disconnect(
    conn: Database,
    sc: State<StravaConfig>,
    urt: UserRolesToken,
    id: i32,
) -> Result<Redirect, String> {
    let res = StravaUser::find_by_id(&*conn, id);
    return if let Ok(found) = res {
        if found.user != urt.id {
            return Err("Not owner of Strava id!".to_string());
        }
        // revoke on Strava
        match sc.api().revoke(found) {
            Ok(_) => println!("Revoked on Strava"),
            Err(_) => println!("Error while revoking on Strava"),
        }
        // delete in database
        let num_deleted = StravaUser::delete_by_id(&*conn, id);
        println!("Deleted {} stravauser", num_deleted);
        Ok(Redirect::to("/settings/profile"))
    } else {
        Err("Unknown id".to_string())
    };
}

#[get("/strava_login")]
pub fn strava_login(mut cookies: Cookies<'_>, sc: State<StravaConfig>) -> Redirect {
    let state = "/".to_string();
    cookies.add_private(Cookie::new("oauth2_state", state.to_string()));
    Redirect::to(sc.api().redirect_url(state))
}

#[get("/strava_redirect?<state>&<code>&<scope>")]
pub fn strava_redirect(
    conn: Database,
    mut cookies: Cookies,
    sc: State<StravaConfig>,
    opt_urt: Option<UserRolesToken>,
    state: &RawStr,
    code: &RawStr,
    scope: &RawStr,
) -> Result<Redirect, String> {
    let cookie = cookies.get_private("oauth2_state");
    let mut cookie_state = "pas de cookie".to_string();
    if let Some(ref cookie) = cookie {
        cookie_state = cookie.value().to_string();
    };
    // check the state code
    let strava_auth = sc.api().strava_auth_from_code(code.to_string());
    if strava_auth.is_none() {
        return Err("Wrong response from Strava".to_string());
    }
    let strava = strava_auth.unwrap();

    // athlete's Strava id is unknown in the stravauser table ?
    let res = StravaUser::find_by_id(&*conn, strava.athlete.id);
    let mut user: Option<User> = None;
    // store in the stravauser table
    if res.is_err() {
        println!("stravauser not found #{}", strava.athlete.id);
        let res_user: QueryResult<User>;
        if let Some(urt) = opt_urt {
            println!("UserRolesToken: {}", urt.login);
            res_user = User::find(&*conn, urt.id);
        } else {
            println!("No UserRolesToken");
            let new_user = User {
                id: None,
                login: strava.athlete.username,
                name: None,
                password: "".to_string(),
                rank: None,
                active: Some(Local::now().format("%Y-%m-%d %H:%M:%S").to_string()),
                inactive: None,
            };
            res_user = new_user.insert(&*conn);
        }
        // TODO check if username is already used!
        if let Ok(found) = res_user {
            let user_id = found.id.unwrap();
            user = Some(found);
            let new_strava_user = StravaUser {
                id: Some(strava.athlete.id),
                user: user_id,
                access_token: strava.access_token,
                expires_at: strava.expires_at,
                refresh_token: strava.refresh_token,
                token_type: strava.token_type,
            };
            println!(
                "StravaUser to insert: id={:?}, user={}",
                new_strava_user.id, new_strava_user.user
            );
            match new_strava_user.insert(&*conn) {
                Ok(found) => println!("strava user inserted: {}", found.id.unwrap()),
                Err(err) => println!("strava user insert error: {}", err),
            };
        } else {
            println!("user insert error");
        };
    } else {
        let mut stravauser = res.unwrap();
        let user_id = stravauser.user;
        match User::find(&*conn, user_id) {
            Ok(found) => user = Some(found),
            Err(err) => println!("No user #{}: {}", user_id, err),
        };
        // update bearer token
        stravauser.access_token = strava.access_token;
        stravauser.expires_at = strava.expires_at;
        stravauser.refresh_token = strava.refresh_token;
        match stravauser.update_token(&*conn) {
            Ok(nb) => println!("{} updated rows", nb),
            Err(err) => println!("Strange, updating user #{} failed: {}", user_id, err),
        };
    }
    if let Some(found_user) = user {
        // set a JWT in cookie redirecting the user to /home or previous page
        println!("User found: {}", found_user.login);
        cookies.add(user_to_jwt_cookie(found_user));
        println!(
            "cookies={}, state={}, code={}, scope={}",
            cookie_state,
            state.as_str(),
            code.as_str(),
            scope.as_str()
        );
        Ok(Redirect::to("/"))
    } else {
        Err("No user found!".to_string())
    }
}

#[get("/strava_redirect_connect?<state>&<code>&<scope>&<error>")]
pub fn strava_redirect_connect(
    conn: Database,
    sc: State<StravaConfig>,
    urt: UserRolesToken,
    state: &RawStr,
    code: Option<&RawStr>,
    scope: Option<&RawStr>,
    error: Option<&RawStr>,
) -> Result<Redirect, String> {
    println!("state: {}", state.as_str());
    if scope.is_some() {
        println!("scope: {}", scope.unwrap().as_str());
    }
    // User denied
    if error.is_some() {
        println!("User denied: {}", error.unwrap().as_str());
        return Ok(Redirect::to("/settings/profile"));
    }
    // Strava response
    if code.is_none() {
        println!("Missing code");
        return Ok(Redirect::to("/settings/profile"));
    }
    let strava_auth = sc.api().strava_auth_from_code(code.unwrap().to_string());
    if strava_auth.is_none() {
        return Err("Wrong response from Strava".to_string());
    }
    let strava = strava_auth.unwrap();
    // token
    println!("UserRolesToken: {}", urt.login);
    println!("urt.id: {}", urt.id);
    let res_user = User::find(&*conn, urt.id);
    if res_user.is_err() {
        return Err("Wrong urt.id".to_string());
    }
    // athlete's Strava id is unknown in the stravauser table ?
    println!("StravaUser::find_by_id({})", strava.athlete.id);
    let res = StravaUser::find_by_id(&*conn, strava.athlete.id);
    if res.is_ok() {
        println!("StravaUser already exists: {:?}", res);
        return Ok(Redirect::to("/settings/profile"));
    }
    // store in the stravauser table
    let new_strava_user = StravaUser {
        id: Some(strava.athlete.id),
        user: urt.id,
        access_token: strava.access_token,
        expires_at: strava.expires_at,
        refresh_token: strava.refresh_token,
        token_type: strava.token_type,
    };
    println!(
        "StravaUser to insert: id={:?}, user={}",
        new_strava_user.id, new_strava_user.user
    );
    return match new_strava_user.insert(&*conn) {
        Ok(found) => {
            println!("strava user inserted: {}", found.id.unwrap());
            Ok(Redirect::to("/settings/profile"))
        }
        Err(err) => Err(format!("strava user insert error: {}", err)),
    };
}

#[post("/token", data = "<request>")]
pub fn token(conn: Database, request: Json<DeviceCodeToken>) -> status::Custom<JsonValue> {
    println!("{:?}", request);
    if !request.grant_type.eq("device_code") {
        let msg = format!("grant_type {:?} not handled", request.grant_type);
        println!("Error: {}", msg);
        return status::Custom(
            Status::BadRequest,
            json!({
            "error": "400",
                "message": msg
            }),
        );
    }
    // find OAuthClient
    let oauthclient_request = OAuthClient::find_by_client_id(&*conn, request.client_id.to_owned());
    if oauthclient_request.is_err() {
        let msg = format!("OAuthClient not found for {:?}", request.client_id);
        println!("Error: {}", msg);
        return status::Custom(
            Status::InternalServerError,
            json!({
            "error": "500",
                "message": msg
            }),
        );
    }
    let oauthclient = oauthclient_request.unwrap();
    // find OAuthDeviceCode
    let oauthdevicecode_request = OAuthDeviceCode::find_by_oauthclient_and_device_code(
        &*conn,
        oauthclient.id.unwrap(),
        request.device_code.to_owned(),
    );
    if oauthdevicecode_request.is_err() {
        let msg = format!("OAuthDeviceCode not found for {:?}", request);
        println!("Error: {}", msg);
        return status::Custom(
            Status::InternalServerError,
            json!({
            "error": "500",
                "message": msg
            }),
        );
    }
    let oauthdevicecode = oauthdevicecode_request.unwrap();
    if oauthdevicecode.user == None {
        let msg = format!(
            "OAuthDeviceCode not validated for {:?}, user_code={:?}",
            request, oauthdevicecode.user_code
        );
        println!("Error: {}", msg);
        return status::Custom(
            Status::Ok,
            json!({
            "error": "200",
                "message": msg
            }),
        );
    }
    println!("OAuthDeviceCode validated for {:?}", request);
    // delete OAuthDeviceCode
    OAuthDeviceCode::delete_with_id(&*conn, oauthdevicecode.id.unwrap());
    let user_query = User::find(&*conn, oauthdevicecode.user.unwrap());
    if user_query.is_err() {
        let msg = format!(
            "User who validated OAuthDeviceCode not found for {:?}",
            request
        );
        println!("Error: {}", msg);
        return status::Custom(
            Status::InternalServerError,
            json!({
            "error": "500",
                "message": msg
            }),
        );
    }
    let expires_in = ONE_WEEK;
    let token = OAuthToken {
        id: None,
        oauthclient: oauthclient.id.unwrap(),
        user: oauthdevicecode.user.unwrap(),
        access_token: generate_random_code(),
        expires_at: now_as_sec() + expires_in,
        refresh_token: generate_random_code(),
    };
    if token.insert(&*conn).is_err() {
        let msg = format!("Strange, cannot insert OAuthToken {:?}", token);
        println!("Error: {}", msg);
        return status::Custom(
            Status::InternalServerError,
            json!({
            "error": "500",
                "message": msg
            }),
        );
    }

    return status::Custom(
        Status::Ok,
        json!({
            "expires_at": token.expires_at,
            "expires_in": expires_in,
            "access_token": token.access_token,
            "refresh_token": token.refresh_token,
            "token_type": "Bearer",
        }),
    );
}
