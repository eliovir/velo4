use std::collections::HashMap;

use diesel::sql_query;
use diesel::sql_types::{BigInt, Double, Integer, Text};
use diesel::*;
use rocket::http::RawStr;
use rocket::request::Form;
use rocket_contrib::json::Json;
use rocket_contrib::msgpack::MsgPack;
use rocket_contrib::templates::Template;
use rocket_slog::SyncLogger;

use models::database::Database;
use request::jwt::UserRolesToken;

#[derive(QueryableByName, Serialize)]
pub struct ActivityYear {
    #[sql_type = "Text"]
    year: String,
}

#[derive(QueryableByName)]
pub struct QueryStatistics {
    #[sql_type = "Integer"]
    year: i32,
    #[sql_type = "Integer"]
    month: i32,
    #[sql_type = "Double"]
    value: f64,
}

#[derive(QueryableByName, Serialize)]
pub struct GeneralStatistics {
    #[sql_type = "Integer"]
    nb: i32,
    #[sql_type = "Double"]
    distance: f64,
    #[sql_type = "Double"]
    duration: f64,
    #[sql_type = "Integer"]
    ascent: i32,
    #[sql_type = "Double"]
    max_distance: f64,
    #[sql_type = "Double"]
    max_duration: f64,
    #[sql_type = "Integer"]
    max_ascent: i32,
    #[sql_type = "Double"]
    max_speed: f64,
}

#[derive(QueryableByName, Serialize)]
pub struct Histogram {
    #[sql_type = "Integer"]
    nb: i32,
    #[sql_type = "Double"]
    value: f64,
}

#[derive(Clone, QueryableByName, Serialize)]
pub struct ScatterPoint {
    #[sql_type = "Double"]
    x: f64,
    #[sql_type = "Double"]
    y: f64,
    #[sql_type = "Text"]
    date: String,
    #[sql_type = "Integer"]
    id: i32,
}

#[derive(Clone, Serialize)]
pub struct ScatterSeries {
    name: String,
    data: Vec<ScatterPoint>,
}

#[derive(QueryableByName, Serialize)]
pub struct Timeline {
    #[sql_type = "BigInt"]
    timestamp: i64,
    #[sql_type = "Double"]
    duration: f64,
    #[sql_type = "Double"]
    distance: f64,
    #[sql_type = "Integer"]
    ascent: i32,
    #[sql_type = "Integer"]
    nb: i32,
}

#[derive(Serialize)]
pub struct Series {
    name: String,
    data: Vec<f64>,
}

#[derive(Serialize)]
pub struct Response<T> {
    error: bool,
    msg: String,
    title: String,
    value: Option<T>,
}

#[derive(FromForm)]
pub struct StatisticsParameters {
    aggregation: String,
    value: String,
    group: String,
    years: String,
}

fn get_group_clause(group: &str) -> &str {
    match group {
        "" | "all" => "",
        "alone" => "AND LOWER(description) NOT LIKE '%avec%'",
        "notalone" => "AND LOWER(description) LIKE '%avec%'",
        "sjvc" => "AND LOWER(description) LIKE '%sjvc%'",
        "actp" => "AND LOWER(description) LIKE '%actp%'",
        "vcn" => "AND LOWER(description) LIKE '%vcn%'",
        _ => panic!("{} is not handled!", group),
    }
}

fn get_year_clause(years: &str) -> String {
    if years.contains("all") {
        return String::from("");
    }
    format!(
        "AND STRFTIME('%Y', date) IN ('{}')",
        years.replace(".", "', '").as_str()
    )
}

fn get_years(conn: Database, urt: UserRolesToken) -> Vec<ActivityYear> {
    let sql_expression = format!(
        "SELECT DISTINCT STRFTIME('%Y', date) AS year FROM activity WHERE user={} ORDER BY year",
        urt.id
    );
    sql_query(sql_expression)
        .get_results::<ActivityYear>(&*conn)
        .expect("Can't query activity years")
}

#[get("/")]
pub fn index(conn: Database, urt: UserRolesToken) -> Template {
    let mut context = HashMap::new();
    context.insert("years", get_years(conn, urt));
    Template::render("statistics", &context)
}

#[get("/annual")]
pub fn annual(_urt: UserRolesToken) -> Template {
    let context: HashMap<&str, i32> = HashMap::new();
    Template::render("statistics-annual", &context)
}

#[get("/annual?<param..>", format = "json")]
pub fn annual_json(
    conn: Database,
    urt: UserRolesToken,
    param: Form<StatisticsParameters>,
) -> Json<Response<Series>> {
    Json(annual_response(conn, urt, param))
}

/// Get statistics by year.
fn annual_response(
    conn: Database,
    urt: UserRolesToken,
    param: Form<StatisticsParameters>,
) -> Response<Series> {
    let sql_expression = match param.value.as_ref() {
        "weight" => {
            format!("SELECT STRFTIME('%Y', date) AS year, 0 AS month, ROUND({0}(weight), 2) AS value FROM health WHERE user={1} AND weight IS NOT NULL GROUP BY year ORDER BY year",
            param.aggregation,
            urt.id)
        }
        _ => {
            let group_clause = get_group_clause(param.group.as_ref());
            format!("SELECT STRFTIME('%Y', date) AS year, 0 AS month, ROUND({0}({1}), 2) AS value FROM activity WHERE user={3} AND {1} IS NOT NULL {2} GROUP BY year ORDER BY year",
                param.aggregation,
                param.value,
                group_clause,
                urt.id
            )
        }
    };
    // http://docs.diesel.rs/diesel/fn.sql_query.html
    let query = sql_query(sql_expression.as_str());
    let stats = query
        .load::<QueryStatistics>(&*conn)
        .expect("Can't query statistics");
    let mut data: Vec<f64> = Vec::new();
    let mut years: Vec<String> = Vec::new();
    for stat in stats {
        years.push(stat.year.to_string());
        data.push(stat.value);
    }
    let name = format!("{}({})", param.aggregation, param.value);
    let series = Series { name, data };
    let title = format!("{}({}) {}", param.aggregation, param.value, param.group);
    Response::<Series> {
        error: false,
        msg: years.join(", "),
        title,
        value: Some(series),
    }
}

/// Extract general statistics for the period
fn general_xxxx(conn: Database, urt: UserRolesToken, period: &str) -> Response<GeneralStatistics> {
    let clause = match period {
        "all" => "1",
        "4weeks" => "date >= DATE(CURRENT_TIMESTAMP, '-28 days')",
        "365days" => "date >= DATE(CURRENT_TIMESTAMP, '-365 days')",
        "year" => "date >= DATE(CURRENT_TIMESTAMP, 'start of year')",
        _ => panic!("{} is not handled!", period),
    };
    let sql_expression = format!("SELECT COUNT(*) AS nb, ROUND(SUM(distance), 2) AS distance, ROUND(SUM(SUBSTR(duration,1,2) + SUBSTR(duration,4,2)/60. + SUBSTR(duration,7,2)/3600.), 2) AS duration, SUM(ascent) AS ascent, MAX(distance) AS max_distance, MAX(SUBSTR(duration,1,2) + SUBSTR(duration,4,2)/60. + SUBSTR(duration,7,2)/3600.) AS max_duration, MAX(ascent) AS max_ascent, MAX(max_speed) AS max_speed FROM activity WHERE user={} AND {}", urt.id, clause);
    let query = sql_query(sql_expression.as_str());
    match query.get_result::<GeneralStatistics>(&*conn) {
        Ok(value) => Response::<GeneralStatistics> {
            error: false,
            msg: "Ok".to_string(),
            title: period.to_string(),
            value: Some(value),
        },
        Err(error) => Response::<GeneralStatistics> {
            error: true,
            msg: error.to_string(),
            title: period.to_string(),
            value: None,
        },
    }
}

/// Statistics for the period
#[get("/general?<period>")]
pub fn general(conn: Database, urt: UserRolesToken, period: &RawStr) -> Template {
    let response = general_xxxx(conn, urt, period.as_str());
    let mut map = HashMap::new();
    map.insert("value", response.value);
    Template::render("general", &map)
}

/// Statistics for the current year
#[get("/general")]
pub fn general_year(conn: Database, urt: UserRolesToken) -> Template {
    let response = general_xxxx(conn, urt, "year");
    let mut map = HashMap::new();
    if response.value.is_some() {
        map.insert("value", response.value.unwrap());
    }
    Template::render("general", &map)
}

#[get("/histogram")]
pub fn histogram(conn: Database, urt: UserRolesToken) -> Template {
    let mut context = HashMap::new();
    context.insert("years", get_years(conn, urt));
    Template::render("statistics-histogram", &context)
}

#[get("/histogram?<param..>", format = "json")]
pub fn histogram_json(
    conn: Database,
    logger: SyncLogger,
    urt: UserRolesToken,
    param: Form<StatisticsParameters>,
) -> Json<Response<Vec<Histogram>>> {
    let group_clause = get_group_clause(param.group.as_ref());
    let year_clause = get_year_clause(param.years.as_ref());
    let sql_expression = format!("SELECT COUNT(*) AS nb, ROUND({0}/{1}) * {1} AS value FROM activity WHERE user={4} AND {0} IS NOT NULL AND {0} != 0 {2} {3} GROUP BY value ORDER BY value",
            param.value,
            param.aggregation,
            group_clause,
            year_clause,
            urt.id);
    debug!(logger.get(), "SQL : {:?}", sql_expression);
    // http://docs.diesel.rs/diesel/fn.sql_query.html
    // https://docs.diesel.rs/diesel/query_dsl/trait.RunQueryDsl.html#method.get_results
    let query = sql_query(sql_expression.as_str());
    let values = query
        .get_results::<Histogram>(&*conn)
        .expect("Can't query statistics");

    Json(Response::<Vec<Histogram>> {
        error: false,
        msg: "Ok".to_string(),
        title: "title".to_string(),
        value: Some(values),
    })
}

#[get("/scatter")]
pub fn scatter(conn: Database, urt: UserRolesToken) -> Template {
    let mut context = HashMap::new();
    context.insert("years", get_years(conn, urt));
    Template::render("statistics-scatter", &context)
}

#[get("/scatter?<param..>", format = "json")]
pub fn scatter_json(
    conn: Database,
    logger: SyncLogger,
    urt: UserRolesToken,
    param: Form<StatisticsParameters>,
) -> Json<Response<Vec<ScatterSeries>>> {
    let group_clause = get_group_clause(param.group.as_ref());
    let year_clause = get_year_clause(param.years.as_ref());
    let sql_expression = format!("SELECT {0} AS x, {1} AS y, date, id FROM activity WHERE user={4} AND {0} IS NOT NULL AND {0} != 0 {2} {3} ORDER BY date",
            param.value,
            param.aggregation,
            group_clause,
            year_clause,
            urt.id);
    debug!(logger.get(), "SQL : {:?}", sql_expression);
    // http://docs.diesel.rs/diesel/fn.sql_query.html
    // https://docs.diesel.rs/diesel/query_dsl/trait.RunQueryDsl.html#method.get_results
    let query = sql_query(sql_expression.as_str());
    let points = query
        .get_results::<ScatterPoint>(&*conn)
        .expect("Can't query statistics");
    let mut map: HashMap<String, ScatterSeries> = HashMap::new();
    for point in points {
        let year = point.date.get(0..4).unwrap();
        if !map.contains_key(year) {
            let data: Vec<ScatterPoint> = Vec::new();
            let scatter_series = ScatterSeries {
                name: year.to_string(),
                data: data,
            };
            map.insert(year.to_string(), scatter_series);
        }
        map.get_mut(year).unwrap().data.push(point);
    }
    let series = map.values().cloned().collect();
    Json(Response::<Vec<ScatterSeries>> {
        error: false,
        msg: "Ok".to_string(),
        title: "title".to_string(),
        value: Some(series),
    })
}

/// Last weeks / months
#[get("/timeline?<param..>")]
pub fn timeline(
    conn: Database,
    urt: UserRolesToken,
    param: Form<StatisticsParameters>,
) -> Json<Response<Vec<Timeline>>> {
    let start = match param.group.as_ref() {
        "months" => "DATE(date, 'start of month')",
        "weeks" => "DATE(date, 'weekday 0', '-7 days')",
        _ => panic!("{} is not handled!", param.group),
    };
    let sql_expression = format!("SELECT STRFTIME('%s000', {}) AS timestamp, ROUND(SUM(SUBSTR(duration,1,2) + SUBSTR(duration,4,2)/60. + SUBSTR(duration,7,2)/3600.), 2) AS duration, ROUND(SUM(distance), 2) AS distance, SUM(ascent) AS ascent, COUNT(*) AS nb FROM activity WHERE user={} AND date > DATE(CURRENT_TIMESTAMP, '-1 year') GROUP BY timestamp", start, urt.id);
    // http://docs.diesel.rs/diesel/fn.sql_query.html
    let query = sql_query(sql_expression.as_str());
    let series = query
        .get_results::<Timeline>(&*conn)
        .expect("Can't query statistics");

    Json(Response::<Vec<Timeline>> {
        error: false,
        msg: "Ok".to_string(),
        title: "title".to_string(),
        value: Some(series),
    })
}

/// Get statistics by year and month.
fn monthly_response(
    conn: Database,
    urt: UserRolesToken,
    param: Form<StatisticsParameters>,
) -> Response<Vec<Series>> {
    let group_clause = get_group_clause(param.group.as_ref());
    let year_clause = get_year_clause(param.years.as_ref());
    let sql_expression = match param.value.as_ref() {
        "weight" => 
            format!("SELECT STRFTIME('%Y', date) AS year, STRFTIME('%m', date) AS month, ROUND({0}(weight), 2) AS value FROM health WHERE user={1} AND weight IS NOT NULL {2} GROUP BY year, month ORDER BY year, month",
            param.aggregation,
            urt.id,
            year_clause),
        _ => format!("SELECT STRFTIME('%Y', date) AS year, STRFTIME('%m', date) AS month, ROUND({0}({1}), 2) AS value FROM activity WHERE user={4} AND {1} IS NOT NULL {2} {3} GROUP BY year, month ORDER BY year, month",
            param.aggregation,
            param.value,
            group_clause,
            year_clause,
            urt.id
            )
    };
    // http://docs.diesel.rs/diesel/fn.sql_query.html
    let query = sql_query(sql_expression.as_str());
    let stats = query
        .load::<QueryStatistics>(&*conn)
        .expect("Can't query statistics");
    let mut series: Vec<Series> = Vec::new();
    let mut previous_year = -1;
    let mut data: Vec<f64> = Vec::new();
    let mut month = 1;
    for stat in stats {
        if previous_year == -1 {
            previous_year = stat.year;
        }
        if previous_year != stat.year {
            let name = format!("{}", previous_year);
            let a_series = Series { name, data };
            series.push(a_series);
            data = Vec::new();
            previous_year = stat.year;
            month = 1;
        }
        while month < stat.month {
            data.push(0.);
            month = month + 1;
        }
        data.push(stat.value);
        month = month + 1;
    }
    let name = format!("{}", previous_year);
    let a_series = Series { name, data };
    series.push(a_series);
    let title = format!("{}({}) {}", param.aggregation, param.value, param.group);
    Response::<Vec<Series>> {
        error: false,
        msg: "Ok".to_string(),
        title,
        value: Some(series),
    }
}
#[get("/monthly?<param..>", format = "application/json")]
pub fn monthly(
    conn: Database,
    urt: UserRolesToken,
    param: Form<StatisticsParameters>,
) -> Json<Response<Vec<Series>>> {
    Json(monthly_response(conn, urt, param))
}
#[get("/monthly?format=msgpack&<param..>", format = "application/msgpack")]
pub fn monthly_msgpack(
    conn: Database,
    urt: UserRolesToken,
    param: Form<StatisticsParameters>,
) -> MsgPack<Response<Vec<Series>>> {
    MsgPack(monthly_response(conn, urt, param))
}
