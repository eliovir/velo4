use chrono::prelude::*;
use diesel::SqliteConnection;
use rocket::http::RawStr;
use rocket::request::{FlashMessage, Form};
use rocket::response::{Flash, Redirect};
use rocket::State;
use rocket_contrib::templates::Template;
use rocket_slog::SyncLogger;

use models::activity::Activity;
use models::bike::Bike;
use models::database::Database;
use models::stravaactivity::StravaActivity;
use models::stravaactivity::StravaActivityType;
use models::stravaactivity::StravaActivityWithMap;
use models::stravaactivity::StravaMap;
use models::stravauser::StravaUser;
use request::jwt::UserRolesToken;
use strava::api::StravaSummaryActivity;
use strava::config::StravaConfig;

use std::collections::HashMap;

#[derive(Debug, Serialize)]
struct Filter {
    bike_id: Option<i32>,
    description: Option<String>,
    distance_min: Option<f64>,
    distance_max: Option<f64>,
    year: Option<f64>,
}

#[derive(Debug, Serialize)]
struct MapActivity {
    date: String,
    polyline: String,
}

#[derive(Debug, Serialize)]
struct AssociateContext {
    activities: Vec<Activity>,
    strava_activity_id: i64,
}

#[derive(Debug, Serialize)]
struct Context<'a, 'b> {
    msg: Option<(&'a str, &'b str)>,
    activities: Vec<Activity>,
    bikes: Vec<Bike>,
    filter: Option<Filter>,
    user_id: i32,
}

impl<'a, 'b> Context<'a, 'b> {
    pub fn err(
        conn: &SqliteConnection,
        msg: &'a str,
        user_id: i32,
        filter: Option<Filter>,
    ) -> Context<'static, 'a> {
        Context::raw(conn, Some(("error", msg)), user_id, filter)
    }

    pub fn raw(
        conn: &SqliteConnection,
        msg: Option<(&'a str, &'b str)>,
        user_id: i32,
        filter: Option<Filter>,
    ) -> Context<'a, 'b> {
        Context {
            msg,
            activities: Activity::find_all_by_user(conn, user_id),
            bikes: Bike::find_all_enabled_by_user(&*conn, user_id),
            filter,
            user_id,
        }
    }
}

#[derive(Debug, Serialize)]
struct EditContext<'a, 'b> {
    msg: Option<(&'a str, &'b str)>,
    activity: Activity,
    bikes: Vec<Bike>,
    user_id: i32,
    previous_id: Option<i32>,
    previous_date: Option<String>,
    next_id: Option<i32>,
    next_date: Option<String>,
    stravaactivities: Vec<StravaActivity>,
}

impl<'a, 'b> EditContext<'a, 'b> {
    pub fn raw(
        conn: Database,
        urt: UserRolesToken,
        msg: Option<(&'a str, &'b str)>,
        activity: Activity,
    ) -> EditContext<'a, 'b> {
        let mut previous_id: Option<i32> = None;
        let mut previous_date: Option<String> = None;
        let mut next_id: Option<i32> = None;
        let mut next_date: Option<String> = None;
        if let Ok(previous) = Activity::find_previous(&*conn, &activity) {
            previous_id = previous.id;
            previous_date = Some(previous.date);
        }
        if let Ok(next) = Activity::find_next(&*conn, &activity) {
            next_id = next.id;
            next_date = Some(next.date);
        }
        let activity_id = activity.id.unwrap_or(-1);
        EditContext {
            msg,
            activity,
            bikes: Bike::find_all_enabled_by_user(&*conn, urt.id),
            user_id: urt.id,
            previous_id,
            previous_date,
            next_id,
            next_date,
            stravaactivities: StravaActivity::find_all_by_activity(&*conn, activity_id),
        }
    }
}

#[derive(Debug, Serialize)]
struct StravaActivitiesContext {
    activities: Vec<StravaSummaryActivity>,
    after: String,
    before: String,
    next: Option<String>,
}

#[get("/")]
pub fn all(conn: Database, urt: UserRolesToken, msg: Option<FlashMessage>) -> Template {
    Template::render(
        "activities",
        &match msg {
            Some(ref msg) => Context::raw(&*conn, Some((msg.name(), msg.msg())), urt.id, None),
            None => Context::raw(&*conn, None, urt.id, None),
        },
    )
}

#[get("/create")]
pub fn create_get(conn: Database, urt: UserRolesToken) -> Template {
    let user_id = urt.id;
    let now = Local::now();
    let mut last_bike: i32 = 0;
    if let Ok(last) = Activity::find_last_with_user(&*conn, user_id) {
        last_bike = last.bike;
    }
    let activity = Activity {
        id: None,
        date: now.format("%Y-%m-%dT%H:%M:%S").to_string(),
        user: user_id,
        distance: 0f64,
        duration: "".to_string(),
        average: None,
        max_speed: None,
        wind: 0,
        temperature: 0,
        description: None,
        ascent: 0,
        bike: last_bike,
        intensity: "".to_string(),
        total: None,
    };
    Template::render("activity", EditContext::raw(conn, urt, None, activity))
}

#[post("/create", data = "<form>")]
pub fn create(conn: Database, _urt: UserRolesToken, form: Form<Activity>) -> Flash<Redirect> {
    let new_activity = form.into_inner();
    if new_activity.date.is_empty() {
        Flash::error(Redirect::to("/activities"), "Date cannot be empty.")
    } else {
        // already exist ?
        if Activity::find_with_date_user(&*conn, &new_activity.date, new_activity.user).is_ok() {
            let msg = format!(
                "Whoops! The activity at {:?} already exists!",
                new_activity.date
            );
            return Flash::error(Redirect::to("/activities"), msg);
        }
        //
        match new_activity.insert(&*conn) {
            Ok(saved) => {
                if let Some(id) = saved.id {
                    let url = format!("/activities/{}", id);
                    Flash::success(Redirect::found(url), "Activity successfully added.")
                } else {
                    Flash::error(
                        Redirect::to("/activities"),
                        "Whoops! The server failed to get activity.id!",
                    )
                }
            }
            Err(err) => {
                let msg = format!("Whoops! The server failed to save activity! {:?}", err);
                Flash::error(Redirect::to("/activities"), msg)
            }
        }
    }
}

#[delete("/<id>")]
pub fn delete(conn: Database, urt: UserRolesToken, id: i32) -> Result<Flash<Redirect>, Template> {
    if Activity::delete_with_id(&*conn, id) {
        Ok(Flash::success(
            Redirect::to("/activities"),
            "activity-was-deleted",
        ))
    } else {
        Err(Template::render(
            "activities",
            &Context::err(&*conn, "failed-to-delete-activity", urt.id, None),
        ))
    }
}

#[get("/?<bike>&<description>&<distance_min>&<distance_max>&<year>")]
pub fn filter(
    conn: Database,
    urt: UserRolesToken,
    logger: SyncLogger,
    msg: Option<FlashMessage>,
    bike: Option<i32>,
    description: Option<&RawStr>,
    distance_min: Option<f64>,
    distance_max: Option<f64>,
    year: Option<f64>,
) -> Template {
    debug!(logger.get(), "Activities for bike #{:?}", bike);
    let descr = match description {
        Some(txt) => Some(txt.to_string()),
        None => None,
    };
    Template::render(
        "activities",
        Context {
            msg: match msg {
                Some(ref msg) => Some((msg.name(), msg.msg())),
                None => None,
            },
            activities: Activity::find_all_by_user_and_bike_and_description_and_distance_and_year(
                &*conn,
                urt.id,
                bike,
                descr,
                distance_min,
                distance_max,
                year,
            ),
            bikes: Bike::find_all_enabled_by_user(&*conn, urt.id),
            filter: Some(Filter {
                bike_id: bike,
                description: match description {
                    Some(txt) => Some(txt.to_string()),
                    None => None,
                },
                distance_min: distance_min,
                distance_max: distance_max,
                year: year,
            }),
            user_id: urt.id,
        },
    )
}

#[get("/<id>")]
pub fn read(
    conn: Database,
    urt: UserRolesToken,
    id: i32,
    msg: Option<FlashMessage>,
) -> Result<Template, Flash<Redirect>> {
    if let Ok(activity) = Activity::find(&*conn, id) {
        Ok(Template::render(
            "activity",
            &match msg {
                Some(ref msg) => {
                    EditContext::raw(conn, urt, Some((msg.name(), msg.msg())), activity)
                }
                None => EditContext::raw(conn, urt, None, activity),
            },
        ))
    } else {
        Err(Flash::error(
            Redirect::to("/activities"),
            "failed-to-get-activity",
        ))
    }
}

use rocket::request::{FormItems, FromForm};
pub struct MapQueryParam {
    id: Vec<i32>,
    details: String,
}
impl<'f> FromForm<'f> for MapQueryParam {
    type Error = ();
    fn from_form(items: &mut FormItems<'f>, strict: bool) -> Result<Self, Self::Error> {
        let mut param = MapQueryParam {
            id: Vec::new(),
            details: String::new(),
        };
        for item in items {
            let value = item.value.url_decode().map_err(|_| ())?;
            match item.key.as_str() {
                "id" => param.id.push(value.parse::<i32>().unwrap()),
                "details" => param.details = value,
                _ if strict => return Err(()),
                _ => (),
            }
        }
        Ok(param)
    }
}

#[post("/map", data = "<query>")]
pub fn read_map(
    conn: Database,
    logger: SyncLogger,
    urt: UserRolesToken,
    query: Form<MapQueryParam>,
) -> Result<Template, Flash<Redirect>> {
    let mut context = HashMap::new();
    let mut data: Vec<MapActivity> = Vec::new();
    for aid in &query.id {
        let stravaactivities = StravaActivity::find_all_by_activity(&*conn, *aid);
        debug!(
            logger.get(),
            "{} stravaactivities for user #{}'s activity #{}",
            stravaactivities.len(),
            urt.id,
            *aid
        );
        if stravaactivities.is_empty() {
            continue;
        }
        for stravaactivity in stravaactivities {
            if stravaactivity.stravamap.is_none() {
                continue;
            }
            let stravamap_result = StravaMap::find(&*conn, stravaactivity.stravamap.unwrap());
            if stravamap_result.is_err() {
                continue;
            }
            let stravamap = stravamap_result.unwrap();
            let polyline_option = match query.details.as_str() {
                "summary_polyline" => stravamap.summary_polyline,
                "polyline" => stravamap.polyline,
                _ => panic!("details not handled: {}", query.details),
            };
            if let Some(polyline) = polyline_option {
                data.push(MapActivity {
                    date: stravaactivity.start_date_local,
                    polyline: polyline,
                });
            } else {
                debug!(
                    logger.get(),
                    "no {} for stravaactivity #{}", query.details, stravaactivity.id
                );
            }
        }
    }
    context.insert("activities", data);
    Ok(Template::render("activity-map", context))
}

#[get("/strava-activities?<after>&<before>")]
pub fn stravaactivities(
    conn: Database,
    logger: SyncLogger,
    sc: State<StravaConfig>,
    urt: UserRolesToken,
    after: Option<&RawStr>,
    before: Option<&RawStr>,
) -> Result<Template, Flash<Redirect>> {
    let before_dt;
    let next;
    match before {
        Some(date) => {
            before_dt = format!("{}T23:59:59Z", date)
                .parse::<DateTime<Utc>>()
                .unwrap();
            if before_dt < Utc::now() {
                next = Some(
                    (before_dt + chrono::Duration::weeks(1))
                        .format("%Y-%m-%d")
                        .to_string(),
                );
            } else {
                next = None;
            }
        }
        None => {
            before_dt = Utc::now();
            next = None;
        }
    };
    let after_dt = match after {
        Some(date) => {
            println!("Date after: {}", date);
            format!("{}T00:00:00Z", date)
                .parse::<DateTime<Utc>>()
                .unwrap()
        }
        None => before_dt - chrono::Duration::weeks(1),
    };
    let page = 1;
    let per_page = 30;
    let mut activities: Vec<StravaSummaryActivity> = vec![];
    match StravaUser::find_all_by_user(&*conn, urt.id) {
        Ok(stravausers) => {
            debug!(
                logger.get(),
                "{} stravausers for user #{}",
                stravausers.len(),
                urt.id
            );
            for mut stravauser in stravausers {
                debug!(logger.get(), "stravauser: {:?}", stravauser.id);
                if stravauser.is_expired() {
                    debug!(logger.get(), "refreshing token");
                    if let Some(newstravauser) = sc
                        .api()
                        .strava_auth_from_refresh_token(stravauser.refresh_token.to_string())
                    {
                        stravauser.access_token = newstravauser.access_token;
                        stravauser.expires_at = newstravauser.expires_at;
                        stravauser.refresh_token = newstravauser.refresh_token;
                        if stravauser.update_token(&*conn).is_err() {
                            debug!(logger.get(), "error while updating token");
                        }
                    } else {
                        debug!(logger.get(), "refreshing token failed");
                    }
                }
                if let Some(stravaactivities) = sc.api().activities(
                    stravauser,
                    before_dt.timestamp(),
                    after_dt.timestamp(),
                    page,
                    per_page,
                ) {
                    for mut a in stravaactivities {
                        if a.activitytype != StravaActivityType::Ride {
                            continue;
                        }
                        if let Ok(a2) = StravaActivity::find(&*conn, a.id) {
                            a.activity = a2.activity;
                        };
                        activities.push(a);
                    }
                } else {
                    debug!(logger.get(), "Getting Strava activities failed");
                }
            }
        }
        Err(_err) => {
            debug!(logger.get(), "Whoops! StravaUser not retrieved!");
        }
    };
    let context = StravaActivitiesContext {
        activities,
        after: after_dt.format("%Y-%m-%d").to_string(),
        before: before_dt.format("%Y-%m-%d").to_string(),
        next,
    };
    Ok(Template::render("stravaactivities", context))
}

fn get_stravaactivity(
    conn: &SqliteConnection,
    log: &slog::Logger,
    sc: State<StravaConfig>,
    user: i32,
    id: i64,
) -> Option<StravaActivityWithMap> {
    return match StravaUser::find_all_by_user(conn, user) {
        Ok(stravausers) => {
            debug!(log, "{} stravausers for user #{}", stravausers.len(), user);
            for mut stravauser in stravausers {
                debug!(log, "stravauser: {:?}", stravauser.id);
                if stravauser.is_expired() {
                    debug!(log, "refreshing token");
                    if let Some(newstravauser) = sc
                        .api()
                        .strava_auth_from_refresh_token(stravauser.refresh_token.to_string())
                    {
                        stravauser.access_token = newstravauser.access_token;
                        stravauser.expires_at = newstravauser.expires_at;
                        stravauser.refresh_token = newstravauser.refresh_token;
                        if stravauser.update_token(conn).is_err() {
                            debug!(log, "error while updating token");
                        }
                    } else {
                        debug!(log, "refreshing token failed");
                    }
                }
                let res = sc.api().activity(stravauser, id);
                if res.is_some() {
                    return res;
                }
            }
            debug!(log, "No activity #{} found!", id);
            None
        }
        Err(_err) => {
            debug!(log, "Whoops! StravaUser not retrieved!");
            None
        }
    };
}

#[get("/strava-add/<id>")]
pub fn strava_add(
    conn: Database,
    logger: SyncLogger,
    sc: State<StravaConfig>,
    urt: UserRolesToken,
    id: i64,
) -> Flash<Redirect> {
    if let Some(activity_with_map) = get_stravaactivity(&*conn, logger.get(), sc, urt.id, id) {
        let mut activity = activity_with_map.stravaactivity;
        println!("Strava activity: {:?}", activity);
        if let Some(stravamap) = activity_with_map.map {
            debug!(logger.get(), "Inserting StravaMap {:?}", stravamap);
            match stravamap.insert(&*conn) {
                Ok(inserted) => {
                    debug!(logger.get(), "StravaMap inserted");
                    activity.stravamap = Some(inserted.id);
                }
                Err(err) => {
                    let msg = format!("Error while inserting StravaMap: {}", err);
                    debug!(logger.get(), "{}", msg);
                    return Flash::error(Redirect::to("/activities/strava-activities"), msg);
                }
            };
        }
        let mut a = Activity::from(activity.clone());
        a.user = urt.id;
        match a.insert(&*conn) {
            Ok(inserted) => {
                activity.activity = inserted.id;
                match activity.insert(&*conn) {
                    Ok(_) => {
                        return Flash::success(
                            Redirect::to(format!("/activities/{}", inserted.id.unwrap())),
                            format!("Activity #{} created.", inserted.id.unwrap()),
                        );
                    }
                    Err(err) => {
                        println!("Error while inserting StravaActivity: {}", err);
                    }
                };
            }
            Err(err) => {
                println!("Error while inserting Activity: {}", err);
            }
        };
    } else {
        println!("Getting Strava activitiy failed");
        debug!(logger.get(), "Getting Strava activitiy failed");
    };
    Flash::success(
        Redirect::to("/activities/strava-activities"),
        format!("Activity #{} found.", id),
    )
}

#[get("/strava-associate/<id>/<day>")]
pub fn strava_associate_get(
    conn: Database,
    urt: UserRolesToken,
    id: i64,
    day: &RawStr,
) -> Template {
    let activities = Activity::find_all_by_user_and_day(&*conn, urt.id, day.to_string());
    let context = AssociateContext {
        strava_activity_id: id,
        activities,
    };
    Template::render("stravaassociate", context)
}

#[post("/strava-associate/<strava_activity_id>/<activity_id>")]
pub fn strava_associate_post(
    conn: Database,
    logger: SyncLogger,
    sc: State<StravaConfig>,
    urt: UserRolesToken,
    strava_activity_id: i64,
    activity_id: i32,
) -> Flash<Redirect> {
    let res = Activity::find(&*conn, activity_id);
    if res.is_err() {
        return Flash::error(Redirect::to("/activities"), "failed-to-get-activity");
    }
    if let Some(stravaactivity_with_map) =
        get_stravaactivity(&*conn, logger.get(), sc, urt.id, strava_activity_id)
    {
        let mut stravaactivity = stravaactivity_with_map.stravaactivity;
        let mut activity = res.unwrap();
        let converted = Activity::from(stravaactivity.clone());
        let mut updated = false;
        if activity.ascent == 0 {
            activity.ascent = converted.ascent;
            updated = true;
        }
        if activity.average.is_none() {
            activity.average = converted.average;
            updated = true;
        }
        if !activity.date.contains("T") {
            activity.date = converted.date;
            updated = true;
        }
        if activity.max_speed.is_none() {
            activity.max_speed = converted.max_speed;
            updated = true;
        }
        if activity.temperature == 0 {
            activity.temperature = converted.temperature;
            updated = true;
        }
        if updated {
            match activity.update(&*conn) {
                Ok(nb) => debug!(logger.get(), "{} activity #{} updated.", nb, activity_id),
                Err(err) => {
                    return Flash::error(
                        Redirect::to("/activities"),
                        format!("Failed to update activity #{}: {}", activity_id, err),
                    )
                }
            }
        }
        // TODO insert stravamap
        stravaactivity.activity = Some(activity_id);
        return match stravaactivity.insert(&*conn) {
            Ok(_) => Flash::success(
                Redirect::to(format!("/activities/{}", activity_id)),
                format!(
                    "Activity #{} associated with StravaActivity #{}.",
                    activity_id, strava_activity_id
                ),
            ),
            Err(err) => Flash::success(
                Redirect::to("/activities/strava-activities"),
                format!(
                    "Error while inserting StravaActivity #{}: {}",
                    strava_activity_id, err
                ),
            ),
        };
    } else {
        Flash::error(
            Redirect::to("/activities/strava-activities"),
            format!("Strange, no StravaActivity #{} found.", strava_activity_id),
        )
    }
}

#[post("/", data = "<form>")]
pub fn update(conn: Database, _urt: UserRolesToken, form: Form<Activity>) -> Flash<Redirect> {
    let update = form.into_inner();
    if let Some(id) = update.id {
        match Activity::find(&*conn, id) {
            Ok(activity) => {
                if activity.update_with(&*conn, update).is_ok() {
                    let url = format!("/activities/{}", id);
                    Flash::success(Redirect::found(url), "Activity successfully updated.")
                } else {
                    Flash::error(Redirect::to("/activities"), "failed-to-update-activity")
                }
            }
            Err(err) => {
                let msg = format!("Whoops! The server failed to get activity! {:?}", err);
                Flash::error(Redirect::to("/activities"), msg)
            }
        }
    } else {
        let msg = "Whoops! No activity.id given!";
        Flash::error(Redirect::to("/activities"), msg)
    }
}
