//! Controllers

pub mod activities;
pub mod auth;
pub mod bikes;
pub mod healths;
pub mod maintenances;
pub mod settings;
pub mod static_files;
pub mod statistics;
