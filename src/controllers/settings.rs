use rocket::response::{Flash, Redirect};
use rocket_contrib::templates::Template;

use models::database::Database;
use models::stravauser::StravaUser;
use models::user::User;
use request::jwt::UserRolesToken;

#[derive(Serialize)]
struct Profile {
    login: String,
    name: String,
    stravauser_ids: Vec<i32>,
}

#[get("/profile")]
pub fn profile(
    conn: Database,
    opt_urt: Option<UserRolesToken>,
) -> Result<Template, Flash<Redirect>> {
    return match opt_urt {
        None => {
            let msg = "Whoops! Not connected!";
            Err(Flash::error(Redirect::to("/auth/login"), msg))
        }
        Some(urt) => match User::find(&*conn, urt.id) {
            Ok(user) => {
                let mut context = Profile {
                    login: urt.login,
                    name: user.name.unwrap_or("".to_string()),
                    stravauser_ids: vec![],
                };
                match StravaUser::find_all_by_user(&*conn, urt.id) {
                    Ok(stravausers) => {
                        println!("{} stravausers for user #{}", stravausers.len(), urt.id);
                        for stravauser in stravausers {
                            println!("stravauser: {:?}", stravauser.id);
                            match stravauser.id {
                                Some(id) => context.stravauser_ids.push(id),
                                None => (),
                            };
                        }
                    }
                    Err(_err) => {
                        let msg = "Whoops! StravaUser not retrieved!";
                        return Err(Flash::error(Redirect::to("/home"), msg));
                    }
                }
                Ok(Template::render("profile", &context))
            }
            Err(_err) => {
                let msg = "Whoops! User not found!";
                Err(Flash::error(Redirect::to("/home"), msg))
            }
        },
    };
}
