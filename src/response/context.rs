use rocket::State;

use request::jwt::UserRolesToken;
use strava::config::StravaConfig;

#[derive(Debug, Serialize)]
pub struct Context<'a, 'b, T> {
    pub data: T,
    pub msg: Option<(&'a str, &'b str)>,
    pub strava_enabled: bool,
    pub user_id: Option<i32>,
}

impl<'a, 'b, T> Context<'a, 'b, T> {
    pub fn err(
        data: T,
        msg: &'a str,
        urt: Option<UserRolesToken>,
        sc: State<StravaConfig>,
    ) -> Context<'static, 'a, T> {
        Context {
            data,
            msg: Some(("error", msg)),
            strava_enabled: sc.is_valid(),
            user_id: match urt {
                Some(v) => Some(v.id),
                None => None,
            },
        }
    }

    pub fn raw(
        data: T,
        msg: Option<(&'a str, &'b str)>,
        urt: Option<UserRolesToken>,
        sc: State<StravaConfig>,
    ) -> Context<'a, 'b, T> {
        Context {
            data,
            msg,
            strava_enabled: sc.is_valid(),
            user_id: match urt {
                Some(v) => Some(v.id),
                None => None,
            },
        }
    }
}
