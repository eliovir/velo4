{% extends "page" %}
{% block title %}application-title-index{% endblock title %}
{% block head %}
    {{ super() }}
    <script src="/js/highcharts.js"></script>
    <style type="text/css">
        .important { color: #336699; }
    </style>
{% endblock head %}
{% block content %}
    <h1 data-l10n-id="activities-12-months"></h1>
    <div id="timeline" class="row" style="height: 400px"></div>
    <div class="input-field col s12">
        <button class="btn waves-effect waves-light" type="submit" name="group" data-l10n-id="by-week" value="weeks"></button>
        <button class="btn waves-effect waves-light" type="submit" name="group" data-l10n-id="by-month" value="months"></button>
    </div>
    <div id="healths"></div>
    <h1 data-l10n-id="application"></h1>
    <p><a class="waves-effect waves-light btn primary" href="/halt"><i class="material-icons">stop</i><span data-l10n-id="application-halt"></span></a></p>
    <p><a class="waves-effect waves-light btn primary" href="/info"><i class="material-icons">info</i><span data-l10n-id="application-title-info"></span></a></p>
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large red waves-effect waves-light">
	    <i class="large material-icons">add</i>
	</a>
	<ul>
            <li><a class="btn-floating blue darken-1" href="activities/create"><i class="material-icons">access_time</i></a>
            <li><a class="btn-floating blue darken-1" href="maintenances/create"><i class="material-icons">build</i></a>
            <li><a class="btn-floating blue darken-1" href="healths/create"><i class="material-icons">accessibility</i></a>
	</ul>
    </div>
    <script type="text/javascript">
    // timeline
    var units = {
        distance: "km",
        duration: "h",
        average: "km/h",
        max_speed: "km/h",
        temperature: "°C",
        ascent: "m",
        nb: ""
    };

    var yAxis = [];
    var attributes = ['distance', 'duration', 'ascent', 'nb'];
    for (x in attributes) {
        var attribute = attributes[x];
        var unit = units[attribute];
        var style = {
            color: Highcharts.getOptions().colors[x]
        };
        var axis = {
            labels: {
                format: '{value}' + unit,
                style: style
            },
            title: {
                text: attribute,
                style: style
            },
            opposite: x > 0
        };
        yAxis.push(axis);
    }

    // TODO http://api.highcharts.com/highcharts/lang

    var chart = Highcharts.chart('timeline', {
        chart: {
            backgroundColor: 'transparent',
            type: 'column'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                pointPadding: 0,
                groupPadding: 0
            }
        },
        series: [],
        title: {
            text: false
        },
        tooltip: {
            shared: true
        },
        xAxis: {
            type: 'datetime',
            minTickInterval: 86400000
        },
        yAxis: yAxis
    });
    function update_chart(value, group) {
        $.ajax({
        method: 'GET',
        url: '/statistics/timeline',
        data: {
            aggregation: '',
            value: value,
            group: group,
            years: ''
        },
        dataType: 'json',
        mimeType: 'application/json'
    }).done(function(data) {
        for (var i = 0, len = chart.series.length; i < len; i++) {
            console.log("remove series");
            chart.series[0].remove();
        }
        for (x in attributes) {
            var attribute = attributes[x];
            var unit = units[attribute];
            var d = [];
            for (var i = 0; i < data.value.length; i++) {
                var dd = [data.value[i].timestamp, data.value[i][attribute]];
                d.push(dd);
            }
            var s = {
                name: attribute,
                data: d,
                tooltip: {
                    valueSuffix: ' ' + unit,
                },
                visible: x == 0,
                yAxis: parseInt(x)
            };
            chart.addSeries(s);
        }
    });
    }
    $('button[name=group]').click(function() {
        update_chart('distance', $(this).val());
    });
    update_chart('distance', 'months');

    // Healths
    var sparkline = new Highcharts.Chart({
        chart: {
            backgroundColor: 'transparent',
            renderTo: 'healths',
            zoomType: 'x'
	},
	title: {
	    text: 'Healths'
	},
	credits: {
	    enabled: false
	},
	xAxis: {
            type: 'datetime',
	},
	yAxis: {
            labels: {
                format: '{value} kg'
            },
            title: {
                text: null
            }
	},
	legend: {
	   enabled: false
	},
        plotOptions: {
           area: {
		fillColor: {
			linearGradient: {
				x1: 0,
				y1: 0,
				x2: 0,
				y2: 1
			},
			stops:
			[
				[0, Highcharts.getOptions().colors[0]],
				[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
			]
		},
		marker: {
			radius: 2
		},
		lineWidth: 1,
		states:	{
			hover: {
				lineWidth: 1
			}
		},
		threshold: null
		}
	},
	toolTip: {
	   enabled: true
	},
	series: [
	]
    });
    function update_sparkline(start, end) {
        $.ajax({
        method: 'GET',
        url: '/healths/weights/' + start + '/' + end,
        dataType: 'json',
        mimeType: 'application/json'
    }).done(function(data) {
        for (var i = 0, len = sparkline.series.length; i < len; i++) {
            console.log("remove series for weights");
            sparkline.series[0].remove();
        }
        var d = [];
        for (var i = 0; i < data.value.length; i++) {
            var dd = [data.value[i].timestamp * 1000, data.value[i].weight];
            d.push(dd);
        }
        var s = {
            data: d,
            name: 'weight',
            type: 'area'
        };
        sparkline.addSeries(s);
    });
    }
    var today = new Date();
    var end = today.getTime();
    var start = end - 31 * 24 * 60 * 60 * 1000;
    update_sparkline(start, end);
    </script>
{% endblock content %}
