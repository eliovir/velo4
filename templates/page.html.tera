<!DOCTYPE html>
<html>
<head>
    {% block head %}
    <link rel="icon" type="image/png" href="/images/favicon.png">
    <title data-l10n-id="{% block title %}{% endblock title %}" data-l10n-args='{% block titleargs %}{% endblock titleargs %}'>Vélo 4</title>
    <meta name="defaultLanguage" content="fr">
    <meta name="availableLanguages" content="en-US, fr">
    <link rel="localization" href="/locales/page.{locale}.ftl">
    <link rel="stylesheet" href="/css/materialize.min.css">
    <link rel="stylesheet" href="/css/material_icons.css">
    <link rel="stylesheet" href="/css/style.css" />
    <script defer src="/js/fluent-web.js"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/materialize.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    {% endblock head %}
</head>
<body>
    <header>
<!-- Dropdown for Activities -->
<ul id="dropdownactivities" class="dropdown-content">
    <li><a href="/activities?year={{ now() | date(format="%Y") }}" data-l10n-id="activities-all"></a></li>
    <li><a href="/activities/create" data-l10n-id="add-activity"></a></li>
    <li><a href="/activities/strava-activities" data-l10n-id="activities-strava"></a></li>
</ul>
<!-- Dropdown for Healths -->
<ul id="dropdownhealths" class="dropdown-content">
    <li><a href="/healths" data-l10n-id="healths"></a></li>
    <li><a href="/healths/create" data-l10n-id="add-health"></a></li>
</ul>
<!-- Dropdown for Statistics -->
<ul id="dropdownstats" class="dropdown-content">
    <li><a href="/statistics/" data-l10n-id="statistics-months"></a></li>
    <li><a href="/statistics/annual" data-l10n-id="statistics-annual"></a></li>
    <li><a href="/statistics/histogram" data-l10n-id="statistics-histogram"></a></li>
    <li><a href="/statistics/scatter" data-l10n-id="statistics-scatter"></a></li>
    <li><a href="/statistics/general/" data-l10n-id="statistics-general"></a></li>
</ul>
<!-- Dropdown for user -->
<ul id="dropdownuser" class="dropdown-content">
    <li><a href="/settings/profile"><i class="material-icons left">account_box</i><span data-l10n-id="user-profile"></span></a></li>
    <li><a href="/auth/password"><i class="material-icons left">vpn_key</i><span data-l10n-id="change-password"></span></a></li>
    <li><a href="/bikes"><i class="material-icons left">directions_bike</i><span data-l10n-id="bikes"></span></a></li>
    <li><a href="/auth/logout"><span data-l10n-id="logout"></span></a></li>
</ul>
    <nav role="navigation">
        <div class="nav-wrapper">
            <a id="logo-container" href="/home" class="brand-logo" data-l10n-id="appName"></a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a class="dropdown-trigger" href="#!" data-target="dropdownactivities"><span data-l10n-id="activities-all"></span><i class="material-icons left">access_time</i><i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a class="dropdown-trigger" href="#!" data-target="dropdownhealths"><span data-l10n-id="healths"></span><i class="material-icons left">accessibility</i><i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a class="dropdown-trigger" href="#!" data-target="dropdownstats"><span data-l10n-id="statistics"></span><i class="material-icons left">insert_chart</i><i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a class="dropdown-trigger" href="#!" data-target="dropdownuser"><span data-l10n-id="user-profile"></span><i class="material-icons left">person</i><i class="material-icons right">arrow_drop_down</i></a></li>
            </ul>
            <ul class="sidenav" id="mobile-demo">
                <li><a href="/home" data-l10n-id="home"></a></li>
                <li><a href="/activities?year={{ now() | date(format="%Y") }}" data-l10n-id="activities-all"></a></li>
                <li><a href="/activities/strava-activities" data-l10n-id="activities-strava"></a></li>
                <li><a href="/healths" data-l10n-id="healths"></a></li>
                <li><a href="/bikes" data-l10n-id="bikes"></a></li>
                <li><a href="/statistics/" data-l10n-id="statistics-months"></a></li>
                <li><a href="/statistics/annual" data-l10n-id="statistics-annual"></a></li>
                <li><a href="/statistics/histogram" data-l10n-id="statistics-histogram"></a></li>
                <li><a href="/statistics/scatter" data-l10n-id="statistics-scatter"></a></li>
                <li><a href="/statistics/general/" data-l10n-id="statistics-general"></a></li>
		<li><a href="/auth/logout"><span data-l10n-id="logout"></span></a></li>
            </ul>
        </div>
    </nav>
    </header>
    <main>
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">{% block content %}{% endblock content %}</div>
    </div>
    </main>
    <footer class="page-footer">
        {% block footer %}
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <p>&copy; Copyright 2017 <a href="https://framagit.org/eliovir" target="_blank">Eliovir</a>.</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <ul class="collapsible no-border">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">folder</i><span data-l10n-id="libraries"></span></div>
                        <div class="collapsible-body">
                            <!-- libraries -->
                            <ul>
                                <li><a class="grey-text text-lighten-3" href="https://doc.rust-lang.org/book/" target="_blank">Rust</a></li>
                                <li><a class="grey-text text-lighten-3" href="https://rocket.rs/guide/getting-started/" target="_blank">Rocket</a></li>
                                <li><a class="grey-text text-lighten-3" href="http://diesel.rs/guides/getting-started/" target="_blank">Diesel</a></li>
                                <li><a class="grey-text text-lighten-3" href="https://github.com/adwhit/diesel-derive-enum" target="_blank">Diesel-derive-enum</a></li>
                                <li><a class="grey-text text-lighten-3" href="https://github.com/Keats/tera" target="_blank">Tera</a></li>
                                <li><a class="grey-text text-lighten-3" href="https://github.com/chronotope/chrono" target="_blank">Chrono</a></li>
                                <li><a class="grey-text text-lighten-3" href="https://github.com/slapresta/rust-dotenv" target="_blank">dotenv</a></li>
                                <li><a class="grey-text text-lighten-3" href="https://docs.rs/crate/jsonwebtoken/5.0.1" target="_blank">jsonwebtoken</a></li>
                                <li><a class="grey-text text-lighten-3" href="https://docs.rs/attohttpc/" target="_blank">attohttpc</a></li>
                                <li class="divider"></li>
                                <li><a class="grey-text text-lighten-3" href="https://projectfluent.org/" target="_blank">Fluent.js</a></li>
                                <li><a class="grey-text text-lighten-3" href="http://materializecss.com/getting-started.html" target="_blank">Materialize CSS</a></li>
                                <li><a class="grey-text text-lighten-3" href="https://www.highcharts.com/products/highcharts" target="_blank">Highcharts</a></li>
				<li><a class="grey-text text-lighten-3" href="https://leafletjs.com/" target="_blank">Leaflet</a></li>
				<li><a class="grey-text text-lighten-3" href="https://github.com/brunob/leaflet.fullscreen" target="_blank">Leaflet.Control.FullScreen</a></li>
				<li><a class="grey-text text-lighten-3" href="https://github.com/jieter/Leaflet.encoded" target="_blank">Leaflet.encoded</a></li>
                             </ul>
                         </div>
                     </li>
                 </ul>
             </div>
        </div>
    </div>
        {% endblock footer %}
    </footer>
    <!--  Scripts-->
    <script src="/js/init.js"></script>
</body>
</html>
