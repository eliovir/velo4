{% extends "page" %}
{% block title %}application-title-strava-activities{% endblock title %}
{% block titleargs %}{"after": "{{after}}", "before": "{{before}}", "nb": "{{activities | length}}"}{% endblock titleargs %}
{% block content %}

    <div class="row">
      <h4 data-l10n-id="strava-activities" data-l10n-args='{"after": "{{after}}", "before": "{{before}}", "nb": "{{activities | length}}"}'></h4>
          <p>
            {% if msg %}
            {{msg.0}} : {{msg.1}}
            {% endif %}
          </p>
    </div>

    <div class="row">
      <form action="/activities/strava-activities" method="get">
        <input type="hidden" name="before" value="{{after}}" />
        <div class="input-field col s4">
          <button class="waves-effect waves-light btn" type="submit">&lt; {{after}}</button>
	</div>
      </form>
      <form action="/activities/strava-activities" method="get">
        <div class="input-field col s4">
          <input type="date" class="validate" data-l10n-id="activity-before" name="before" id="before" onchange="this.form.submit()" value="{{before}}">
	  <label for="before" data-l10n-id="activity-before-label"></label>
	</div>
      </form>
      {% if next -%}
      <form action="/activities/strava-activities" method="get">
        <input type="hidden" name="before" value="{{next}}" />
        <div class="input-field col s4">
          <button class="waves-effect waves-light btn" type="submit">{{next}} &gt;</button>
	</div>
      </form>
      {%- endif %}
    </div>
    <div class="row">
      <div class="twelve columns">
        <table class="striped highlight">
          <thead>
            <tr>
              <th data-field="date" data-l10n-id="activity-date"></th>
              <th data-field="description" data-l10n-id="activity-description"></th>
              <th data-field="distance" data-l10n-id="activity-distance"></th>
              <th data-field="duration" data-l10n-id="activity-duration"></th>
              <th data-field="average" data-l10n-id="activity-average"></th>
              <th data-field="max_speed" data-l10n-id="activity-max_speed"></th>
              <th data-field="ascent" data-l10n-id="activity-ascent"></th>
              <th data-l10n-id="col-action"></th>
            </tr>
          </thead>
          <tbody>
            {% for activity in activities -%}
            <tr>
              <td><a href="https://www.strava.com/activities/{{activity.id}}" data-l10n-id="show-strava-activity" target="_blnk">{{activity.start_date_local}}</a></td>
              <td>{{activity.name}}</td>
              <td>{{activity.distance}}</td>
              <td>{{activity.moving_time}}</td>
              <td>{{activity.average_speed | round(method="ceil", precision=2)}}</td>
              <td>{{activity.max_speed | round(method="ceil", precision=2)}}</td>
              <td>{{activity.total_elevation_gain}}</td>
              <td>
		{% if activity.activity -%}
                <a class="waves-effect waves-light btn primary" href="/activities/{{activity.activity}}" data-l10n-id="edit-activity-title">
                    <i class="material-icons">access_time</i>
                </a>
                {%- else -%}
                <a class="waves-effect waves-light btn primary" href="/activities/strava-add/{{activity.id}}" data-l10n-id="add-strava-activity">
                    <i class="material-icons">add</i>
                </a>
                <a class="waves-effect waves-light btn primary" href="/activities/strava-associate/{{activity.id}}/{{activity.start_date_local | truncate(length=10, end="")}}" data-l10n-id="associate-strava-activity">
                    <i class="material-icons">attach_file</i>
                </a>
                {%- endif %}
              </td>
            </tr>
            {%- endfor %}
          </tbody>
        </table>
      </div>
    </div>

{% endblock content %}
