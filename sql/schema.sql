CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  login VARCHAR NOT NULL,
  name VARCHAR(25) DEFAULT NULL,
  password VARCHAR(32) NOT NULL,
  rank INTEGER DEFAULT NULL,
  active VARCHAR DEFAULT NULL,
  inactive VARCHAR DEFAULT NULL
);
CREATE TABLE activity (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  date VARCHAR NOT NULL,
  distance NUMERIC NOT NULL,
  duration VARCHAR NOT NULL,
  average NUMERIC DEFAULT NULL,
  max_speed NUMERIC DEFAULT NULL,
  wind INTEGER NOT NULL,
  temperature INTEGER NOT NULL,
  description VARCHAR,
  ascent INTEGER NOT NULL,
  bike INTEGER NOT NULL,
  user INTEGER NOT NULL,
  intensity VARCHAR NOT NULL DEFAULT 'NORMAL',
  total NUMERIC DEFAULT NULL,
  FOREIGN KEY (bike) REFERENCES bike(id),
  FOREIGN KEY (user) REFERENCES user(id)
);
CREATE TABLE polar (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  activity INTEGER NOT NULL,
  path VARCHAR(70) DEFAULT NULL,
  average_hr INTEGER DEFAULT NULL,
  max_hr INTEGER DEFAULT NULL,
  energy INTEGER DEFAULT NULL,
  ascent INTEGER DEFAULT NULL,
  min_temperature INTEGER DEFAULT NULL,
  average_temperature INTEGER DEFAULT NULL,
  max_temperature INTEGER DEFAULT NULL,
  min_altitude INTEGER DEFAULT NULL,
  average_altitude INTEGER DEFAULT NULL,
  max_altitude INTEGER DEFAULT NULL,
  average_cadence INTEGER DEFAULT NULL,
  max_cadence INTEGER DEFAULT NULL,
  FOREIGN KEY (activity) REFERENCES activity(id)
);
CREATE TABLE road (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name VARCHAR NOT NULL,
  department INTEGER NOT NULL,
  altitude INTEGER DEFAULT NULL
);
CREATE TABLE route (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  activity INTEGER NOT NULL,
  route_order INTEGER NOT NULL,
  road INTEGER NOT NULL,
  FOREIGN KEY (road) REFERENCES road(id)
);
CREATE TABLE health (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  date VARCHAR NOT NULL,
  user INTEGER NOT NULL,
  weight NUMERIC NOT NULL,
  fat_content NUMERIC DEFAULT NULL,
  water_content NUMERIC DEFAULT NULL,
  FOREIGN KEY (user) REFERENCES user(id)
);
CREATE TABLE stravauser (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  user INTEGER NOT NULL,
  access_token VARCHAR NOT NULL,
  expires_at INTEGER NOT NULL,
  refresh_token VARCHAR NOT NULL,
  token_type VARCHAR NOT NULL,
  FOREIGN KEY (user) REFERENCES user(id)
);
CREATE TABLE maintenance (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  bike INTEGER NOT NULL,
  date VARCHAR NOT NULL,
  description VARCHAR NOT NULL,
  FOREIGN KEY (bike) REFERENCES bike(id)
);
CREATE TABLE IF NOT EXISTS "bike" (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  user INTEGER NOT NULL,
  name VARCHAR NOT NULL,
  enabled BOOLEAN NOT NULL,
  FOREIGN KEY (user) REFERENCES user(id)
);
CREATE VIEW v_bikesummary AS
SELECT
  COUNT(*) AS activities,
  SUM(distance) AS distance,
  (SELECT MIN(date) FROM activity WHERE bike=b.id) AS first_activity,
  (SELECT COUNT(*) FROM activity WHERE bike=b.id AND strftime('%Y', date)=strftime('%Y', CURRENT_TIMESTAMP)) AS year_activities,
  (SELECT COALESCE(SUM(distance), 0) FROM activity WHERE bike=b.id AND strftime('%Y', date)=strftime('%Y', CURRENT_TIMESTAMP)) AS year_distance,
  b.id AS bike_id,
  b.name AS bike_name
FROM activity AS a
  JOIN bike AS b ON a.bike=b.id
GROUP BY b.id, b.name;
CREATE TABLE stravamap (
	  id VARCHAR PRIMARY KEY NOT NULL,
	  polyline VARCHAR,
	  summary_polyline VARCHAR
);
CREATE TABLE stravaactivity (
  id BIGINT PRIMARY KEY AUTOINCREMENT NOT NULL,
  activity INTEGER,
  external_id VARCHAR NOT NULL,
  upload_id BIGINT NOT NULL,
  name VARCHAR NOT NULL,
  distance NUMERIC NOT NULL,
  moving_time INTEGER NOT NULL,
  elapsed_time INTEGER NOT NULL,
  total_elevation_gain NUMERIC NOT NULL,
  elev_high NUMERIC NOT NULL,
  elev_low NUMERIC,
  activitytype VARCHAR CHECK (activitytype in ('alpine_ski', 'backcountry_ski', 'canoeing', 'crossfit', 'e_bike_ride', 'elliptical', 'golf', 'handcycle', 'hike', 'ice_skate', 'inline_skate', 'kayaking', 'kitesurf', 'nordic_ski', 'ride', 'rock_climbing', 'roller_ski', 'rowing', 'run', 'sail', 'skateboard', 'snowboard', 'snowshoe', 'soccer', 'stair_stepper', 'stand_up_paddling', 'surfing', 'swim', 'velomobile', 'virtual_ride', 'virtual_run', 'walk', 'weight_training', 'wheelchair', 'windsurf', 'workout', 'yoga')) NOT NULL,
  start_date VARCHAR NOT NULL,
  start_date_local VARCHAR NOT NULL,
  timezone VARCHAR NOT NULL,
  -- start_latlng LatLng 	An instance of LatLng.
  -- end_latlng LatLng 	An instance of LatLng.
  achievement_count INTEGER NOT NULL,
  kudos_count INTEGER NOT NULL,
  comment_count INTEGER NOT NULL,
  athlete_count INTEGER,
  photo_count INTEGER NOT NULL,
  total_photo_count INTEGER NOT NULL,
  stravamap VARCHAR,
  trainer BOOLEAN NOT NULL,
  average_speed NUMERIC NOT NULL,
  max_speed NUMERIC NOT NULL,
  average_cadence NUMERIC,
  average_heartrate NUMERIC,
  max_heartrate NUMERIC,
  average_temp NUMERIC,
  gear_id VARCHAR NOT NULL,
  kilojoules NUMERIC,
  average_watts NUMERIC,
  device_watts BOOLEAN,
  max_watts INTEGER,
  weighted_average_watts INTEGER,
  description VARCHAR,
  -- gear SummaryGear 	An instance of SummaryGear.
  calories NUMERIC NOT NULL,
  device_name VARCHAR,
  FOREIGN KEY (activity) REFERENCES activity(id)
);
