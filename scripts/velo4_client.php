<?php
class Result {
	private $ok;
	private $err;
	private $isOk;
	private $isErr;
    private function __construct($ok, $err) {
    	$this->ok = $ok;
    	$this->err = $err;
    }
    public function getOk() {
    	return $this->ok;
    }
    public function getErr() {
    	return $this->err;
    }
    public function isOk() {
    	return $this->isOk;
    }
    public function isErr() {
    	return $this->isErr;
    }
    public static function err($err) {
    	$r = new Result(null, $err);
    	$r->isOk = false;
    	$r->isErr = true;
    	return $r;
    }
    public static function ok($ok) {
    	$r = new Result($ok, null);
    	$r->isOk = true;
    	$r->isErr = false;
    	return $r;
    }
}
class RestClient {
	public $postJson = true;
	function post($url, $data, $headers) {
		$crl = curl_init();
		curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, true);
 		curl_setopt($crl, CURLOPT_URL, $url);
 		if ($headers === null) {
 			$headers = array();
 		}
 		if ($data !== null) {
 			if ($this->postJson) {
		 		$payload = json_encode($data);
		 		$headers[] = 'Content-Type: application/json';
 			} else {
	 			$payload = http_build_query($data);
	 			//$headers[] = 'Content-Type: multipart/form-data';
	 			$headers[] = 'Content-Type: application/x-www-form-urlencoded';
 			}
 			curl_setopt($crl, CURLOPT_POSTFIELDS, $payload);
		} else {
	 		$headers[] = 'Content-Type: application/json';
 		}
 		curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);
 		curl_setopt($crl, CURLOPT_POST, true);
 		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
    		$result = curl_exec($crl);
    		curl_close($crl);
    		if ($result === false) {
    			return Result::err("Failed to get response at $url");
    		}
    		$json = json_decode($result, true);
    		if ($json === null) {
    			return Result::err("Failed to decode response as JSON at $url : " . substr($result, 0, 100) . '...');
    		}
    		return Result::ok($json);
	}
}

abstract class Velo4HasLogger {
    private $logger;
    protected function initLogger($prefix) {
        $this->logger = new Velo4Log($prefix);
    }
    function error($text) {
        $this->logger->error($text);
    }
    function info($text) {
        $this->logger->info($text);
    }
    function trace($text) {
        $this->logger->trace($text);
    }
}
class Velo4Cli extends Velo4HasLogger{
    function __construct() {
        $this->initLogger('Velo4Cli   ');
        if (!$this->checkRequirements()) {
            return;
        }
        $args = getopt('d:h:i:m:u:');
        if (empty($args) || isset($args['h'])) {
            $this->usage();
            return;
        }
        if (!isset($args['i']) || !isset($args['u'])) {
            $this->info('Missing argument');
            $this->usage();
            return;
        }
        print_r($args);
        $clientId = $args['i'];
        $url = $args['u'];
        $cli = new Velo4Client($clientId, $url);
        if ($cli->hasValidAccessToken()) {
            $this->trace('Token is valid');
        } else {
            $this->trace('Token is not valid');
            $result = $cli->getDeviceCode();
            if ($result->isErr()) {
            	$this->error('Failed to get device code: ' . $result->getErr());
            	return;
    	     }
    	     $response = $result->getOk();
            // device_code, user_code, verification_uri, verification_uri_complete, expires_in, interval
            $this->info('Please, go to ' . $response['verification_uri_complete']);
            $cli->checkAuthorization($response);
        }
        if (isset($args['m'])) {
        	if (isset($args['d'])) {
		        $response = $cli->postMass($args['d'], $args['m']);
	        } else {
		        $response = $cli->postMass(null, $args['m']);
	        }
        	$this->info('mass response : ' . $response);
        }
    }
    function checkRequirements() {
        $check = true;
    	$extensions = array('curl');
    	foreach ($extensions as $extension) {
	    if (!extension_loaded($extension)) {
	    	$this->error("Extension '$extension' is missing");
	    	$check = false;
	    }
    	}
    	return $check;
    }
    function usage() {
        $this->info('Usage:');
        $this->info('      -i client id');
        $this->info('      -u base URL');
        exit(0);
    }
}
class Velo4Client extends Velo4HasLogger {
    private $accessToken;
    private $baseUrl;
    private $clientId;
    private const DEVICE_CODE_URL = '/auth/device/code';
    private const HEALTH_CREATE = '/healths/create';
    private const TOKEN_URL = '/auth/token';
    /**
     * Request interval to get access_token, in seconds.
     */
    private $requestInterval = 5;
    function __construct($clientId, $url) {
        $this->initLogger('Velo4Client');
        $this->trace("clientId: $clientId");
        $this->trace("baseUrl: $url");
        $this->clientId = $clientId;
        $this->baseUrl = $url;
    }
    function getAccessTokenFilePath() {
        $base64 = base64_encode($this->clientId . $this->baseUrl);
        return sys_get_temp_dir() . "/velo4_access-$base64.json";
    }
    function checkAuthorization($deviceCodeResponse) {
    	$max = 10;
    	$cli = new RestClient();
    	$data = array(
    		'client_id'=>$this->clientId,
    		'device_code'=>$deviceCodeResponse['device_code'],
    		'grant_type'=>'device_code'
    	);
    	$headers = null;
    	$url = $this->baseUrl . Velo4Client::TOKEN_URL;
    	for ($i = 0; $i < $max; $i++) {
    		$this->info("Check #$i/$max...");
	    	$result = $cli->post($url, $data, $headers);
	    	if ($result->isOk()) {
	    		$response = $result->getOk();
	    		$json_response = substr(json_encode($response), 0, 100);
	    		$this->info("Got response at $url: $json_response...");
	    		$valid = true;
			$props = array('expires_at', 'expires_in', 'access_token', 'refresh_token', 'expires_in', 'token_type');
			foreach ($props as $prop) {
				if (!isset($response[$prop])) {
					$this->info("Missing $prop in response of token $json_response...");
					$valid = false;
					break;
				}
			}
			if ($valid) {
		    		$this->info('Store access token file');
		    		file_put_contents($this->getAccessTokenFilePath(), json_encode($response));
		    		break;
	    		}
	    	} else {
	    		$this->error("Failed to check device_code at $url: " . $result->getErr());
    		}
    		sleep($deviceCodeResponse['interval']);
    	}
    }
    function getDeviceCode() {
    	$url = $this->baseUrl . Velo4Client::DEVICE_CODE_URL;
    	$cli = new RestClient();
    	$data = array('client_id'=>$this->clientId);
    	$headers = null;
    	$result = $cli->post($url, $data, $headers);
    	if ($result->isOk()) {
		$response = $result->getOk();
		$props = array('device_code', 'user_code', 'verification_uri', 'verification_uri_complete', 'expires_in', 'interval');
		foreach ($props as $prop) {
			if (!isset($response[$prop])) {
				return Result::err("Missing $prop in response of device_code: " . substr(json_encode($response), 0, 100) . '...');
			}
		}
    	}
    	return $result;
    }
    public function hasValidAccessToken() {
        $filePath = $this->getAccessTokenFilePath();
        $this->trace("token file path is $filePath");
        if (!file_exists($filePath)) {
        	return false;
	}
	$string = file_get_contents($filePath);
	$json = json_decode($string, TRUE);
	$this->accessToken = $json['access_token'];
        $this->trace("token file path is $filePath");
        $this->info('TODO : check expiration of token');
        $this->info('TODO : use refresh token');
	return true;
    }
    public function postMass($date, $mass) {
        if ($date == null) {
        	$date = -1;
	}
    	$url = $this->baseUrl . Velo4Client::HEALTH_CREATE;
    	$cli = new RestClient();
    	$data = array('timestamp'=>$date, 'weight'=>$mass);
    	$headers = array('Authorization: Bearer ' . $this->accessToken);
    	$cli->postJson = false;
    	$result = $cli->post($url, $data, $headers);
    	$cli->postJson = true;
    	print_r($result);
    }
}
class Velo4Log {
    private $prefix;
    function __construct($prefix) {
        $this->prefix = $prefix;
    }
    function log($level, $text) {
        print($this->prefix . "    $level    $text\n");
    }
    function error($text) {
        $this->log('ERROR', $text);
    }
    function info($text) {
        $this->log('INFO ', $text);
    }
    function trace($text) {
        $this->log('TRACE', $text);
    }
}

$cli = new Velo4Cli();
$cli->info('done');
exit(0);
